﻿using System;
using System.Threading.Tasks;

namespace Libraries.Peripherals.BarcodeScanner
{
    public interface ISerialBarcodeScanner
    {
        bool Initialized { get; set; }

        event EventHandler<BarcodeDataReceivedEventArgs> DataReceivedEvent;

        void Dispose();
        Task<bool> InitBarcodeScanner();
    }
}