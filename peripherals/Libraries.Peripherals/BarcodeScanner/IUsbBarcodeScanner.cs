﻿using System;

namespace Libraries.Peripherals.BarcodeScanner
{
    public interface IUsbBarcodeScanner
    {
        bool Initialized { get; set; }

        event EventHandler<BarcodeDataReceivedEventArgs> DataReceivedEvent;

        void Dispose();
        bool InitBarcodeScanner();
    }
}