﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MT.Singularity.Composition;

namespace Libraries.Peripherals.BarcodeScanner
{
    [Export(typeof(IUsbBarcodeScannerConfiguration))]
    [InjectionBehavior(IsSingleton = true)]
    public class UsbBarcodeScannerConfiguration : BarcodeScannerConfiguration, IUsbBarcodeScannerConfiguration
    {
        public SerialPort UsedSerialPort { get; set; }
    }
}
