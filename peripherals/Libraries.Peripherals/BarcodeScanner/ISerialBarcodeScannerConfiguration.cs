﻿using MT.Singularity.IO;

namespace Libraries.Peripherals.BarcodeScanner
{
    public interface ISerialBarcodeScannerConfiguration : IBarcodeScannerConfiguration
    {
        IConnectionChannel<DataSegment> SerialConnectionChannel { get; set; }
    }
}