﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Libraries.Peripherals.BarcodeScanner
{
    public class BarcodeDataReceivedEventArgs : EventArgs
    {
        public string Data { get; }

        public BarcodeDataReceivedEventArgs(string data)
        {
            Data = data;
        }
    }
}
