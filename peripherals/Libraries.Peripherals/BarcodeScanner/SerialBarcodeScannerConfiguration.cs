﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MT.Singularity.Composition;
using MT.Singularity.IO;

namespace Libraries.Peripherals.BarcodeScanner
{
    [Export(typeof(ISerialBarcodeScannerConfiguration))]
    [InjectionBehavior(IsSingleton = true)]
    public class SerialBarcodeScannerConfiguration : BarcodeScannerConfiguration, ISerialBarcodeScannerConfiguration
    {
        public IConnectionChannel<DataSegment> SerialConnectionChannel { get; set; }
    }
}
