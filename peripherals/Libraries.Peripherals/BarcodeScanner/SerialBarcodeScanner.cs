﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Libraries.Peripherals.Helper;
using MT.Singularity.Composition;
using MT.Singularity.IO;
using MT.Singularity.Logging;
using MT.Singularity.Serialization;

namespace Libraries.Peripherals.BarcodeScanner
{
    [Export(typeof(ISerialBarcodeScanner))]
    public class SerialBarcodeScanner  : ISerialBarcodeScanner
    {
        private StringSerializer _stringSerializer;
        private string _dataline;

        public event EventHandler<BarcodeDataReceivedEventArgs> DataReceivedEvent;
        private ISerialBarcodeScannerConfiguration _scannerConfiguration;

        public bool Initialized { get; set; }

        public SerialBarcodeScanner(ISerialBarcodeScannerConfiguration configuration)
        {
            _scannerConfiguration = configuration;
        }
        public async Task<bool> InitBarcodeScanner()
        {
            try
            {
                Log4NetManager.ApplicationLogger.Info($"init barcode reader");
                var delimiterSerializer = new DelimiterSerializer(_scannerConfiguration.SerialConnectionChannel, CommonDataSegments.Cr);
                _stringSerializer = new StringSerializer(delimiterSerializer);

                if (_stringSerializer != null)
                {
                    await _stringSerializer.OpenAsync();
                    Log4NetManager.ApplicationLogger.Info($"serial channel opened");
                    var eventEmitter = new ChannelEventEmitter<string>(_stringSerializer);
                    Initialized = true;
                    eventEmitter.MessageRead += EventEmitterOnMessageRead;
                }
                else
                {
                    Initialized = false;
                }
                
            }
            catch (Exception e)
            {
                Initialized = false;
                Log4NetManager.ApplicationLogger.Error($"init failed ", e);
                return Initialized;
            }

            return Initialized;
        }

        private void EventEmitterOnMessageRead(object sender, MessageReadEventArgs<string> e)
        {
            Log4NetManager.ApplicationLogger.Info($"data received: {e.Message}");
            var data = e.Message;

            if (string.IsNullOrEmpty(data) == false)
            {
                DataReceivedEvent?.Invoke(null, new BarcodeDataReceivedEventArgs(data));
                Log4NetManager.ApplicationLogger.Info($"event triggered with data {data}");
            }
        }

        public void Dispose()
        {
            Initialized = false;
            _stringSerializer.Close();
            _stringSerializer.Dispose();
        }
    }
}
