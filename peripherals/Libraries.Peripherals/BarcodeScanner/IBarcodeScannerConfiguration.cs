﻿using Libraries.Peripherals.Helper;

namespace Libraries.Peripherals.BarcodeScanner
{
    public interface IBarcodeScannerConfiguration
    {
        EndOfDataCharacter EndOfDataCharacter { get; set; }
    }
}