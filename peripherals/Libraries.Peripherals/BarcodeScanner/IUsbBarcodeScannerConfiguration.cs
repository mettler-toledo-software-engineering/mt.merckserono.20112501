﻿using System.IO.Ports;

namespace Libraries.Peripherals.BarcodeScanner
{
    public interface IUsbBarcodeScannerConfiguration : IBarcodeScannerConfiguration
    {
        SerialPort UsedSerialPort { get; set; }
    }
}