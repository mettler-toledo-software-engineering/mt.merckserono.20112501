﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Libraries.Peripherals.Helper;

namespace Libraries.Peripherals.BarcodeScanner
{
    public abstract class BarcodeScannerConfiguration : IBarcodeScannerConfiguration
    {
        public EndOfDataCharacter EndOfDataCharacter { get; set; }

    }
}
