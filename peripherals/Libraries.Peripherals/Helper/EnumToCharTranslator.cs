﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Libraries.Peripherals.Helper
{
    public static class EnumToCharTranslator
    {
        public static string TranslateCharToString(this EndOfDataCharacter character)
        {
            switch (character)
            {
                case EndOfDataCharacter.Cr: return "\r";
                case EndOfDataCharacter.Lf: return "\n";
                case EndOfDataCharacter.CrLf: return "\r\n";

                default: return "";
            }
        }
    }
}
