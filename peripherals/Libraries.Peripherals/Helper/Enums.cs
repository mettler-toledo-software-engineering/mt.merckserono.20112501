﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Libraries.Peripherals.Helper
{
    public enum EndOfDataCharacter
    {
        Cr,
        Lf,
        CrLf
    }
}
