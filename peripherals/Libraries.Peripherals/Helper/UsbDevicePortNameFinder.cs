﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Management;
using System.Text;
using System.Threading.Tasks;
using MT.Singularity.Composition;
using MT.Singularity.Logging;

namespace Libraries.Peripherals.Helper
{
    [Export(typeof(IUsbDevicePortNameFinder))]
    [InjectionBehavior(IsSingleton = true)]
    public class UsbDevicePortNameFinder : IUsbDevicePortNameFinder
    {
        private const string SourceClass = nameof(UsbDevicePortNameFinder);

        public string FindPortName(string deviceName)
        {
            //if (deviceName.ToLowerInvariant().StartsWith("com"))
            //{
            //    return deviceName;
            //}

            using (ManagementObjectSearcher searcher = new ManagementObjectSearcher("root\\CIMV2", "SELECT * FROM Win32_PnPEntity"))
            {
                foreach (var o in searcher.Get())
                {
                    var queryObj = (ManagementObject)o;
                    string devicename = (string)queryObj["Name"];


                    if (string.IsNullOrEmpty(devicename) == false && devicename.Contains(deviceName))
                    {
                        //bsp: "Zebra CDC Scanner (COM5)"
                        int portstart = devicename.IndexOf("(", StringComparison.Ordinal) + 1;
                        int portWordLength = devicename.Length - portstart - 1;

                        var portname = devicename.Substring(portstart, portWordLength);

                        return portname;
                    }
                }
                Log4NetManager.ApplicationLogger.ErrorEx($"No Device with name {deviceName} found ", SourceClass);
                return "";
            }
        }
    }
}
