﻿namespace Libraries.Peripherals.Helper
{
    public interface IUsbDevicePortNameFinder
    {
        string FindPortName(string deviceName);
    }
}