﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Text;
using Libraries.Peripherals.Scoreboard;
using MT.Singularity.Composition;
using MT.Singularity.Platform.BackupRestore;
using MT.Singularity.Platform.Devices.Scale;

namespace Libraries.Peripherals.Protocols
{
    [Export(typeof(IMtContinous))]
    [InjectionBehavior(IsSingleton = true)]
    public class MtContinousAdvanced : IMtContinous
    {
        private WeightInformation _weight;
        private ISerialScoreboardConfiguration _scoreboardConfiguration;
        private int _decimalPointPosition;

        public MtContinousAdvanced(ISerialScoreboardConfiguration scoreboardConfiguration)
        {
            _scoreboardConfiguration = scoreboardConfiguration;
        }

        private byte CreateStatusByteA()
        {
            string bit7 = "0";
            string bit6 = "0";
            string bit5 = "1";
            string buildCode = "01";
            string decimalPlace = GetDecimalPlace();

            string output = $"{bit7}{bit6}{bit5}{buildCode}{decimalPlace}";
            return Convert.ToByte(output, 2);
        }
        private string GetDecimalPlace()
        {
           

            switch (_decimalPointPosition)
            {
                case 1: return "111";
                case 2: return "110";
                case 3: return "101";
                case 4: return "100";
                case 5: return "011";
                default: return "010";

            }

        }

        private byte CreateStatusByteB()
        {
            string bit7 = "0";
            string bit6 = "0"; //nullkalibrierung nicht gespeichert = 1
            string bit5 = "1";
            string bit4 = "1"; //lb vs kg
            string bit3 = _weight.NetStable ? "0" : "1";
            string bit2 = _weight.State == WeightState.Over || _weight.State == WeightState.Under ? "1" : "0";
            string bit1 = _weight.NetWeight < 0 ? "1" : "0";
            string bit0 = "1"; //netto = 1, brutto = 0

            string output = $"{bit7}{bit6}{bit5}{bit4}{bit3}{bit2}{bit1}{bit0}";

            return Convert.ToByte(output, 2);
        }

        private byte CreateStatusByteC()
        {
            string bit7 = "0";
            string bit6 = "0";
            string bit5 = "1";
            string bit4 = "0"; //times ten modifier = 1
            string bit3 = "0"; //print request = 1
            string bit2 = "0";
            string bit1 = "0";
            string bit0 = "0"; //bit 210 configure the unit

            string output = $"{bit7}{bit6}{bit5}{bit4}{bit3}{bit2}{bit1}{bit0}";

            return Convert.ToByte(output, 2);
        }

        private string GetDisplayedNetWeight()
        {
            //string netweight = _weight.NetWeightString.Replace(".", "").Trim();
            //netweight = netweight.Replace("-", "");
            //netweight = netweight.PadLeft(_scoreboardConfiguration.NumberOfDigits, ' ');
            //netweight = netweight.PadLeft(6, ' ');
            //return netweight;


            string netweight = PadNetWeight();

            return netweight;
        }

        private string GetTareWeight()
        {
            string tareweight = _weight.TareWeightString.Replace(".", "").Trim();
            tareweight = tareweight.Replace("-", "");
            tareweight = tareweight.PadLeft(_scoreboardConfiguration.NumberOfDigits, ' ');
            tareweight = tareweight.PadLeft(6, ' ');
            return tareweight;
        }

        private string PadNetWeight()
        {
            int protokolldigits = 6;
            string netweightstring = _weight.NetWeightString;
            double netweight = _weight.NetWeight;
            string tareweightstring = _weight.TareWeightString;
            int digitsToDisplay = netweightstring.Replace(".","").Trim().Length;
            int digitsAfterDot = tareweightstring.Length - (tareweightstring.IndexOf(".", StringComparison.CurrentCulture) + 1)  ;
            int digitsBeforeDot = digitsToDisplay - digitsAfterDot;
            int availableDigits = _scoreboardConfiguration.NumberOfDigits;
            // -102.653
            // 5 < 7 && 3 > (7-5 = 2)
            if (availableDigits >= digitsBeforeDot && availableDigits < digitsToDisplay && digitsAfterDot > (digitsToDisplay - availableDigits))
            {
                digitsAfterDot = digitsAfterDot - (digitsToDisplay - availableDigits);
            }

            _decimalPointPosition = protokolldigits - digitsAfterDot;

            string result = Math.Round(netweight, digitsAfterDot).ToString($"F{digitsAfterDot}",CultureInfo.InvariantCulture).Replace(".", "").Replace("-", "").PadLeft(protokolldigits,' ');

            Debug.WriteLine($"{_decimalPointPosition} {result}");
            return result;
        }

        //private string GetNetStringFromOptionalNetWeight()
        //{
        //    string tareweightstring = _weight.TareWeightString;
        //    int digitsAfterDot = tareweightstring.Length - (tareweightstring.IndexOf(".", StringComparison.CurrentCulture) + 1);
        //    string result = Math.Round(_optionalNetweight, digitsAfterDot).ToString($"F{digitsAfterDot}", CultureInfo.InvariantCulture);

        //    return result;
        //}

        public string GetContinousFormattedWeightString(WeightInformation weight)
        {
            _weight = weight;

            if (_weight == null)
            {
                _weight = new WeightInformation(1, true);

            }

            //netweight berechnen, damit die anzahl dezimalstellen für word a bereits ermittelt sind.
            var displayedWeight = GetDisplayedNetWeight();
            var tareWeight = GetTareWeight();

            var startcharacter = "\x02";


            byte worda = CreateStatusByteA();

            byte wordb = CreateStatusByteB();
            byte wordc = CreateStatusByteC();

            byte[] words = new byte[3];
            words[0] = worda;
            words[1] = wordb;
            words[2] = wordc;



            var endcharacter = "\x0D";
            
            string output = $"{startcharacter}{Encoding.ASCII.GetString(words)}{displayedWeight}{tareWeight}{endcharacter}";

            return output;

        }

    
}
}
