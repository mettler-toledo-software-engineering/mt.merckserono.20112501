﻿using MT.Singularity.Platform.Devices.Scale;

namespace Libraries.Peripherals.Protocols
{
    public interface IMtContinous
    {
        string GetContinousFormattedWeightString(WeightInformation weight);
    }
}