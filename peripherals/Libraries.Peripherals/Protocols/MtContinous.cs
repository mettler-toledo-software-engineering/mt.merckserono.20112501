﻿using System;
using System.Diagnostics;
using System.Text;
using Libraries.Peripherals.Scoreboard;
using MT.Singularity.Composition;
using MT.Singularity.Platform.Devices.Scale;

namespace Libraries.Peripherals.Protocols
{
    //[Export(typeof(IMtContinous))]
    //[InjectionBehavior(IsSingleton = true)]
    public class MtContinous// : IMtContinous
    {

        private WeightInformation _weight;
        private ISerialScoreboardConfiguration _scoreboardConfiguration;
        private int _decimalPlaces;
        public MtContinous(ISerialScoreboardConfiguration scoreboardConfiguration)
        {
            _scoreboardConfiguration = scoreboardConfiguration;
        }

        private byte CreateStatusByteA()
        {
            string bit7 = "0";
            string bit6 = "0";
            string bit5 = "1";
            string buildCode = "01";
            string decimalPlace = GetDecimalPlace();

            string output = $"{bit7}{bit6}{bit5}{buildCode}{decimalPlace}";
            return Convert.ToByte(output, 2);
        }
        private string GetDecimalPlace()
        {
            string netweight = _weight.NetWeightString.Replace("-", "").Trim();
            int decimalPointPosition = netweight.IndexOf(".", StringComparison.CurrentCulture);
            int length = netweight.Length;
            // 0.74
            if (decimalPointPosition > 0)
            {

                _decimalPlaces = length - decimalPointPosition + 1;
            }

            switch (_decimalPlaces)
            {
                case 1: return "111";
                case 2: return "110";
                case 3: return "101";
                case 4: return "100";
                case 5: return "011";
                default: return "010";

            }

            //switch (_decimalPlaces)
            //{
            //    case 5: return "111";
            //    case 4: return "110";
            //    case 3: return "101";
            //    case 2: return "100";
            //    case 1: return "011";
            //    default: return "010";

            //}
        }

        private byte CreateStatusByteB()
        {
            string bit7 = "0";
            string bit6 = "0"; //nullkalibrierung nicht gespeichert = 1
            string bit5 = "1";
            string bit4 = "1"; //lb vs kg
            string bit3 = _weight.NetStable ? "0" : "1";
            string bit2 = _weight.State == WeightState.Over || _weight.State == WeightState.Under ? "1" : "0";
            string bit1 = _weight.NetWeight < 0 ? "1" : "0";
            string bit0 = "1"; //netto = 1, brutto = 0

            string output = $"{bit7}{bit6}{bit5}{bit4}{bit3}{bit2}{bit1}{bit0}";

            return Convert.ToByte(output, 2);
        }

        private byte CreateStatusByteC()
        {
            string bit7 = "0";
            string bit6 = "0";
            string bit5 = "1";
            string bit4 = "0"; //times ten modifier = 1
            string bit3 = "0"; //print request = 1
            string bit2 = "0";
            string bit1 = "0";
            string bit0 = "0"; //bit 210 configure the unit

            string output = $"{bit7}{bit6}{bit5}{bit4}{bit3}{bit2}{bit1}{bit0}";

            return Convert.ToByte(output, 2);
        }

        private string GetDisplayedNetWeight()
        {
            string netweight = _weight.NetWeightString.Replace(".", "").Trim();
            netweight = netweight.Replace("-", "");
            netweight = netweight.PadLeft(_scoreboardConfiguration.NumberOfDigits, ' ');
            netweight = netweight.PadLeft(6, ' ');
            return netweight;


            //string netweight = PadNetWeight();

            //return netweight;
        }

        private string GetTareWeight()
        {
            string tareweight = _weight.TareWeightString.Replace(".", "").Trim();
            tareweight = tareweight.Replace("-", "");
            tareweight = tareweight.PadLeft(_scoreboardConfiguration.NumberOfDigits, ' ');
            tareweight = tareweight.PadLeft(6, ' ');
            return tareweight;
        }

        private string PadNetWeight()
        {
            string output = _weight.NetWeightString.Replace("-", "").Trim();
            double weight = _weight.NetWeight;
            // prüfen wie viele Stellen das Scoreboard hat
            int availableDigits = _scoreboardConfiguration.NumberOfDigits;
            if (weight<0)
            {
                //bei negativen beträgen ist eine stelle weniger verfügbar
                availableDigits = availableDigits - 1;
            }

            int decimalPointPosition = output.IndexOf(".", StringComparison.CurrentCulture);
            int length = output.Length;
            // schauen, wie viele nachkommastellen geliefert werden
            if (decimalPointPosition > 0)
            {
                _decimalPlaces = length - decimalPointPosition + 1;
            }


            output = output.Replace("-", "").Replace(".", "").Trim();

            //wenn mehr zahlen gebraucht werden als das scoreboard bieten kann
            //schneiden wir nachkommastellen ab und runden
            //dabei wird _decimalplaces auch verwendet um zu entscheiden, wo der punkt auf dem scoreboard gesetzt werden muss
            //-1250.12  6 > 5
            while (output.Length > availableDigits && _decimalPlaces > 0)
            {
                _decimalPlaces--;
                output = $"{Math.Round(weight, _decimalPlaces)}";
                output = output.Replace("-", "").Replace(".", "").Trim();
            } 
            
            output = output.PadLeft(6, ' ');
            
            return output;
        }
      
        public string GetContinousFormattedWeightString(WeightInformation weight)
        {
            _weight = weight;
            if (_weight == null)
            {
                _weight = new WeightInformation(1,true);
                
            }

            //netweight berechnen, damit die anzahl dezimalstellen für word a bereits ermittelt sind.
            var displayedWeight = GetDisplayedNetWeight();
            var tareWeight = GetTareWeight();
            Debug.WriteLine($"{_decimalPlaces} {displayedWeight}");
            var startcharacter = "\x02";


            byte worda = CreateStatusByteA();
            
            byte wordb = CreateStatusByteB();
            byte wordc = CreateStatusByteC();

            byte[] words = new byte[3];
            words[0] = worda;
            words[1] = wordb;
            words[2] = wordc;



            var endcharacter = "\x0D";

            string output = $"{startcharacter}{Encoding.ASCII.GetString(words)}{displayedWeight}{tareWeight}{endcharacter}";

            return output;

        }

    }
}

