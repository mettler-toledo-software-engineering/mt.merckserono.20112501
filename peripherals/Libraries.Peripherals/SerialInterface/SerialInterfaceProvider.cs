﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MT.Singularity.Composition;
using MT.Singularity.Logging;
using MT.Singularity.Platform.Devices;
using MT.Singularity.Platform.Infrastructure;

namespace Libraries.Peripherals.SerialInterface
{
    [Export(typeof(ISerialInterfaceProvider))]
    public class SerialInterfaceProvider : ISerialInterfaceProvider
    {
        public async Task<ISerialInterface> FindSerialInterface(IPlatformEngine engine, string portname)
        {
            var interfaceservice = await engine.GetInterfaceServiceAsync();
            var allinterfaces = await interfaceservice.GetAllInterfacesAsync();
            var allserialinterfaces = allinterfaces.OfType<ISerialInterface>().ToList();
            var serialInterface = allserialinterfaces.FirstOrDefault(serial => serial.PortName == portname);


            return serialInterface;

        }

        public async Task<ISerialInterface> FindSerialInterface(IPlatformEngine engine, int physicalPort)
        {
            var interfaceservice = await engine.GetInterfaceServiceAsync();
            var allinterfaces = await interfaceservice.GetAllInterfacesAsync();
            var allserialinterfaces = allinterfaces.OfType<ISerialInterface>().ToList();
            var serialInterface = allserialinterfaces.FirstOrDefault(serial => serial.LogicalPort== physicalPort);


            return serialInterface;

        }
    }
}
