﻿using System.Threading.Tasks;
using MT.Singularity.Platform.Devices;
using MT.Singularity.Platform.Infrastructure;

namespace Libraries.Peripherals.SerialInterface
{
    public interface ISerialInterfaceProvider
    {
        Task<ISerialInterface> FindSerialInterface(IPlatformEngine engine, string portname);
        Task<ISerialInterface> FindSerialInterface(IPlatformEngine engine, int physicalPort);
    }
}