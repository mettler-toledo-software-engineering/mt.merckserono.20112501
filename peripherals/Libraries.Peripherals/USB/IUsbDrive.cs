﻿namespace Libraries.Peripherals.USB
{
    public interface IUsbDrive
    {
        string DriveLetter { get; set; }
    }
}