﻿using System;

namespace Libraries.Peripherals.USB
{
    public interface IUsbEventProvider
    {
        bool UsbDriveAvailable { get; }

        //event EventHandler DeviceInsertedEvent;
        //event EventHandler DeviceRemovedEvent;

    }
}