﻿using System;
using System.IO;
using System.Management;
using MT.Singularity.Composition;

namespace Libraries.Peripherals.USB
{
    [Export(typeof(IUsbEventProvider))]
    [InjectionBehavior(IsSingleton = true)]
    public class UsbEventProvider : IUsbEventProvider
    {
        public bool UsbDriveAvailable { get; private set; }

        public event EventHandler DeviceInsertedEvent;
        public event EventHandler DeviceRemovedEvent;
        private IUsbDrive _usbDrive;

        public UsbEventProvider(IUsbDrive usbDrive)
        {
            _usbDrive = usbDrive;
            //RegisterForUsbEvents();
        }

        private bool CheckUsbDirectory()
        {
            return (Directory.Exists(_usbDrive.DriveLetter));
        }

        //private void RegisterForUsbEvents()
        //{

        //    UsbDriveAvailable = CheckUsbDirectory();
        //    WqlEventQuery insertQuery = new WqlEventQuery("SELECT * FROM __InstanceCreationEvent WITHIN 2 WHERE TargetInstance ISA 'Win32_PnPEntity'");

        //    ManagementEventWatcher insertWatcher = new ManagementEventWatcher(insertQuery);
        //    insertWatcher.EventArrived += DeviceInserted;

        //    insertWatcher.Start();

        //    //Win32_USBHub als target reagiert nur auf usb drives
        //    //Win32_PnPEntity als target reagiert auf alle geräte
        //    WqlEventQuery removeQuery = new WqlEventQuery("SELECT * FROM __InstanceDeletionEvent WITHIN 2 WHERE TargetInstance ISA 'Win32_PnPEntity'");
        //    ManagementEventWatcher removeWatcher = new ManagementEventWatcher(removeQuery);
        //    removeWatcher.EventArrived += DeviceRemoved;
        //    removeWatcher.Start();


        //}

        private void DeviceRemoved(object sender, EventArrivedEventArgs e)
        {
            UsbDriveAvailable = CheckUsbDirectory();
            DeviceRemovedEvent?.Invoke(this, EventArgs.Empty);
        }

        private void DeviceInserted(object sender, EventArrivedEventArgs e)
        {
            UsbDriveAvailable = CheckUsbDirectory();
            DeviceInsertedEvent?.Invoke(this, EventArgs.Empty);
        }

        


        //private const UInt32 WM_DEVICECHANGE = 0x0219;

        //private void onWindow_Loaded(object sender, RoutedEventArgs e)

        //{

        //    HwndSource source = PresentationSource.FromVisual(this) as HwndSource;

        //    source.AddHook(WndProc);

        //}





        //private IntPtr WndProc(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)

        //{

        //    //

        //    if ((hwnd == null) || (msg != WM_DEVICECHANGE))

        //        return IntPtr.Zero;





        //    //

        //    this.Dispatcher.Invoke(new Action(() => {

        //        // Placeholder for WMI calls

        //    }), null);



        //    return IntPtr.Zero;



        //}
    }
}
