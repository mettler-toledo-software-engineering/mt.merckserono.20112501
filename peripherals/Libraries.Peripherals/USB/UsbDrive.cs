﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MT.Singularity.Composition;

namespace Libraries.Peripherals.USB
{
    [Export(typeof(IUsbDrive))]
    [InjectionBehavior(IsSingleton = true)]
    public class UsbDrive : IUsbDrive
    {
        public string DriveLetter { get; set; } = "D:\\";
    }
}
