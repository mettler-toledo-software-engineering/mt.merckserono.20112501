﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MT.Singularity.Composition;
using MT.Singularity.Platform.Devices.Scale;
using MT.Singularity.Serialization;

namespace Libraries.Peripherals.Scoreboard
{
    [Export(typeof(ISerialScoreboardConfiguration))]
    [InjectionBehavior(IsSingleton = true)]
    public class SerialScoreboardConfiguration : ISerialScoreboardConfiguration
    {
        public int NumberOfDigits { get; set; }

        public StringSerializer StringSerializer { get; set; }


    }
}
