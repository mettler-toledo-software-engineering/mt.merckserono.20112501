﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using Libraries.Peripherals.Protocols;
using MT.Singularity.Composition;
using MT.Singularity.IO;
using MT.Singularity.Platform.Devices.Scale;

namespace Libraries.Peripherals.Scoreboard
{
    /// <summary>
    /// this scoreboard is used to display the current net value on the scale
    /// </summary>
    [Export(typeof(IAutomatedSerialScoreBoard))]
    [InjectionBehavior(IsSingleton = true)]
    public class AutomatedSerialScoreBoard : SerialScoreBoard, IAutomatedSerialScoreBoard
    {
        private IScaleService _scaleService;

        public AutomatedSerialScoreBoard(ISerialScoreboardConfiguration scoreboardConfiguration, IMtContinous continousProtokol, IScaleService scaleService) : base(scoreboardConfiguration, continousProtokol)
        {
            _scaleService = scaleService;
        }

        private void SelectedScaleOnNewChangedWeightInDisplayUnit(WeightInformation weight)
        {
            Weight = weight;
        }

        public override void InitScoreBoard()
        {
            base.InitScoreBoard();
            _scaleService.SelectedScale.NewWeightInDisplayUnit += SelectedScaleOnNewChangedWeightInDisplayUnit;
        }


    }
}
