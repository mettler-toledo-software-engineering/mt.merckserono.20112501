﻿using MT.Singularity.Serialization;

namespace Libraries.Peripherals.Scoreboard
{
    public interface ISerialScoreboardConfiguration
    {
        StringSerializer StringSerializer { get; set; }
        int NumberOfDigits { get; set; }
    }
}