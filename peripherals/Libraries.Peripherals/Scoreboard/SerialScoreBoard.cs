﻿using System;
using System.Threading.Tasks;
using System.Timers;
using Libraries.Peripherals.Protocols;
using MT.Singularity.IO;
using MT.Singularity.Platform.Devices.Scale;

namespace Libraries.Peripherals.Scoreboard
{
    public abstract class SerialScoreBoard 
    {
        protected ISerialScoreboardConfiguration ScoreboardConfiguration;
        protected IMtContinous ContinousProtokol;
        protected WeightInformation Weight;
        private Timer _weightUpdateTimer;
        protected SerialScoreBoard(ISerialScoreboardConfiguration scoreboardConfiguration, IMtContinous continousProtokol)
        {
            ScoreboardConfiguration = scoreboardConfiguration;
            ContinousProtokol = continousProtokol;
        }

        public virtual void InitScoreBoard()
        {
            if (ScoreboardConfiguration.StringSerializer?.IsOpen == false)
            {
                ScoreboardConfiguration.StringSerializer?.OpenAsync().ContinueWith(StartTimer);
            }
        }

        private  void StartTimer(Task openSerializerTask)
        {
            if (openSerializerTask.Exception != null)
            {
                Console.WriteLine(openSerializerTask.Exception);
                //return;
            }
            _weightUpdateTimer = new Timer(250);
            _weightUpdateTimer.AutoReset = true;
            _weightUpdateTimer.Elapsed += RefreshScoreBoard;
            _weightUpdateTimer.Start();
        }

        protected void CatchException(Task serializerTask)    
        {
            if (serializerTask.Exception != null)
            {
                Console.WriteLine(serializerTask.Exception);
            }
        }

        protected virtual void RefreshScoreBoard(object sender, ElapsedEventArgs e)
        {
            if (ScoreboardConfiguration.StringSerializer != null && ScoreboardConfiguration.StringSerializer.IsOpen)
            {
                var scoreboardweight = ContinousProtokol.GetContinousFormattedWeightString(Weight);
                ScoreboardConfiguration.StringSerializer.WriteAsync(scoreboardweight).ContinueWith(CatchException);
            }
        }
    }
}
