﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Libraries.Peripherals.Scoreboard
{
    public interface ISerialScoreBoard
    {
        void InitScoreBoard();
    }
}
