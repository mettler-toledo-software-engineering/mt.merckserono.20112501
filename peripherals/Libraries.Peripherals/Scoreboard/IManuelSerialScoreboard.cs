﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MT.Singularity.Platform.Devices.Scale;

namespace Libraries.Peripherals.Scoreboard
{
    public interface IManuelSerialScoreboard : ISerialScoreBoard
    {
        void SetNewScoreBoardWeight(WeightInformation weightToDisplay);

    }
}