﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using Libraries.Peripherals.Protocols;
using MT.Singularity.Composition;
using MT.Singularity.IO;
using MT.Singularity.Platform.Devices.Scale;

namespace Libraries.Peripherals.Scoreboard
{
    /// <summary>
    /// this scoreboard is used to display your own values independent from the current weight on the scale
    /// </summary>
    [Export(typeof(IManuelSerialScoreboard))]
    [InjectionBehavior(IsSingleton = true)]
    public class ManuelSerialScoreboard : SerialScoreBoard, IManuelSerialScoreboard
    {
        private double _optionalNetWeight;
        public ManuelSerialScoreboard(ISerialScoreboardConfiguration scoreboardConfiguration, IMtContinous continousProtokol) : base(scoreboardConfiguration, continousProtokol)
        {
        }

        public void SetNewScoreBoardWeight(WeightInformation weightToDisplay)
        {
            Weight = weightToDisplay;
        }


        protected override void RefreshScoreBoard(object sender, ElapsedEventArgs e)
        {


            if (ScoreboardConfiguration.StringSerializer != null && ScoreboardConfiguration.StringSerializer.IsOpen)
            {
                var scoreboardweight = ContinousProtokol.GetContinousFormattedWeightString(Weight);
                ScoreboardConfiguration.StringSerializer.WriteAsync(scoreboardweight).ContinueWith(CatchException);
            }
        }
    }
}
