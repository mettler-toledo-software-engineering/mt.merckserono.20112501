﻿using MT.Singularity.Composition;
using MT.Singularity.IO;
using MT.Singularity.Platform.Devices;
using MT.Singularity.Serialization;

namespace Libraries.Peripherals.Printer
{
    [Export(typeof(ISerialPrinterConfiguration))]
    [InjectionBehavior(IsSingleton = true)]
    public class SerialPrinterConfiguration : ISerialPrinterConfiguration
    {
        public ISerialInterface SerialInterface { get; set; }
        public StringSerializer StringSerializer { get; set; }
        public int TimeDelayBetweenPrintingLines {get; set; } = 100;
        public string PrintContrast { get; set; } = "K6";
        public int ResistanceClass { get; set; }
        public int CodePage { get; set; } = 6;

        //codepage 6 = 1252; 2 = ga46 german legacy; 4 = 1250
    }
}
