﻿using System;
using MT.Singularity.Composition;
using Libraries.Authentication.ActiveDirectory;

namespace Libraries.Authentication.User
{
    [Export(typeof(IAuthentificatedUser))]
    public class AuthentificatedUser :  IAuthentificatedUser
    {
        public string Password { get; set; }
        public string UserName { get; set; }
        public string DisplayName { get; set; }
        public string Access_Token { get; set; }
        public DateTime LoginTime { get; set; }
    }
}
