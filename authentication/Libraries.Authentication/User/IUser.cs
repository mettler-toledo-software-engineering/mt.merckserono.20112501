﻿namespace Libraries.Authentication.User
{
    public interface IUser
    {
        string Password { get; set; }
        string UserName { get; set; }
    }
}