﻿using MT.Singularity.Composition;

namespace Libraries.Authentication.User
{
    [Export(typeof(IUser))]
    public class User : IUser
    {
        public string UserName { get; set; }
        public string Password { get; set; }

    }
}
