﻿using Libraries.Authentication.ActiveDirectory;
using System;

namespace Libraries.Authentication.User
{
    public interface IAuthentificatedUser
    {
        string Password { get; set; }
        string UserName { get; }
        string DisplayName { get; set; }
        string Access_Token { get; set; }
        DateTime LoginTime { get; set; }
    }
}