﻿using System.Threading.Tasks;
using Libraries.Authentication.User;

namespace Libraries.Authentication.ActiveDirectory
{
    public interface IActiveDirectoryAuthentification
    {
        IActiveDirectoryAuthentificationResult AuthentificateUser(IUser user);

        IActiveDirectoryAuthentificationResult AuthentificateUserWithDomainGroup(IUser user);
        IActiveDirectoryAuthentificationResult ResetPassword(IUser user, string newPassword);
    }
}