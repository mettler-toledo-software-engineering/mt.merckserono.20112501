﻿using System.Collections.Generic;
using Libraries.Authentication.LoginAndLogout;
using MT.Singularity.Composition;

namespace Libraries.Authentication.ActiveDirectory
{
    [Export(typeof(IActiveDirectoryConfiguration))]
    [InjectionBehavior(IsSingleton = true)]
    public class ActiveDirectoryConfiguration : IActiveDirectoryConfiguration
    {
        public string DomainName { get; set; }
        public Dictionary<int,string> DomainGroups { get; set; } = new Dictionary<int, string>();

    }
}
