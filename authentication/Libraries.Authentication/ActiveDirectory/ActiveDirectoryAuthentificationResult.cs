﻿using Libraries.Authentication.User;
using MT.Singularity.Composition;

namespace Libraries.Authentication.ActiveDirectory
{
    [Export(typeof(IActiveDirectoryAuthentificationResult))]
    public class ActiveDirectoryAuthentificationResult : IActiveDirectoryAuthentificationResult
    {
        public bool AuthentificationSuccess { get; set; }
        public string ReasonCode { get; set; }
        public IAuthentificatedUser AuthentificatedUser { get; set; }
    }
}
