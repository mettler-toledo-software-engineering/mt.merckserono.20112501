﻿using System.Collections.Generic;
using Libraries.Authentication.LoginAndLogout;

namespace Libraries.Authentication.ActiveDirectory
{
    public interface IActiveDirectoryConfiguration
    {
        string DomainName { get; set; }
        Dictionary<int, string> DomainGroups { get; set; }
    }
}