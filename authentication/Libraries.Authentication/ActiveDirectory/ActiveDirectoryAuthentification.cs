﻿using System;
using System.DirectoryServices.AccountManagement;
using System.DirectoryServices.Protocols;
using System.Net;
using System.Threading.Tasks;
using Libraries.Authentication.LocalTerminal;
using Libraries.Authentication.User;
using MT.Singularity.Composition;

namespace Libraries.Authentication.ActiveDirectory
{
 
    [Export(typeof(IActiveDirectoryAuthentification))]
    public class ActiveDirectoryAuthentification : IActiveDirectoryAuthentification
    {

        private IActiveDirectoryConfiguration _activeDirectoryConfiguration;
        private ILocalAuthentification _localAuthentification;
        private IActiveDirectoryAuthentificationResult _authentificationResult;
        public ActiveDirectoryAuthentification(IActiveDirectoryAuthentificationResult authentificationResult, ILocalAuthentification localAuthentification, IActiveDirectoryConfiguration activeDirectoryConfiguration)
        {
            _authentificationResult = authentificationResult;
            _localAuthentification = localAuthentification;
            _activeDirectoryConfiguration = activeDirectoryConfiguration;
        }
        public IActiveDirectoryAuthentificationResult AuthentificateUser(IUser user)
        {
            _authentificationResult.AuthentificatedUser = null;

            var localUser = _localAuthentification.AuthentificateLocalUser(user);
            if (localUser != null)
            {
                _authentificationResult.AuthentificatedUser = localUser;
                _authentificationResult.AuthentificationSuccess = true;
                _authentificationResult.ReasonCode = string.Empty;
                return _authentificationResult;
            }
            try
            {
                using (PrincipalContext pc = new PrincipalContext(ContextType.Domain, _activeDirectoryConfiguration.DomainName))
                {
                    bool isValid = pc.ValidateCredentials(user.UserName, user.Password);
                    
                    if (isValid)
                    {
                        UserPrincipal up = UserPrincipal.FindByIdentity(pc, user.UserName);

                        var validuser = new AuthentificatedUser();
                        validuser.LoginTime = DateTime.Now;
                        validuser.Password = user.Password;
                        validuser.UserName = user.UserName;
                        validuser.DisplayName = up?.DisplayName;
                        _authentificationResult.AuthentificatedUser = validuser;

                        _authentificationResult.AuthentificationSuccess = true;
                        _authentificationResult.ReasonCode = string.Empty;
                        return _authentificationResult;
                    }
                    _authentificationResult.AuthentificationSuccess = false;
                    _authentificationResult.ReasonCode = string.Empty;
                    return _authentificationResult;

                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                _authentificationResult.AuthentificationSuccess = false;
                _authentificationResult.ReasonCode = e.Message;
                return _authentificationResult;
            }

            //try
            //{
            //    LdapConnection connection = new LdapConnection(_activeDirectoryConfiguration.DomainName);
            //    NetworkCredential credential = new NetworkCredential(user.UserName, user.Password);
            //    connection.Credential = credential;
            //    connection.Bind();

            //}
            //catch (LdapException lexc)
            //{
            //    string error = lexc.ServerErrorMessage;
            //    Console.WriteLine(lexc);
            //}
            //catch (Exception exc)
            //{
            //    Console.WriteLine(exc);
            //}

        }

        public IActiveDirectoryAuthentificationResult AuthentificateUserWithDomainGroup(IUser user)
        {
            _authentificationResult.AuthentificatedUser = null;

            var localUser = _localAuthentification.AuthentificateLocalUser(user);
            if (localUser != null)
            {
                _authentificationResult.AuthentificatedUser = localUser;
                _authentificationResult.AuthentificationSuccess = true;
                _authentificationResult.ReasonCode = string.Empty;
                return _authentificationResult;
            }
            try
            {
                using (PrincipalContext pc = new PrincipalContext(ContextType.Domain, _activeDirectoryConfiguration.DomainName))
                {
                    bool isValid = pc.ValidateCredentials(user.UserName, user.Password);
                    
                    UserPrincipal up = UserPrincipal.FindByIdentity(pc, user.UserName);
                    GroupPrincipal group = null;

                    foreach (var domainGroup in _activeDirectoryConfiguration.DomainGroups)
                    {
                        group = GroupPrincipal.FindByIdentity(pc, domainGroup.Value);
                        if (isValid && up != null && group != null && up.IsMemberOf(group))
                        {
                            var validuser = new AuthentificatedUser();
                            validuser.LoginTime = DateTime.Now;
                            validuser.Password = user.Password;
                            validuser.UserName = user.UserName;
                            validuser.DisplayName = up.DisplayName;
                            _authentificationResult.AuthentificatedUser = validuser;

                            _authentificationResult.AuthentificationSuccess = true;
                            _authentificationResult.ReasonCode = string.Empty;
                            return _authentificationResult;
                        }
                    }
                    

                    _authentificationResult.AuthentificationSuccess = false;
                    _authentificationResult.ReasonCode = string.Empty;
                    return _authentificationResult;

                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                _authentificationResult.AuthentificationSuccess = false;
                _authentificationResult.ReasonCode = e.Message;
                return _authentificationResult;
            }

            //try
            //{
            //    LdapConnection connection = new LdapConnection(_activeDirectoryConfiguration.DomainName);
            //    NetworkCredential credential = new NetworkCredential(user.UserName, user.Password);
            //    connection.Credential = credential;
            //    connection.Bind();

            //}
            //catch (LdapException lexc)
            //{
            //    string error = lexc.ServerErrorMessage;
            //    Console.WriteLine(lexc);
            //}
            //catch (Exception exc)
            //{
            //    Console.WriteLine(exc);
            //}

        }
        public IActiveDirectoryAuthentificationResult ResetPassword(IUser user, string newPassword)
        {
            try
            {
                using (var context = new PrincipalContext(ContextType.Domain, _activeDirectoryConfiguration.DomainName))
                {
                    using (var aduser = UserPrincipal.FindByIdentity(context, IdentityType.SamAccountName, user.UserName))
                    {
                        //aduser?.SetPassword("newpassword");
                        // or
                        aduser?.ChangePassword(user.Password, newPassword);

                        aduser?.Save();
                        //_authentificationResult.AuthentificatedUser.UserName = user.UserName;
                        _authentificationResult.AuthentificatedUser.LoginTime = DateTime.Now;
                        _authentificationResult.AuthentificationSuccess = true;
                        _authentificationResult.ReasonCode = string.Empty;
                        return _authentificationResult;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                _authentificationResult.AuthentificationSuccess = false;
                _authentificationResult.ReasonCode = e.Message;
                return _authentificationResult;
            }


        }

        /*
         *
"8009030C: LdapErr: DSID-0C0904DC, comment: AcceptSecurityContext error, data 52e, v1db1",

if the users password needs to be changed, it will contain

"8009030C: LdapErr: DSID-0C0904DC, comment: AcceptSecurityContext error, data 773, v1db1"

The lexc.ServerErrorMessage data value is a hex representation of the Win32 Error Code. These are the same error codes which would be returned by otherwise invoking the Win32 LogonUser API call. The list below summarizes a range of common values with hex and decimal values:

525​ user not found ​(1317)
52e​ invalid credentials ​(1326)
530​ not permitted to logon at this time​ (1328)
531​ not permitted to logon at this workstation​ (1329)
532​ password expired ​(1330)
533​ account disabled ​(1331) 
701​ account expired ​(1793)
773​ user must reset password (1907)
775​ user account locked (1909)
         */
    }
}
