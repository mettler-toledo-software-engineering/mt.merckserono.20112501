﻿using Libraries.Authentication.User;

namespace Libraries.Authentication.ActiveDirectory
{
    public interface IActiveDirectoryAuthentificationResult
    {
        bool AuthentificationSuccess { get; set; }
        string ReasonCode { get; set; }
        IAuthentificatedUser AuthentificatedUser { get; set; }
    }
}