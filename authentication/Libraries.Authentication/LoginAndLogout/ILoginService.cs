﻿using System;
using Libraries.Authentication.User;

namespace Libraries.Authentication.LoginAndLogout
{
    public interface ILoginService
    {
        event EventHandler LoginEvent;

        void RaiseUserLoginEvent();
    }
}