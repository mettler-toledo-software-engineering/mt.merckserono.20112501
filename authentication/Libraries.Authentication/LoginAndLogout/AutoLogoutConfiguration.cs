﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MT.Singularity.Composition;

namespace Libraries.Authentication.LoginAndLogout
{
    [Export(typeof(IAutoLogoutConfiguration))]
    [InjectionBehavior(IsSingleton = true)]
    public class AutoLogoutConfiguration : IAutoLogoutConfiguration
    {
        public int LogoutTime { get; set; }
    }
}
