﻿namespace Libraries.Authentication.LoginAndLogout
{
    public interface IAutoLogoutConfiguration
    {
        int LogoutTime { get; set; }
    }
}