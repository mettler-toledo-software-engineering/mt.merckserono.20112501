﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Libraries.Authentication.User;
using MT.Singularity.Composition;

namespace Libraries.Authentication.LoginAndLogout
{
    [Export(typeof(ILoginService))]
    [InjectionBehavior(IsSingleton = true)]
    public class LoginService : ILoginService
    {
        public event EventHandler LoginEvent;

        public void RaiseUserLoginEvent()
        {
            LoginEvent?.Invoke(this, EventArgs.Empty);
        }
    }
}
