﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using Libraries.Authentication.User;
using MT.Singularity.Composition;

namespace Libraries.Authentication.LoginAndLogout
{

    [Export(typeof(IAutoLogoutService))]
    [InjectionBehavior(IsSingleton = true)]
    public class AutoLogoutService : IAutoLogoutService
    {

        private ILoginService _loginService;
        private DateTime _lastActivity;
        private readonly Timer _logOutTimer = new Timer(1000);
        private IAutoLogoutConfiguration _logoutConfiguration;

        public AutoLogoutService(ILoginService loginService, IAutoLogoutConfiguration logoutConfiguration)
        {
            _loginService = loginService;
            _logOutTimer.AutoReset = true;
            _logoutConfiguration = logoutConfiguration;
            _logOutTimer.Elapsed += CheckIfUserNeedsToLogOff;
            _loginService.LoginEvent += OnUserLoggedInEvent;
        }

        private void CheckIfUserNeedsToLogOff(object sender, ElapsedEventArgs e)
        {
            var checktime = DateTime.Now.AddMinutes(_logoutConfiguration.LogoutTime*(-1));
            //var checktime = DateTime.Now.AddSeconds(-15);
            if (_lastActivity < checktime)
            {
                LogOutUser();
            }
        }

        private void OnUserLoggedInEvent(object sender, EventArgs e)
        {
            _lastActivity = DateTime.Now;
            _logOutTimer.Start();
        }

        public event EventHandler LogOutEvent;

        private void LogOutUser()
        {
            LogOutEvent?.Invoke(this, EventArgs.Empty);
            _logOutTimer.Stop();
        }

        public void ResetLogOffTimer()
        {
            _lastActivity = DateTime.Now;
        }
    }
}
