﻿using System;

namespace Libraries.Authentication.LoginAndLogout
{
    public interface IAutoLogoutService
    {
        event EventHandler LogOutEvent;

        void ResetLogOffTimer();
    }
}