﻿using System.Threading.Tasks;
using Libraries.Authentication.User;

namespace Libraries.Authentication.LocalTerminal
{
    public interface ILocalAuthentification
    {
        IAuthentificatedUser AuthentificateLocalUser(IUser user);
    }
}