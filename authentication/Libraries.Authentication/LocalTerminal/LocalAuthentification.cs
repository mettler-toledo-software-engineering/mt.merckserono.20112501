﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Libraries.Authentication.User;
using MT.Singularity.Composition;
using MT.Singularity.Platform.UserManagement;

namespace Libraries.Authentication.LocalTerminal
{
    [Export(typeof(ILocalAuthentification))]
    public class LocalAuthentification : ILocalAuthentification
    {
        private readonly ISecurityService _securityService;

        public LocalAuthentification(ISecurityService securityService)
        {
            _securityService = securityService;
        }

        public IAuthentificatedUser AuthentificateLocalUser(IUser user)
        {
            
            if (_securityService.CurrentUser.Name == "DegradedModeUser")
            {
                var authentificatedUser = new AuthentificatedUser {UserName = "DegradedModeUser", LoginTime = DateTime.Now};

                return authentificatedUser;
            }
            return null;

        }
    }
}
