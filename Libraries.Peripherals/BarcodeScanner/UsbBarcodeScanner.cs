﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Libraries.Peripherals.Helper;
using MT.Singularity.Composition;
using MT.Singularity.Logging;

namespace Libraries.Peripherals.BarcodeScanner
{
    [Export(typeof(IUsbBarcodeScanner))]
    public class UsbBarcodeScanner : IUsbBarcodeScanner
    {


        private string _dataline;
        public event EventHandler<BarcodeDataReceivedEventArgs> DataReceivedEvent;
        private IUsbBarcodeScannerConfiguration _scannerConfiguration;
        public bool Initialized { get; set; }

        public UsbBarcodeScanner(IUsbBarcodeScannerConfiguration configuration)
        {
            _scannerConfiguration = configuration;
        }


        public bool InitBarcodeScanner()
        {
            _scannerConfiguration.UsedSerialPort.DataReceived -= UsedSerialPortOnDataReceived;

            if (_scannerConfiguration.UsedSerialPort != null)
            {
                if (_scannerConfiguration.UsedSerialPort.IsOpen)
                {
                    _scannerConfiguration.UsedSerialPort.DataReceived -= UsedSerialPortOnDataReceived;
                    _scannerConfiguration.UsedSerialPort.Close();
                }
                _scannerConfiguration.UsedSerialPort.Open();
                _scannerConfiguration.UsedSerialPort.DataReceived += UsedSerialPortOnDataReceived;

                Initialized = true;
            }
            else
            {
                Log4NetManager.ApplicationLogger.ErrorEx($"USB Barcode Scanner coud not be initialized ");
                Initialized = false;
            }
            return Initialized;
        }

        private void UsedSerialPortOnDataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            var data = _scannerConfiguration.UsedSerialPort.ReadExisting();
            _dataline += data;
            if (string.IsNullOrEmpty(_dataline) == false && _dataline.EndsWith(_scannerConfiguration.EndOfDataCharacter.TranslateCharToString()))
            {
                _dataline = _dataline.Replace(_scannerConfiguration.EndOfDataCharacter.TranslateCharToString(), "");
                DataReceivedEvent?.Invoke(null, new BarcodeDataReceivedEventArgs(_dataline));
                _dataline = "";
            }

        }

        public void Dispose()
        {
            _scannerConfiguration.UsedSerialPort.DataReceived -= UsedSerialPortOnDataReceived;
            _scannerConfiguration.UsedSerialPort.Close();
            Initialized = false;
            _dataline = "";
        }
    }
}
