﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IND930.Library.Models;

namespace IND930.Library.Helpers
{
    public static class FillProcessResetter
    {
        public static IFillProcessModel ResetFillProcess(this IFillProcessModel fillProcess)
        {
            fillProcess.UsedComponents.Clear();

            fillProcess.CurrentComponent = string.Empty;
            fillProcess.InitialScaleValues = null;
            fillProcess.HasBeenPrinted = false;
            fillProcess.ProcessFinishTime = DateTime.MinValue;
            fillProcess.ProcessStartTime = DateTime.MinValue;
            fillProcess.TaredContainer = null;
            fillProcess.HasBeenManuallyTared = false;

            return fillProcess;
        }
    }
}
