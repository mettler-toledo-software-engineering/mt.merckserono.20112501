﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IND930.Library.Helpers;
using MT.Singularity.Platform.Devices.Scale;
using MT.Singularity.Platform.Translation;

namespace IND930.Library.Models
{
    public class ComponentModel
    {

        public int ComponentCounter { get; set; }
        public string ComponentName { get; set; }
        public WeightInformation ComponentWeight { get; set; }

        public DateTime RegistrationTime { get; set; }

        public string DisplayText => $"{ComponentCounter}. {ComponentName} - Net : {ComponentWeight.NetWeight.ToString(RecipeFormatter.RecipeDecimalPlaceFormatting)} {ComponentWeight.Unit.ToTranslatedAbbreviation()} - Brut : {ComponentWeight.GrossWeight.ToString(RecipeFormatter.RecipeDecimalPlaceFormatting)} {ComponentWeight.Unit.ToTranslatedAbbreviation()}";

    }
}
