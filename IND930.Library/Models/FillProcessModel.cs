﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Libraries.Authentication.User;
using MT.Singularity.Composition;
using MT.Singularity.Platform.Devices.Scale;

namespace IND930.Library.Models
{
    [Export(typeof(IFillProcessModel))]
    [InjectionBehavior(IsSingleton = true)]
    public class FillProcessModel : IFillProcessModel
    {
        public IAuthentificatedUser LoggedInUser { get; set; }
        public string Formula { get; set; }
        public string BatchNumber { get; set; }
        public string CurrentComponent { get; set; }
        public string ScaleName { get; set; }
        public string Location { get; set; }
        public WeightInformation InitialScaleValues { get; set; }
        public WeightInformation TaredContainer { get; set; }
        public DateTime TareTime { get; set; }
        public DateTime ProcessStartTime { get; set; }
        public DateTime ProcessFinishTime { get; set; }
        public DateTime PrintTime { get; set; }

        public List<ComponentModel> UsedComponents { get; set; } = new List<ComponentModel>();
        public double TotalFormulaNetWeight => UsedComponents.Sum(component => component.ComponentWeight.NetWeight);
        public bool HasBeenPrinted { get; set; }
        public bool HasBeenManuallyTared { get; set; }
    }
}
