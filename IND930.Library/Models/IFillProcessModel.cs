﻿using System;
using System.Collections.Generic;
using Libraries.Authentication.User;
using MT.Singularity.Platform.Devices.Scale;

namespace IND930.Library.Models
{
    public interface IFillProcessModel
    {
        WeightInformation InitialScaleValues { get; set; }
        WeightInformation TaredContainer { get; set; }
        IAuthentificatedUser LoggedInUser { get; set; }
        string CurrentComponent { get; set; }
        DateTime ProcessFinishTime { get; set; }
        DateTime ProcessStartTime { get; set; }
        List<ComponentModel> UsedComponents { get; set; }
        string Formula { get; set; }
        string BatchNumber { get; set; }
        double TotalFormulaNetWeight { get; }
        DateTime PrintTime { get; set; }
        DateTime TareTime { get; set; }
        bool HasBeenPrinted { get; set; }
        bool HasBeenManuallyTared { get; set; }
        string ScaleName { get; set; }
        string Location { get; set; }

    }
}