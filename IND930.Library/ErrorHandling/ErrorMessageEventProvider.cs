﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MT.Singularity.Composition;

namespace IND930.Library.ErrorHandling
{
    [Export(typeof(IErrorMessageEventProvider))]
    [InjectionBehavior(IsSingleton = true)]
    public class ErrorMessageEventProvider : IErrorMessageEventProvider
    {
        public event EventHandler<ErrorMessageEventArgs> ErrorMessageEvent;

        public void TriggerErrorMessageEvent(object sender, string errorMessage, Exception exception = null)
        {
            ErrorMessageEvent?.Invoke(sender, new ErrorMessageEventArgs(errorMessage, exception));
        }
    }
}
