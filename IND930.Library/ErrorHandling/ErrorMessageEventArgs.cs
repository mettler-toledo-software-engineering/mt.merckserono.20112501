﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IND930.Library.ErrorHandling
{
    public  class ErrorMessageEventArgs : EventArgs
    {
        public string ErrorMessage { get; }
        public Exception Exception { get; }

        public ErrorMessageEventArgs(string errorMessage, Exception exception = null)
        {
            ErrorMessage = errorMessage;
            Exception = exception;
        }
    }
}
