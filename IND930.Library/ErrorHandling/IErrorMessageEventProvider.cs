﻿using System;

namespace IND930.Library.ErrorHandling
{
    public interface IErrorMessageEventProvider
    {
        event EventHandler<ErrorMessageEventArgs> ErrorMessageEvent;

        void TriggerErrorMessageEvent(object sender, string errorMessage, Exception exception = null);
    }
}