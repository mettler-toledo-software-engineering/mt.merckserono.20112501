﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IND930.Library.Models;
using IND930.Library.Printing;
using Libraries.Peripherals.BarcodeScanner;
using Libraries.Peripherals.Printer;
using MT.Singularity;
using MT.Singularity.Platform.Devices.Scale;

namespace IND930.Library.FillProcess.States
{
    public class AddComponentState : ProcessState
    {
        private int _counter = 0;
        private readonly IPrinterGa46 _printer;
        private readonly IPrinterTemplateConfiguration _printerTemplateConfiguration;

        public AddComponentState(IPrinterGa46 printer, IPrinterTemplateConfiguration printerTemplateConfiguration)
        {
            _printer = printer;
            _printerTemplateConfiguration = printerTemplateConfiguration;
        }

        public override Task Init()
        {
            Context.ProcessState = ProcessStateEnum.AddComponent;
            Context.CanTareScale = true;
            Context.CanAddItem = true;
            Context.CanConfirm = false;
            Context.CanPresetTare = false;
            Context.FillProcess.CurrentComponent = string.Empty;
            _counter = Context.FillProcess.UsedComponents.Count;
            Context.CanPrintTotal = _counter > 0;
            Context.UpdateCanConfirmProcessOnUi(Context.CanConfirm);
            return Task.CompletedTask;
        }

        public override async Task ConfirmationCommand()
        {
            bool componentInList = Context.FillProcess.UsedComponents.Any(c => c.ComponentCounter == _counter + 1);

            if (string.IsNullOrWhiteSpace(Context.FillProcess.CurrentComponent) == false && componentInList == false)
            {
                var weight = await Context.ScaleService.SelectedScale.GetStableWeightAsync(UnitType.Display);
                var component = new ComponentModel { ComponentCounter = _counter+1, ComponentName = Context.FillProcess.CurrentComponent, ComponentWeight = weight, RegistrationTime = DateTime.Now};
                Context.FillProcess.UsedComponents.Add(component);
                _counter = Context.FillProcess.UsedComponents.Count;
                await Context.ScaleService.SelectedScale.TareAsync();
                TemplatePrinter.PrintComponent(Context.FillProcess, _printer, _printerTemplateConfiguration);
                
                if (_counter >= 15)
                {
                    Context.CanAddItem = false;
                    Context.CanPrintTotal = true;
                    
                    Context.UpdateCanConfirmProcessOnUi(Context.CanConfirm);
                }
                else
                {
                    await Context.TransitionTo(new AddComponentState(_printer,_printerTemplateConfiguration));
                }
            }
        }

        public override void GetBarcodeData(BarcodeDataReceivedEventArgs args)
        {
            if (string.IsNullOrWhiteSpace(args.Data) == false)
            {
                Context.FillProcess.CurrentComponent = args.Data;
              //  Context.CanConfirm = (await Context.ScaleService.SelectedScale.GetStableWeightAsync(UnitType.Display)).NetWeight > 0;
                Context.CanConfirm = true; // emmi
                Context.UpdateCanConfirmProcessOnUi(Context.CanConfirm);
            }
        }

        public override async Task PrintTotalCommand()
        {


            var printpositivstate = new PrintTotalPositiveState(_printer, _printerTemplateConfiguration);
            await Context.TransitionTo(printpositivstate);
            await printpositivstate.PrintTotalCommand();

        }

        internal override void GetScaleEvents(WeightInformation weight)
        {
            if (string.IsNullOrWhiteSpace(Context.FillProcess.CurrentComponent) == false)
            {
               // Context.CanConfirm = weight.NetWeight > 0;
                Context.CanConfirm = true; // emmi
                Context.UpdateCanConfirmProcessOnUi(Context.CanConfirm);
            }
        }

        public override Task TareCommand()
        {
            return Task.CompletedTask;
        }

    }
}
