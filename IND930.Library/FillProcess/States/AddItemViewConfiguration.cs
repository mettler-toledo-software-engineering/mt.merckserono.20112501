﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MT.Singularity.Composition;

namespace IND930.Library.FillProcess.States
{
    [Export(typeof(IAddItemViewConfiguration))]
    [InjectionBehavior(IsSingleton = true)]
    public class AddItemViewConfiguration : IAddItemViewConfiguration
    {
        public ProcessStateEnum CurrentProcessState { get; set; } = ProcessStateEnum.None;
    }
}
