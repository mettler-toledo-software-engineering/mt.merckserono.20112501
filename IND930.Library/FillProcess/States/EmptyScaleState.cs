﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IND930.Library.Printing;
using Libraries.Peripherals.BarcodeScanner;
using Libraries.Peripherals.Printer;
using MT.Singularity;
using MT.Singularity.Platform.Devices.Scale;

namespace IND930.Library.FillProcess.States
{
    public class EmptyScaleState : ProcessState
    {

        private readonly IPrinterGa46 _printer;
        private readonly IPrinterTemplateConfiguration _printerTemplateConfiguration;

        public EmptyScaleState(IPrinterGa46 printer, IPrinterTemplateConfiguration printerTemplateConfiguration)
        {
            _printer = printer;
            _printerTemplateConfiguration = printerTemplateConfiguration;
        }

        public override async Task Init()
        {
            Context.ProcessState = ProcessStateEnum.EmptyScale;
            Context.CanTareScale = false;
            Context.CanPresetTare = false;
            Context.CanAddItem = false;
            Context.CanPrintTotal = false;
            Context.CanConfirm = (await Context.ScaleService.SelectedScale.GetStableWeightAsync(UnitType.Display)).NetWeight < 10;
            Context.UpdateCanConfirmProcessOnUi(Context.CanConfirm);
        }

        public override async Task ConfirmationCommand()
        {// emmi
            Context.FillProcess.InitialScaleValues = await Context.ScaleService.SelectedScale.GetStableWeightAsync(UnitType.Display);
            if (Context.FillProcess.InitialScaleValues != null)
            {
                Context.FillProcess.ProcessStartTime = DateTime.Now;
                TemplatePrinter.PrintHeaderWithInitials(Context.FillProcess, _printer, _printerTemplateConfiguration);
                
                await Context.TransitionTo(new TareFormulaState(_printer, _printerTemplateConfiguration));
            }
        }

        public override void GetBarcodeData(BarcodeDataReceivedEventArgs args)
        {
        }

        public override Task PrintTotalCommand()
        {
            return Task.CompletedTask;
        }

        internal override async void GetScaleEvents(WeightInformation weight)
        {
            
            Context.CanConfirm = (await Context.ScaleService.SelectedScale.GetStableWeightAsync(UnitType.Display)).NetWeight < 10;
            Context.UpdateCanConfirmProcessOnUi(Context.CanConfirm);
        }

        public override Task TareCommand()
        {
            return Task.CompletedTask;
        }

    }
}
