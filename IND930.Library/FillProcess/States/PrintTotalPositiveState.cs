﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IND930.Library.Printing;
using Libraries.Peripherals.BarcodeScanner;
using Libraries.Peripherals.Printer;
using MT.Singularity;
using MT.Singularity.Platform.Devices.Scale;

namespace IND930.Library.FillProcess.States
{
    public class PrintTotalPositiveState : ProcessState
    {
        private readonly IPrinterGa46 _printer;
        private readonly IPrinterTemplateConfiguration _printerTemplateConfiguration;

        public PrintTotalPositiveState(IPrinterGa46 printer, IPrinterTemplateConfiguration printerTemplateConfiguration)
        {
            _printer = printer;
            _printerTemplateConfiguration = printerTemplateConfiguration;
        }

        public override Task Init()
        {
            Context.ProcessState = ProcessStateEnum.PrintTotalNet;
            Context.CanTareScale = true;
            Context.CanPresetTare = false;
            Context.CanAddItem = false;
            Context.CanPrintTotal = true;
            Context.CanConfirm = false;
            Context.UpdateCanConfirmProcessOnUi(Context.CanConfirm);

            Context.FillProcess.ProcessFinishTime = DateTime.Now;

            return Task.CompletedTask;
        }

        public override Task ConfirmationCommand()
        {
            return Task.CompletedTask;
        }

        public override void GetBarcodeData(BarcodeDataReceivedEventArgs args)
        {
        }

        public override async Task PrintTotalCommand()
        {
            var config = await Context.ScaleService.SelectedScale.GetConfigurationAsync();
            _printerTemplateConfiguration.ScaleIdentification = config.ScaleModel;
            _printerTemplateConfiguration.ScaleSerialNumber = config.SerialNumber;
            Context.FillProcess.PrintTime = DateTime.Now;

            if (Context.FillProcess.HasBeenPrinted)
            {
                TemplatePrinter.PrintTemplate(Context.FillProcess, _printer, _printerTemplateConfiguration);
            }
            else
            {
                TemplatePrinter.PrintFooter(Context.FillProcess, _printer, _printerTemplateConfiguration);
            }

            Context.FillProcess.HasBeenPrinted = true;

        }

        internal override void GetScaleEvents(WeightInformation weight)
        {
        }

        public override Task TareCommand()
        {
            return Task.CompletedTask;
        }

    }
}
