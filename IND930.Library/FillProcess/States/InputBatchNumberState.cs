﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IND930.Library.Printing;
using Libraries.Peripherals.BarcodeScanner;
using Libraries.Peripherals.Printer;
using MT.Singularity;
using MT.Singularity.Platform.Devices.Scale;
using MT.Singularity.Presentation.Controls;

namespace IND930.Library.FillProcess.States
{
    public class InputBatchNumberState : ProcessState
    {

        private readonly IPrinterGa46 _printer;
        private readonly IPrinterTemplateConfiguration _printerTemplateConfiguration;

        public InputBatchNumberState(IPrinterGa46 printer, IPrinterTemplateConfiguration printerTemplateConfiguration)
        {
            _printer = printer;
            _printerTemplateConfiguration = printerTemplateConfiguration;
        }
        public override Task Init()
        {
            Context.ProcessState = ProcessStateEnum.InputBatchNumber;
            Context.CanTareScale = false;
            Context.CanPresetTare = false;
            Context.CanAddItem = true;
            Context.CanPrintTotal = false;
            Context.CanConfirm = false;
            Context.CanConfirm = string.IsNullOrWhiteSpace(Context.FillProcess.BatchNumber) == false;
            Context.UpdateCanConfirmProcessOnUi(Context.CanConfirm);

            return Task.CompletedTask;
        }

        public override async Task ConfirmationCommand()
        {
            await Context.TransitionTo(new EmptyScaleState(_printer, _printerTemplateConfiguration));
        }

        public override void GetBarcodeData(BarcodeDataReceivedEventArgs barcodeData)
        {
            if (string.IsNullOrWhiteSpace(barcodeData.Data) == false)
            {
                Context.FillProcess.BatchNumber = barcodeData.Data;
                Context.CanConfirm = true;
                Context.UpdateCanConfirmProcessOnUi(Context.CanConfirm);
            }
        }

        public override Task PrintTotalCommand()
        {
            return Task.CompletedTask;
        }

        internal override void GetScaleEvents(WeightInformation weight)
        {
        }

        public override Task TareCommand()
        {
            return Task.CompletedTask;
        }


    }
}
