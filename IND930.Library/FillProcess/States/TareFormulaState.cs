﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using IND930.Library.Printing;
using Libraries.Peripherals.BarcodeScanner;
using Libraries.Peripherals.Printer;
using MT.Singularity.Platform.Devices.Scale;

namespace IND930.Library.FillProcess.States
{


    public class TareFormulaState : ProcessState
    {
        private readonly IPrinterGa46 _printer;
        private readonly IPrinterTemplateConfiguration _printerTemplateConfiguration;

        public TareFormulaState(IPrinterGa46 printer, IPrinterTemplateConfiguration printerTemplateConfiguration)
        {
            _printer = printer;
            _printerTemplateConfiguration = printerTemplateConfiguration;
        }
        public override Task Init()
        {
            Context.ProcessState = ProcessStateEnum.TareFormula;
            Context.CanTareScale = true;
            Context.CanAddItem = true;
            Context.CanPrintTotal = false;
            Context.CanConfirm = false;
            Context.UpdateCanConfirmProcessOnUi(Context.CanConfirm);
            return Task.CompletedTask;

        }

        public override async Task ConfirmationCommand()
        {
            try
            {
                Context.FillProcess.TareTime = DateTime.Now;
                TemplatePrinter.PrintTare(Context.FillProcess, _printer, _printerTemplateConfiguration);
                await Context.TransitionTo(new AddComponentState(_printer, _printerTemplateConfiguration));
            }
            catch
            {
                ;
            }

        }


        public override void GetBarcodeData(BarcodeDataReceivedEventArgs barcodeData)
        {
        }

        public override Task PrintTotalCommand()
        {
            return Task.CompletedTask;
        }

        internal override void GetScaleEvents(WeightInformation weight)
        {
            
        }

        public override async Task TareCommand()
        {
            var state = await Context.ScaleService.SelectedScale.TareAsync();
            if (state == WeightState.OK)
            {
                Context.FillProcess.TaredContainer =
                    await Context.ScaleService.SelectedScale.GetStableWeightAsync(UnitType.Display,-1);
                if (Context.FillProcess.TaredContainer.IsValid)
                {
                    Context.CanConfirm = true;
                    Context.UpdateCanConfirmProcessOnUi(Context.CanConfirm);
                }
            }
        }
    }
}
