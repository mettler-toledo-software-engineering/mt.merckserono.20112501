﻿namespace IND930.Library.FillProcess.States
{
    public interface IAddItemViewConfiguration
    {
        ProcessStateEnum CurrentProcessState { get; set; }
    }
}