﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IND930.Library.FillProcess.States
{
    public enum ProcessStateEnum
    {
        InputFormula,
        InputBatchNumber,
        EmptyScale,
        TareFormula,
        AddComponent,
        PrintTotalNet,
        None
    }
}
