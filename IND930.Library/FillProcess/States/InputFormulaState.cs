﻿using System;
using System.Threading.Tasks;
using IND930.Library.Printing;
using Libraries.Peripherals.BarcodeScanner;
using Libraries.Peripherals.Printer;
using MT.Singularity;
using MT.Singularity.Localization;
using MT.Singularity.Platform.Devices.Scale;
using MT.Singularity.Presentation.Controls;

namespace IND930.Library.FillProcess.States
{
    public class InputFormulaState : ProcessState
    {
        private readonly IPrinterGa46 _printer;
        private readonly IPrinterTemplateConfiguration _printerTemplateConfiguration;

        public InputFormulaState(IPrinterGa46 printer, IPrinterTemplateConfiguration printerTemplateConfiguration)
        {
            _printer = printer;
            _printerTemplateConfiguration = printerTemplateConfiguration;
        }

        public override async Task Init()
        {
            Context.ProcessState = ProcessStateEnum.InputFormula;
            Context.CanTareScale = false;
            Context.CanPresetTare = false;
            Context.CanAddItem = true;
            Context.CanPrintTotal = false;
            Context.CanConfirm = string.IsNullOrWhiteSpace(Context.FillProcess.Formula) == false ;
            Context.UpdateCanConfirmProcessOnUi(Context.CanConfirm);

            var config = await Context.ScaleService.SelectedScale.GetConfigurationAsync();
            _printerTemplateConfiguration.ScaleIdentification = config.ScaleModel;
            _printerTemplateConfiguration.ScaleSerialNumber = config.SerialNumber;
            
        }

        public override async Task ConfirmationCommand()
        {
            
            await Context.TransitionTo(new InputBatchNumberState(_printer, _printerTemplateConfiguration));

        }
        


        public override void GetBarcodeData(BarcodeDataReceivedEventArgs barcodeData)
        {
            
            if (string.IsNullOrWhiteSpace(barcodeData.Data) == false)
            {
                Context.FillProcess.Formula = barcodeData.Data;
                Context.CanConfirm = string.IsNullOrWhiteSpace(Context.FillProcess.Formula) == false;
                Context.UpdateCanConfirmProcessOnUi(true);
            }
            
        }

        public override Task PrintTotalCommand()
        {
            return Task.CompletedTask;
        }

        internal override void GetScaleEvents(WeightInformation weight)
        {
        }

        public override Task TareCommand()
        {
            return Task.CompletedTask;
        }

    }
}
