﻿using System;
using System.Threading.Tasks;
using IND930.Library.FillProcess.States;
using IND930.Library.Models;
using IND930.Library.Printing;
using Libraries.Peripherals.Printer;
using MT.Singularity.Platform.Devices.Scale;

namespace IND930.Library.FillProcess
{
    public interface IContext
    {
        bool CanTareScale { get; set; }
        bool CanAddItem { get; set; }
        bool CanPrintTotal { get; set; }
        bool CanConfirm { get; set; }
        bool CanPresetTare { get; set; }

        void UpdateCanConfirmProcessOnUi(bool canConfirmProcess);
        event EventHandler<CanConfirmProcessEventArg> CanConfirmProcessEvent;
        IScaleService ScaleService { get; set; }

        ProcessStateEnum ProcessState { get; set; }
        IFillProcessModel FillProcess { get; set; }
        Task TransitionTo(ProcessState processState);
        //void UpdatePropertiesOnUi();
        Task ConfirmationCommand();
        Task PrintTotalCommand();
        Task TareCommand();
    }
}