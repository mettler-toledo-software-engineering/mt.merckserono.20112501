﻿using System.Threading.Tasks;
using IND930.Library.Printing;
using Libraries.Peripherals.BarcodeScanner;
using Libraries.Peripherals.Printer;
using MT.Singularity;
using MT.Singularity.Platform.Devices.Scale;

namespace IND930.Library.FillProcess
{

    public abstract class ProcessState
    {
        
        protected IContext Context;

        public void SetContext(IContext context)
        {
            Context = context;
        }
        public abstract Task Init();
        
        public abstract Task ConfirmationCommand();

        public abstract Task PrintTotalCommand();
        public abstract Task TareCommand();
        public abstract void GetBarcodeData(BarcodeDataReceivedEventArgs args);
        internal abstract void GetScaleEvents(WeightInformation weight);
    }
}
