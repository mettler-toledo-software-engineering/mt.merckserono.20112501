﻿using System;
using System.Security.AccessControl;
using System.Threading;

namespace IND930.Library.FillProcess
{
    public class CanConfirmProcessEventArg : EventArgs
    {
        public bool CanConfirmProcess { get; set; }

        public CanConfirmProcessEventArg(bool canConfirmProcess)
        {
            CanConfirmProcess = canConfirmProcess;
        }
    }
}