﻿using System;
using System.Threading.Tasks;
using IND930.Library.Models;
using IND930.Library.FillProcess.States;
using IND930.Library.Printing;
using Libraries.Peripherals.BarcodeScanner;
using Libraries.Peripherals.Printer;
using MT.Singularity.Composition;
using MT.Singularity.Platform.Devices.Scale;
using MT.Singularity.Presentation.Controls;

namespace IND930.Library.FillProcess
{
    [Export(typeof(IContext))]
    [InjectionBehavior(IsSingleton = true)]
    public class Context : IContext
    {
        private ProcessState _processState = null;
        private IUsbBarcodeScanner _barcodeScanner;
        public IScaleService ScaleService { get; set; }
        public Context(IFillProcessModel fillprocess, IUsbBarcodeScanner barcodeScanner, IScaleService scaleService, IPrinterGa46 printer, IPrinterTemplateConfiguration printerTemplateConfiguration)
        {
            FillProcess = fillprocess;
            _barcodeScanner = barcodeScanner;
            ScaleService = scaleService;
            ScaleService.SelectedScale.NewChangedWeightInDisplayUnit += SelectedScaleOnNewChangedWeightInDisplayUnit;
            _barcodeScanner.DataReceivedEvent += BarcodeScannerOnDataReceivedEvent;
            TransitionTo(new InputFormulaState(printer, printerTemplateConfiguration)).ContinueWith(OnErrorTransition);
        }

        private void OnErrorTransition(Task obj)
        {
            if (obj.Exception != null)
            {
                Console.WriteLine(obj.Exception);
            }
        }

        private void SelectedScaleOnNewChangedWeightInDisplayUnit(WeightInformation weight)
        {
            _processState.GetScaleEvents(weight);
        }

        private void BarcodeScannerOnDataReceivedEvent(object sender, BarcodeDataReceivedEventArgs barcodeData)
        {
            _processState.GetBarcodeData(barcodeData);
        }

        public bool CanTareScale { get; set; }
        public bool CanAddItem { get; set; }
        public bool CanPrintTotal { get; set; }
        public bool CanConfirm { get; set; }

        public bool CanPresetTare { get; set; }
        public IFillProcessModel FillProcess { get; set; }
        public ProcessStateEnum ProcessState { get; set; }

        public event EventHandler<CanConfirmProcessEventArg> CanConfirmProcessEvent;

        public async Task TransitionTo(ProcessState processState)
        {
            
            _processState = processState;
            _processState.SetContext(this);
            await _processState.Init();
        }

        public void UpdateCanConfirmProcessOnUi(bool canConfirmProcess)
        {
            CanConfirmProcessEvent?.Invoke(this, new CanConfirmProcessEventArg(canConfirmProcess));
        }



        public async Task ConfirmationCommand()
        {
            await _processState.ConfirmationCommand();
        }

        public async Task PrintTotalCommand()
        {
            await _processState.PrintTotalCommand();
        }

        public async Task TareCommand()
        {
            await _processState.TareCommand();
        }
    }
}
