﻿namespace IND930.Library.Printing
{
    public interface IPrinterTemplateConfiguration
    {
        string RoomNumber { get; set; }
        string ScaleIdentification { get; set; }
        string ScaleSerialNumber { get; set; }
        string TerminalIdentification { get; set; }
        string TerminalSerialNumber { get; set; }
    }
}