﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iText;
using iText.IO.Font;
using iText.IO.Image;
using iText.Kernel.Font;
using iText.Kernel.Geom;
using iText.Kernel.Pdf;
using iText.Layout;
using iText.Layout.Element;
using iText.Pdfa;
using MT.Singularity.Presentation.Drawing;
using Path = System.IO.Path;

namespace IND930.Library.Printing
{
    public class PdfPrinter
    {
        private const string INTENT = @"C:\PDFBackup\sRGB_CS_profile.icm";
        private List<string> _linesToPrint;

        private const string Esc = "\x1b";
        private const string StartWidePrint = "\x0e";
        private const string StopWidePrint = "\x0f";
        private const string NormalHeight = "\x1bH2";
        private const string BigHeight = "\x1bH3";
        private const string Configuration = "K6^6";


        public PdfPrinter(List<string> linesToPrint)
        {
            _linesToPrint = new List<string>(linesToPrint);

        }

        public void CreatePdfDocument(string scaleIdentification, DateTime printTime)
        {
            string location = @"C:\PDFBackup";
            string pdffile = Path.Combine(location, $"{scaleIdentification}_{printTime:yyyyMMddHHmmss}.pdf");
            var pdfadocument = new PdfADocument(new PdfWriter(pdffile), PdfAConformanceLevel.PDF_A_1B, new PdfOutputIntent("Custom", "", "https://www.color.org", "sRGB IEC61966-2.1", new FileStream(INTENT, FileMode.Open, FileAccess.Read
             )));

            var document = new Document(pdfadocument);

            pdfadocument.SetTagged();
            //Fonts need to be embedded
            PdfFont font = PdfFontFactory.CreateFont("c:\\windows\\fonts\\arial.ttf", PdfEncodings.WINANSI, true);
            Style normalStyle = new Style();
            normalStyle.SetHeight(10);
            Paragraph paragraph = new Paragraph();
            normalStyle.SetFont(font).SetFontSize(10);
            foreach (var line in _linesToPrint)
            {
                var unformattedline = line.Replace(Esc, "").Replace(StartWidePrint, "").Replace(StopWidePrint, "").Replace(NormalHeight, "").Replace(BigHeight, "").Replace(Configuration,"");
                if (unformattedline.StartsWith("H2") || unformattedline.StartsWith("H3"))
                {
                    unformattedline = unformattedline.Substring(2);
                }
                if (unformattedline.EndsWith("H2") || unformattedline.EndsWith("H3"))
                {
                    unformattedline = unformattedline.Substring(0,unformattedline.Length-2);
                }
                paragraph.Add(new Text($"{unformattedline}\n").AddStyle(normalStyle));
            }
            
            document.Add(paragraph);
            //Set alt text


            document.Close();
            //using (var pdfwriter =  PdfWriter.GetInstance(document,filestream))
            //{
            //    pdfwriter.PDFXConformance = PdfWriter.PDFX1A2001;
            //    BaseFont basefont = BaseFont.CreateFont("c:\\windows\\fonts\\arial.ttf", BaseFont.WINANSI, true);
            //    Font font = new Font(basefont, 12);

            //    document.Open();
            //    document.AddAuthor("IND930");
            //    document.AddCreator("Sample application using iTextSharp");
            //    document.AddKeywords("PDF tutorial education");
            //    document.AddSubject("Document subject - Describing the steps creating a PDF document");
            //    document.AddTitle("The document title - PDF creation using iTextSharp");


            //    // Add a simple and wellknown phrase to the document in a flow layout manner  
            //    _linesToPrint.ForEach(line => document.Add(new Paragraph(line, font)));
            //    // Close the document  
            //    document.Close();
            //    // Close the writer instance  
            //    pdfwriter.Close();
            //    // Always close open filehandles explicity  
            //    filestream.Close();
            //}

        }

    }
}
