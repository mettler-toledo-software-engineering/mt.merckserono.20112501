﻿using System;
using System.Collections.Generic;
using System.Diagnostics.PerformanceData;
using System.Linq;
using IND930.Library.Helpers;
using IND930.Library.Models;
using Libraries.Peripherals.Printer;
using MT.Singularity.Platform.Translation;

namespace IND930.Library.Printing
{
    public static class TemplatePrinter
    {
        private const string Company = "Merck Serono SA";
        private const string FormulaTitle = "Produit :";
        private const string RoomNumberTitle = "Local:";
        private const string BatchNoTitle = "Numéro de Lot:";
        private const string ScaleSerialNoTitle = "No. série:";
        private const string ScaleTypeTitle = "Balance: ";
        private const string UserTitle = "Utilisateur";

        private const string StartDateTitle = "date début";
        private const string StartHourTitle = "heure début";
        private const string HourTitle = "heure";

        private const string EndDateTitle = "date fin";
        private const string EndHourTitle = "heure fin";
        private const string ImpressionTitle = "impression";


        private const string GrossTitle = "BRUT";
        private const string NetTitle = "NET";
        private const string TareTitle = "Tare";

        private const string TareRecipeTitle = "Tare récipient";
        private const string ComponentTitle = "Composant";

        private const string NumberComponentsTitle = "Nbre de composants";
        private const string ReceipeNetTitle = "Pesée nette";


        private static IFillProcessModel _fillProcess;
        private static IPrinterGa46 _printer;
        private static IPrinterTemplateConfiguration _printerTemplateConfiguration;

        private static List<string> _tempPrintBuffer = new List<string>();

        public static void PrintTestTemplate(IPrinterGa46 printerGa46)
        {

            _printer = printerGa46;
            _printer.WritePrinterSettings();
            _printer.PrintLnWide("äöüéàè".PadLeft(24, ' '));
            _printer.PrintLnWide("".PadLeft(24, '*'));

            _printer.PrintLnWide("".PadLeft(24, ' '));
            _printer.PrintLnWide("".PadLeft(24, ' '));
            _printer.PrintLnWide("".PadLeft(24, ' '));
            _printer.PrintLnWide("".PadLeft(24, ' '));
            _printer.PrintLnWide("".PadLeft(24, ' '));
            _printer.PrintLnWide("".PadLeft(24, ' '));

            _printer.ExecutePrint();
        }

        public static void PrintHeaderWithInitials(IFillProcessModel fillProcess, IPrinterGa46 printerGa46, IPrinterTemplateConfiguration printerTemplateConfiguration)
        {
            _tempPrintBuffer.Clear();

            _fillProcess = fillProcess;
            _printer = printerGa46;
            _printerTemplateConfiguration = printerTemplateConfiguration;
            _printer.WritePrinterSettings();
            PrintHeader();
            PrintInitialValues();

            PrintSpacingLines();
            _tempPrintBuffer.AddRange(_printer.GetPrintBuffer()); 

            _printer.ExecutePrint();
        }

        public static void PrintComponent(IFillProcessModel fillProcess, IPrinterGa46 printerGa46, IPrinterTemplateConfiguration printerTemplateConfiguration)
        {
            _fillProcess = fillProcess;
            _printer = printerGa46;
            _printerTemplateConfiguration = printerTemplateConfiguration;
            _printer.WritePrinterSettings();
            PrintLatestComponent();
            PrintSpacingLines();
            _tempPrintBuffer.AddRange(_printer.GetPrintBuffer());

            _printer.ExecutePrint();
        }

        public static void PrintTare(IFillProcessModel fillProcess, IPrinterGa46 printerGa46, IPrinterTemplateConfiguration printerTemplateConfiguration)
        {
            _fillProcess = fillProcess;
            _printer = printerGa46;
            _printerTemplateConfiguration = printerTemplateConfiguration;
            _printer.WritePrinterSettings();
            PrintTareReceipe();
            PrintSpacingLines();
            _tempPrintBuffer.AddRange(_printer.GetPrintBuffer());

            _printer.ExecutePrint();
        }

        public static void PrintFooter(IFillProcessModel fillProcess, IPrinterGa46 printerGa46, IPrinterTemplateConfiguration printerTemplateConfiguration)
        {
            _fillProcess = fillProcess;
            _printer = printerGa46;
            _printerTemplateConfiguration = printerTemplateConfiguration;
            _printer.WritePrinterSettings();
            PrintTotalNet();
            PrintEndDate();
            PrintSpacingLines();
            _tempPrintBuffer.AddRange(_printer.GetPrintBuffer());
            var pdfprinter = new PdfPrinter(_tempPrintBuffer);
            pdfprinter.CreatePdfDocument(_fillProcess.ScaleName, fillProcess.PrintTime);
            _printer.ExecutePrint();
        }
        public static void PrintTemplate(IFillProcessModel fillProcess, IPrinterGa46 printerGa46, IPrinterTemplateConfiguration printerTemplateConfiguration)
        {
            _fillProcess = fillProcess;
            _printer = printerGa46;
            _printerTemplateConfiguration = printerTemplateConfiguration;
            _printer.WritePrinterSettings();
            PrintHeader();
            PrintInitialValues();
            PrintTareReceipe();
            PrintComponents();
            PrintTotalNet();
            PrintEndDate();
            PrintSpacingLines();
            var printtemplate = _printer.GetPrintBuffer();
            var pdfprinter = new PdfPrinter(printtemplate);
            pdfprinter.CreatePdfDocument(_fillProcess.ScaleName, fillProcess.PrintTime);

            _printer.ExecutePrint();
        }

        private static void PrintHeader()
        {
            _printer.PrintLnWide(Company, false);
            _printer.PrintLnWide(RoomNumberTitle);
            _printer.PrintLnWide(_fillProcess.Location);
            _printer.PrintLnWide("".PadLeft(24, ' '));
            _printer.PrintLnWide(ScaleTypeTitle);
            _printer.PrintLnWideRight(_fillProcess.ScaleName);
            _printer.PrintLnWideRight(_printerTemplateConfiguration.ScaleIdentification);
            _printer.PrintLnWide($"{ScaleSerialNoTitle} {_printerTemplateConfiguration.ScaleSerialNumber}");
            _printer.PrintLnWide(FormulaTitle);
            _printer.PrintLnWideRight(_fillProcess.Formula);
            _printer.PrintLnWide(BatchNoTitle);
            _printer.PrintLnWide(_fillProcess.BatchNumber);
            _printer.PrintLnWide(UserTitle);
            _printer.PrintLnWideRight(_fillProcess.LoggedInUser.DisplayName);

            _printer.PrintLnWide(_printer.CombineTitleAndValue(StartDateTitle, _fillProcess.ProcessStartTime.ToString("dd.MM.yyyy")));
            _printer.PrintLnWide(_printer.CombineTitleAndValue(StartHourTitle, _fillProcess.ProcessStartTime.ToString("HH:mm:ss")));

            _printer.PrintLnWide("".PadLeft(24, '*'));
        }

        private static void PrintInitialValues()
        {
            _printer.PrintLnWide("".PadLeft(24, ' '));
            _printer.PrintLnWide(_printer.CombineTitleAndValue(GrossTitle, $"{_fillProcess.InitialScaleValues.GrossWeight.ToString(RecipeFormatter.RecipeDecimalPlaceFormatting)} {_fillProcess.InitialScaleValues.Unit.ToTranslatedAbbreviation()}"));
            _printer.PrintLnWide(_printer.CombineTitleAndValue(NetTitle, $"{_fillProcess.InitialScaleValues.NetWeight.ToString(RecipeFormatter.RecipeDecimalPlaceFormatting)} {_fillProcess.InitialScaleValues.Unit.ToTranslatedAbbreviation()}"));
            _printer.PrintLnWide(_printer.CombineTitleAndValue(TareTitle, $"{_fillProcess.InitialScaleValues.TareWeight.ToString(RecipeFormatter.RecipeDecimalPlaceFormatting)} {_fillProcess.InitialScaleValues.Unit.ToTranslatedAbbreviation()}"));

            _printer.PrintLnWide("".PadLeft(24, '*'));
        }

        public static void PrintTareReceipe()
        {
            _printer.PrintLnWide("".PadLeft(24, ' '));
            _printer.PrintLnWide(TareRecipeTitle);
            _printer.PrintLnWide(_printer.CombineTitleAndValue(HourTitle, _fillProcess.TareTime.ToString("HH:mm:ss")));
            _printer.PrintLnWideRight($"{_fillProcess.TaredContainer.TareWeight.ToString(RecipeFormatter.RecipeDecimalPlaceFormatting)} {_fillProcess.TaredContainer.Unit.ToTranslatedAbbreviation()}");
            _printer.PrintEmptyLine();
            _printer.PrintLnWide("".PadLeft(24, '*'));
        }

        private static void PrintComponents()
        {

            foreach (var component in _fillProcess.UsedComponents)
            {
                _printer.PrintLnWide("".PadLeft(24, ' '));
                _printer.PrintLnWide(_printer.CombineTitleAndValue(ComponentTitle, $"{component.ComponentCounter}"));
                _printer.PrintLnWideRight(component.ComponentName);
                _printer.PrintLnWide(_printer.CombineTitleAndValue(HourTitle, component.RegistrationTime.ToString("HH:mm:ss")));
                _printer.PrintWide(_printer.CombineTitleAndValue(NetTitle, $"{component.ComponentWeight.NetWeight.ToString(RecipeFormatter.RecipeDecimalPlaceFormatting)} {component.ComponentWeight.Unit.ToTranslatedAbbreviation()}"));
                _printer.PrintLnWide("".PadLeft(24, '*'));
            }
        }
        public static void PrintLatestComponent()
        {

            var component = _fillProcess.UsedComponents.Last();

            _printer.PrintLnWide("".PadLeft(24, ' '));
            _printer.PrintLnWide(_printer.CombineTitleAndValue(ComponentTitle, $"{component.ComponentCounter}"));
            _printer.PrintLnWideRight(component.ComponentName);
            _printer.PrintLnWide(_printer.CombineTitleAndValue(HourTitle, component.RegistrationTime.ToString("HH:mm:ss")));
            _printer.PrintWide(_printer.CombineTitleAndValue(NetTitle, $"{component.ComponentWeight.NetWeight.ToString(RecipeFormatter.RecipeDecimalPlaceFormatting)} {component.ComponentWeight.Unit.ToTranslatedAbbreviation()}"));
            _printer.PrintLnWide("".PadLeft(24, '*'));

        }

        private static void PrintTotalNet()
        {
            _printer.PrintLnWide("".PadLeft(24, ' '));
            _printer.PrintLnWide(_printer.CombineTitleAndValue(NumberComponentsTitle, $"{_fillProcess.UsedComponents.Count}"));
            _printer.PrintLnWide(ReceipeNetTitle, true);
            _printer.PrintLnWideRight($"{_fillProcess.TotalFormulaNetWeight.ToString(RecipeFormatter.RecipeDecimalPlaceFormatting)} {_fillProcess.InitialScaleValues.Unit.ToTranslatedAbbreviation()}", true);
            _printer.PrintLnWide("".PadLeft(24, '*'));
        }


        private static void PrintEndDate()
        {
            _printer.PrintLnWide(_printer.CombineTitleAndValue(EndDateTitle, _fillProcess.ProcessFinishTime.ToString("dd.MM.yyyy")));
            _printer.PrintLnWide(_printer.CombineTitleAndValue(EndHourTitle, _fillProcess.ProcessFinishTime.ToString("HH:mm:ss")));

            _printer.PrintLnWide("".PadLeft(24, ' '));

            _printer.PrintLnWide(_printer.CombineTitleAndValue(ImpressionTitle, _fillProcess.PrintTime.ToString("dd.MM.yyyy")));
            _printer.PrintLnWide(_printer.CombineTitleAndValue(string.Empty, _fillProcess.PrintTime.ToString("HH:mm:ss")));
            
        }

        private static void PrintSpacingLines()
        {
            _printer.PrintLnWide("".PadLeft(24, ' '));
            _printer.PrintLnWide("".PadLeft(24, ' '));
            _printer.PrintLnWide("".PadLeft(24, ' '));
            _printer.PrintLnWide("".PadLeft(24, ' '));
            _printer.PrintLnWide("".PadLeft(24, ' '));
        }
    }
}

