﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MT.Singularity.Composition;

namespace IND930.Library.Printing
{
    [Export(typeof(IPrinterTemplateConfiguration))]
    [InjectionBehavior(IsSingleton = true)]
    public class PrinterTemplateConfiguration : IPrinterTemplateConfiguration
    {
        public string ScaleIdentification { get; set; }
        public string ScaleSerialNumber { get; set; }
        public string TerminalIdentification { get; set; }
        public string TerminalSerialNumber { get; set; }
        public string RoomNumber { get; set; }

    }
}
