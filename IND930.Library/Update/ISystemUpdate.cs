﻿namespace IND930.Library.Update
{

    public interface ISystemUpdate
    {
        bool ExecuteUpdate();
    }
}