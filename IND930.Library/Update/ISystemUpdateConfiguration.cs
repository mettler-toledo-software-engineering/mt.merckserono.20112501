﻿namespace IND930.Library.Update
{
    
    public interface ISystemUpdateConfiguration
    {
        string SourceDirectory { get; set; }
        string TargetDirectory { get; set; }
    }
}