﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MT.Singularity.Composition;

namespace IND930.Library.Update
{
    [Export(typeof(ISystemUpdateConfiguration))]
    [InjectionBehavior(IsSingleton = true)]
    public class SystemUpdateConfiguration : ISystemUpdateConfiguration
    {
        public string SourceDirectory { get; set; }
        public string TargetDirectory { get; set; }
    }
}
