﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using MT.Singularity.Composition;
using MT.Singularity.Logging;
using MT.Singularity.Platform;

namespace IND930.Library.Update
{
    [Export(typeof(ISystemUpdate))]
    [InjectionBehavior(IsSingleton = true)]
    public class SystemUpdate : ISystemUpdate
    {
        private ISystemUpdateConfiguration _updateConfiguration;

        private  readonly ILog Logger = Log4NetManager.ApplicationLogger;
        private  readonly string SourceClass = nameof(SystemUpdate);

        private string _destinationPath;
        private  List<string> _availableUpdateFiles = new List<string>();


        private  string _systemUpdateSourceDirectory;

        

        public SystemUpdate(ISystemUpdateConfiguration updateConfiguration)
        {
            _updateConfiguration = updateConfiguration;
            _destinationPath = _updateConfiguration.TargetDirectory;
            _systemUpdateSourceDirectory = _updateConfiguration.SourceDirectory;
        }

        private bool SystemUpdateFileExists()
        {
            if (CheckDirectoriesExists())
            {
                _availableUpdateFiles = Directory.EnumerateFiles(_systemUpdateSourceDirectory).Select(Path.GetFileName).Where(x => x.Contains(SingularityEnvironment.AppPackageExtension)).ToList();
                bool hasFiles = _availableUpdateFiles.Count > 0;
                return hasFiles;
            }

            return false;
        }

        public bool ExecuteUpdate()
        {
            bool result = false;
            if (SystemUpdateFileExists())
            {
                foreach (string updateFile in _availableUpdateFiles)
                {
                    result = CopyFile(updateFile);
                    if (result == false)
                    {
                        break;
                    }
                }

            }

            return result;
        }

        private bool CopyFile(string filename)
        {
            string sourcefile = Path.Combine(_systemUpdateSourceDirectory, filename);
            string destinationfile = Path.Combine(_destinationPath, filename);
            bool result = false;
            try
            {
                File.Copy(sourcefile, destinationfile, true);
                result = true;
            }
            catch (IOException ex)
            {
                Logger.ErrorEx($"could not copy File to Destination Directory. FileName: {filename}, Source: {_systemUpdateSourceDirectory}, Destination: {_destinationPath}", SourceClass, ex);
            }

            return result;
        }


        private bool CheckDirectoriesExists()
        {
            bool result = true;
            if (Directory.Exists(_destinationPath) == false)
            {
                result = CreateDirectory(_destinationPath);

            }

            if (Directory.Exists(_systemUpdateSourceDirectory) == false)
            {
                result = CreateDirectory(_systemUpdateSourceDirectory);
            }

            return result;
        }

        private bool CreateDirectory(string path)
        {
            bool result = false;
            try
            {
                Directory.CreateDirectory(path);
                result = true;
            }
            catch (IOException ex)
            {
                Logger.ErrorEx($"Update Destination Directory could not be created: {path}", SourceClass, ex);
                result = false;
            }

            return result;
        }
    }
}
