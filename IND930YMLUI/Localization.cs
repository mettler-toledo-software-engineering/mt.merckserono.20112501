﻿// <auto-generated/>

using System;
using System.Collections.Generic;
using MT.Singularity.Collections;
using MT.Singularity.Localization;

namespace IND930YMLUI 
{
	[System.CodeDom.Compiler.GeneratedCodeAttribute("MT.Singularity.Environment.Yml.Localization.LanguageXmlTemplate", "2.1.0.0")]
	[TranslationModule("de-DE", "en-US", "fr-FR", Strings = 80)]
	public class Localization
	{	
		internal static readonly TranslationModule Module = new ModuleClass();

		public enum Key
		{
			/// <summary>
			/// Bitte Batch Nummer eingeben
			/// </summary>
			AddItemTextBatchNo,
			/// <summary>
			/// Bitte Komponente eingeben
			/// </summary>
			AddItemTextComponent,
			/// <summary>
			/// Bitte Produkt eingeben
			/// </summary>
			AddItemTextFormula,
			/// <summary>
			/// Bitte manuelles Tara eingeben
			/// </summary>
			AddItemTextTare,
			/// <summary>
			/// Achtung!
			/// </summary>
			Attention,
			/// <summary>
			/// BarcodeScannerPort
			/// </summary>
			BarcodeScannerPort,
			/// <summary>
			/// Batch No
			/// </summary>
			BatchNo,
			/// <summary>
			/// Brutto
			/// </summary>
			Brutto,
			/// <summary>
			/// Abbrechen
			/// </summary>
			Cancel,
			/// <summary>
			/// Wollen Sie das aktuelle Rezept wirklich abbrechen?
			/// </summary>
			CancelProcess1,
			/// <summary>
			/// Alle Arbeitsschritte werden verworfen.
			/// </summary>
			CancelProcess2,
			/// <summary>
			/// überprüfe credentials, bitte warten...
			/// </summary>
			CheckingCredentials,
			/// <summary>
			/// Komponente
			/// </summary>
			Component,
			/// <summary>
			/// Bestätigen
			/// </summary>
			Confirm,
			/// <summary>
			/// Aktuelle Komponente
			/// </summary>
			CurrentComponent,
			/// <summary>
			/// Anwendung
			/// </summary>
			CustomApplication,
			/// <summary>
			/// Domain Group
			/// </summary>
			DomainGroup,
			/// <summary>
			/// Domain Name
			/// </summary>
			DomainName,
			/// <summary>
			/// Drucker Config
			/// </summary>
			DruckerConfig,
			/// <summary>
			/// Einstellungen
			/// </summary>
			Einstellungen,
			/// <summary>
			/// Error
			/// </summary>
			Error,
			/// <summary>
			/// Beenden
			/// </summary>
			Exit,
			/// <summary>
			/// Abschliessen und Drucken
			/// </summary>
			Finish,
			/// <summary>
			/// Produkt
			/// </summary>
			Formel,
			/// <summary>
			/// FTP Server Ip:
			/// </summary>
			FtpServerIp,
			/// <summary>
			/// FTP User Name:
			/// </summary>
			FtpUserName,
			/// <summary>
			/// FTP User Password:
			/// </summary>
			FtpUserPassword,
			/// <summary>
			/// General Settings
			/// </summary>
			GeneralSettings,
			/// <summary>
			/// Grundzustand
			/// </summary>
			Grundzustand,
			/// <summary>
			/// Int. Order No
			/// </summary>
			InternalOrderNumber,
			/// <summary>
			/// Ja
			/// </summary>
			Ja,
			/// <summary>
			/// Kein USB-Stick erkannt.
			/// </summary>
			KeinUSBStickErkannt,
			/// <summary>
			/// Standort
			/// </summary>
			Location,
			/// <summary>
			/// Löschen
			/// </summary>
			Loeschen,
			/// <summary>
			/// Login Fehlgeschlagen
			/// </summary>
			LoginFailed,
			/// <summary>
			/// Auto Logout in min
			/// </summary>
			LogoutTime,
			/// <summary>
			/// Merck Serono Setup
			/// </summary>
			MainSetupNode,
			/// <summary>
			/// Eigene Referenzmenge:
			/// </summary>
			ManulRefQuantity,
			/// <summary>
			/// Berechnetes Referenzgewicht:
			/// </summary>
			ManulRefWeight,
			/// <summary>
			/// Material No
			/// </summary>
			MatNo,
			/// <summary>
			/// Name
			/// </summary>
			Name,
			/// <summary>
			/// Nein
			/// </summary>
			Nein,
			/// <summary>
			/// Netto
			/// </summary>
			Netto,
			/// <summary>
			/// aktuelles Nettogewicht:
			/// </summary>
			NettoLive,
			/// <summary>
			/// Neues Passwort
			/// </summary>
			NewPassword,
			/// <summary>
			/// Passwort wiederholen
			/// </summary>
			NewPasswordRepeat,
			/// <summary>
			/// Ok
			/// </summary>
			Ok,
			/// <summary>
			/// Passwort
			/// </summary>
			Password,
			/// <summary>
			/// Bitte zuerst einloggen
			/// </summary>
			PleaseLoginFirst,
			/// <summary>
			/// Drucker Port:
			/// </summary>
			PrinterPort,
			/// <summary>
			/// Produkt Ende:
			/// </summary>
			ProductEnd,
			/// <summary>
			/// Produkt Start:
			/// </summary>
			ProductStart,
			/// <summary>
			/// Rezept Nachkommastellen:
			/// </summary>
			RecipeDecimalPlaces,
			/// <summary>
			/// Raum Nummer
			/// </summary>
			RoomNumber,
			/// <summary>
			/// SAP Waagen Nummer:
			/// </summary>
			SapScaleNumber,
			/// <summary>
			/// SAP Settings
			/// </summary>
			SapSettings,
			/// <summary>
			/// Waagenbezeichnung
			/// </summary>
			ScaleName,
			/// <summary>
			/// Scoreboard Digits
			/// </summary>
			ScoreBoardDigits,
			/// <summary>
			/// Scoreboard Port
			/// </summary>
			ScoreBoardPort,
			/// <summary>
			/// Setup
			/// </summary>
			Setup,
			/// <summary>
			/// Start
			/// </summary>
			Start,
			/// <summary>
			/// Start Datum
			/// </summary>
			StartDate,
			/// <summary>
			/// Bitte Batch No scannen und anschliessend bestätigen
			/// </summary>
			StateTextBatchNo,
			/// <summary>
			/// Bitte Komponente scannen / eingeben, anschliessend in den
			/// </summary>
			StateTextComponent1,
			/// <summary>
			/// Behälter füllen und Netto Gewicht bestätigen
			/// </summary>
			StateTextComponent2,
			/// <summary>
			/// Bitte bestätigen Sie dass die Waage Leer ist
			/// </summary>
			StateTextEmptyScale,
			/// <summary>
			/// Bitte Produkt scannen und anschliessend bestätigen
			/// </summary>
			StateTextFormula,
			/// <summary>
			/// Das Rezept wurde erfolgreich beendet.
			/// </summary>
			StateTextPrintTotal,
			/// <summary>
			/// Bitte stellen Sie den Behälter für das Rezept auf die Waage
			/// </summary>
			StateTextTareScale1,
			/// <summary>
			/// und tarieren Sie diese und anschliessend bestätigen.
			/// </summary>
			StateTextTareScale2,
			/// <summary>
			/// Tara
			/// </summary>
			TareFormula,
			/// <summary>
			/// Manuelles Tara
			/// </summary>
			TareManuel,
			/// <summary>
			/// Terminal config
			/// </summary>
			TerminalSubNode,
			/// <summary>
			/// Test
			/// </summary>
			Test,
			/// <summary>
			/// Test Result
			/// </summary>
			TestResult,
			/// <summary>
			/// Total Netto Gewicht
			/// </summary>
			TotalNetWeight,
			/// <summary>
			/// System Update
			/// </summary>
			Update,
			/// <summary>
			/// Update Status
			/// </summary>
			UpdateStatus,
			/// <summary>
			/// Benutzername
			/// </summary>
			Username,
			/// <summary>
			/// Waage
			/// </summary>
			Waage,
		}

		public static string Get(Key key)
		{
			return LocalizationManager.GetTranslation(Module, (int)key);
		}

		public static string Format(Key key, string arg0)
		{
			return string.Format(LocalizationManager.GetTranslation(Module, (int)key), arg0);
		}

		public static string Format(Key key, string arg0, string arg1)
		{
			return string.Format(LocalizationManager.GetTranslation(Module, (int)key), arg0, arg1);
		}

		public static string Format(Key key, params string[] args)
		{
			return string.Format(LocalizationManager.GetTranslation(Module, (int)key), args);
		}

		public static TranslationModule GetTranslationModule()
		{
			return Module;
		}

		private class ModuleClass : TranslationModule
		{
			private readonly IImmutableIndexable<string> languages = Indexable.ImmutableValues("de-DE", "en-US", "fr-FR");

			public override string PrimaryLanguage
			{
				get { return "de-DE"; }
			}

			public override IEnumerable<string> SupportedLanguages
			{
				get { return languages; }
			}

			public override string Name 
			{
				get { return "IND930YMLUI.Localization"; }
			}

			public override ITranslationProvider GetTranslation(string language)
			{
				if (language == "de-DE")
					return new TranslationDE_DE();
				if (language == "en-US")
					return new TranslationEN_US();
				if (language == "fr-FR")
					return new TranslationFR_FR();
				return null;
			}

			public override Dictionary<string, int> GetKeyNames()
			{
                Dictionary<string, int> keyNames = new Dictionary<string, int>();
				for (int i = 0; i < 80; i++)
				{
					keyNames.Add(((Key)i).ToString(), i);
				}
                return keyNames;
			}
		}

		private class TranslationDE_DE : ITranslationProvider
		{
			public string Get(int key) 
			{
				switch((Key)key)
				{
					case Key.Attention:
						return "Achtung!";
					case Key.BarcodeScannerPort:
						return "BarcodeScannerPort";
					case Key.BatchNo:
						return "Batch No";
					case Key.Brutto:
						return "Brutto";
					case Key.Confirm:
						return "Bestätigen";
					case Key.Cancel:
						return "Abbrechen";
					case Key.CancelProcess1:
						return "Wollen Sie das aktuelle Rezept wirklich abbrechen?";
					case Key.CancelProcess2:
						return "Alle Arbeitsschritte werden verworfen.";
					case Key.CheckingCredentials:
						return "überprüfe credentials, bitte warten...";
					case Key.CurrentComponent:
						return "Aktuelle Komponente";
					case Key.CustomApplication:
						return "Anwendung";
					case Key.Component:
						return "Komponente";
					case Key.DomainName:
						return "Domain Name";
					case Key.DomainGroup:
						return "Domain Group";
					case Key.DruckerConfig:
						return "Drucker Config";
					case Key.Einstellungen:
						return "Einstellungen";
					case Key.Error:
						return "Error";
					case Key.Exit:
						return "Beenden";
					case Key.Finish:
						return "Abschliessen und Drucken";
					case Key.Formel:
						return "Produkt";
					case Key.FtpUserName:
						return "FTP User Name:";
					case Key.FtpUserPassword:
						return "FTP User Password:";
					case Key.FtpServerIp:
						return "FTP Server Ip:";
					case Key.GeneralSettings:
						return "General Settings";
					case Key.Grundzustand:
						return "Grundzustand";
					case Key.InternalOrderNumber:
						return "Int. Order No";
					case Key.Ja:
						return "Ja";
					case Key.KeinUSBStickErkannt:
						return "Kein USB-Stick erkannt.";
					case Key.Loeschen:
						return "Löschen";
					case Key.Location:
						return "Standort";
					case Key.LoginFailed:
						return "Login Fehlgeschlagen";
					case Key.LogoutTime:
						return "Auto Logout in min";
					case Key.MainSetupNode:
						return "Merck Serono Setup";
					case Key.ManulRefQuantity:
						return "Eigene Referenzmenge:";
					case Key.ManulRefWeight:
						return "Berechnetes Referenzgewicht:";
					case Key.MatNo:
						return "Material No";
					case Key.Name:
						return "Name";
					case Key.Nein:
						return "Nein";
					case Key.Netto:
						return "Netto";
					case Key.NettoLive:
						return "aktuelles Nettogewicht:";
					case Key.Ok:
						return "Ok";
					case Key.Password:
						return "Passwort";
					case Key.NewPassword:
						return "Neues Passwort";
					case Key.NewPasswordRepeat:
						return "Passwort wiederholen";
					case Key.PleaseLoginFirst:
						return "Bitte zuerst einloggen";
					case Key.PrinterPort:
						return "Drucker Port:";
					case Key.ProductStart:
						return "Produkt Start:";
					case Key.ProductEnd:
						return "Produkt Ende:";
					case Key.RecipeDecimalPlaces:
						return "Rezept Nachkommastellen:";
					case Key.RoomNumber:
						return "Raum Nummer";
					case Key.SapScaleNumber:
						return "SAP Waagen Nummer:";
					case Key.SapSettings:
						return "SAP Settings";
					case Key.ScaleName:
						return "Waagenbezeichnung";
					case Key.ScoreBoardPort:
						return "Scoreboard Port";
					case Key.ScoreBoardDigits:
						return "Scoreboard Digits";
					case Key.Setup:
						return "Setup";
					case Key.StateTextBatchNo:
						return "Bitte Batch No scannen und anschliessend bestätigen";
					case Key.StateTextEmptyScale:
						return "Bitte bestätigen Sie dass die Waage Leer ist";
					case Key.StateTextFormula:
						return "Bitte Produkt scannen und anschliessend bestätigen";
					case Key.StateTextPrintTotal:
						return "Das Rezept wurde erfolgreich beendet.";
					case Key.StateTextTareScale1:
						return "Bitte stellen Sie den Behälter für das Rezept auf die Waage";
					case Key.StateTextTareScale2:
						return "und tarieren Sie diese und anschliessend bestätigen.";
					case Key.StateTextComponent1:
						return "Bitte Komponente scannen / eingeben, anschliessend in den";
					case Key.StateTextComponent2:
						return "Behälter füllen und Netto Gewicht bestätigen";
					case Key.AddItemTextFormula:
						return "Bitte Produkt eingeben";
					case Key.AddItemTextBatchNo:
						return "Bitte Batch Nummer eingeben";
					case Key.AddItemTextComponent:
						return "Bitte Komponente eingeben";
					case Key.AddItemTextTare:
						return "Bitte manuelles Tara eingeben";
					case Key.Start:
						return "Start";
					case Key.StartDate:
						return "Start Datum";
					case Key.TareFormula:
						return "Tara";
					case Key.TareManuel:
						return "Manuelles Tara";
					case Key.TerminalSubNode:
						return "Terminal config";
					case Key.Test:
						return "Test";
					case Key.TestResult:
						return "Test Result";
					case Key.TotalNetWeight:
						return "Total Netto Gewicht";
					case Key.UpdateStatus:
						return "Update Status";
					case Key.Update:
						return "System Update";
					case Key.Username:
						return "Benutzername";
					case Key.Waage:
						return "Waage";
					default:
						return string.Empty;
				}
			}

			public string Language
			{
				get { return "de-DE"; }
			}
		}
		private class TranslationEN_US : ITranslationProvider
		{
			public string Get(int key) 
			{
				switch((Key)key)
				{
					case Key.Attention:
						return "Attention!";
					case Key.BarcodeScannerPort:
						return "Port du lecteur de codes-barres";
					case Key.BatchNo:
						return "Batch No";
					case Key.Brutto:
						return "Gross";
					case Key.Confirm:
						return "Confirm";
					case Key.Cancel:
						return "Cancel";
					case Key.CancelProcess1:
						return "Do you really want to cancel the current process?";
					case Key.CancelProcess2:
						return "All saved process steps will be discarded.";
					case Key.CheckingCredentials:
						return "checking credentials, please wait...";
					case Key.CurrentComponent:
						return "Current Component";
					case Key.CustomApplication:
						return "Application";
					case Key.Component:
						return "Component";
					case Key.DomainName:
						return "Domain Name";
					case Key.DomainGroup:
						return "Domain Group";
					case Key.DruckerConfig:
						return "Printer Config";
					case Key.Einstellungen:
						return "Settings";
					case Key.Error:
						return "Error";
					case Key.Exit:
						return "Finish";
					case Key.Finish:
						return "Finish and Print";
					case Key.Formel:
						return "Product";
					case Key.FtpUserName:
						return "FTP User Name:";
					case Key.FtpUserPassword:
						return "FTP User Password:";
					case Key.FtpServerIp:
						return "FTP Server Ip:";
					case Key.GeneralSettings:
						return "General Settings";
					case Key.Grundzustand:
						return "Basic state";
					case Key.InternalOrderNumber:
						return "Int. Order No";
					case Key.Ja:
						return "Yes";
					case Key.KeinUSBStickErkannt:
						return "No USB drive found";
					case Key.Loeschen:
						return "Delete";
					case Key.Location:
						return "Location";
					case Key.LoginFailed:
						return "Login Failed";
					case Key.LogoutTime:
						return "Auto Logout in min";
					case Key.MainSetupNode:
						return "Merck Serono Setup";
					case Key.ManulRefQuantity:
						return "Own Reference Quantity:";
					case Key.ManulRefWeight:
						return "Calculated Ref Weight:";
					case Key.MatNo:
						return "Material No";
					case Key.Name:
						return "Name";
					case Key.Nein:
						return "No";
					case Key.Netto:
						return "Net";
					case Key.NettoLive:
						return "current net weight:";
					case Key.Ok:
						return "Ok";
					case Key.Password:
						return "Password ";
					case Key.NewPassword:
						return "New Password";
					case Key.NewPasswordRepeat:
						return "Repeat Password";
					case Key.PleaseLoginFirst:
						return "Please login first";
					case Key.PrinterPort:
						return "Printer Port:";
					case Key.ProductStart:
						return "Product Start:";
					case Key.ProductEnd:
						return "Product End:";
					case Key.RecipeDecimalPlaces:
						return "Recipe Decimal Places:";
					case Key.RoomNumber:
						return "RoomNumber:";
					case Key.SapScaleNumber:
						return "SAP Scale Number:";
					case Key.SapSettings:
						return "SAP Settings";
					case Key.ScaleName:
						return "Scale Name";
					case Key.ScoreBoardPort:
						return "Scoreboard Port";
					case Key.ScoreBoardDigits:
						return "Scoreboard Digits";
					case Key.Setup:
						return "Setup";
					case Key.StateTextBatchNo:
						return "Please scan batch number and confirm afterwards";
					case Key.StateTextEmptyScale:
						return "Please confirm that the scale is empty";
					case Key.StateTextFormula:
						return "Please scan product and confirm afterwards";
					case Key.StateTextPrintTotal:
						return "The recipe was successfully completed.";
					case Key.StateTextTareScale1:
						return "Please place the container for the recipe on the scales and";
					case Key.StateTextTareScale2:
						return "tare it and confirm afterwards.";
					case Key.StateTextComponent1:
						return "Please scan / enter component, then fill it into the";
					case Key.StateTextComponent2:
						return "container and confirm net weight";
					case Key.AddItemTextFormula:
						return "Please enter product";
					case Key.AddItemTextBatchNo:
						return "Please enter batch number";
					case Key.AddItemTextComponent:
						return "Please enter component";
					case Key.AddItemTextTare:
						return "Please enter manuel tare";
					case Key.Start:
						return "Start";
					case Key.StartDate:
						return "Start Date";
					case Key.TareFormula:
						return "Tare";
					case Key.TareManuel:
						return "manuel Tare";
					case Key.TerminalSubNode:
						return "Station config";
					case Key.Test:
						return "Test";
					case Key.TestResult:
						return "Test Result";
					case Key.TotalNetWeight:
						return "Total Net Weight";
					case Key.UpdateStatus:
						return "Update Status";
					case Key.Update:
						return "System Update";
					case Key.Username:
						return "Username";
					case Key.Waage:
						return "Scale";
					default:
						return string.Empty;
				}
			}

			public string Language
			{
				get { return "en-US"; }
			}
		}
		private class TranslationFR_FR : ITranslationProvider
		{
			public string Get(int key) 
			{
				switch((Key)key)
				{
					case Key.Attention:
						return "Attention!";
					case Key.BarcodeScannerPort:
						return "Port du lecteur de codes-barres";
					case Key.BatchNo:
						return "Numéro de lot/Solution";
					case Key.Brutto:
						return "Brut";
					case Key.Confirm:
						return "Confirmer";
					case Key.Cancel:
						return "Annuler";
					case Key.CancelProcess1:
						return "Vous voulez vraiment annuler la recette actuelle ?";
					case Key.CancelProcess2:
						return "Toutes les étapes de travail sont éliminées.";
					case Key.CheckingCredentials:
						return "Vérification des justificatifs d'identité, veuillez patienter...";
					case Key.CurrentComponent:
						return "Composant actuel";
					case Key.CustomApplication:
						return "Anwendung";
					case Key.Component:
						return "Composant";
					case Key.DomainName:
						return "Domain Name";
					case Key.DomainGroup:
						return "Domain Group";
					case Key.DruckerConfig:
						return "Imprimante Config";
					case Key.Einstellungen:
						return "Réglages";
					case Key.Error:
						return "Erreur";
					case Key.Exit:
						return "Finaliser";
					case Key.Finish:
						return "Finaliser et imprimer";
					case Key.Formel:
						return "Produit";
					case Key.FtpUserName:
						return "FTP User Name:";
					case Key.FtpUserPassword:
						return "FTP User Password:";
					case Key.FtpServerIp:
						return "FTP Server Ip:";
					case Key.GeneralSettings:
						return "General Settings";
					case Key.Grundzustand:
						return "Grundzustand (de)";
					case Key.InternalOrderNumber:
						return "Int. Order No";
					case Key.Ja:
						return "Oui";
					case Key.KeinUSBStickErkannt:
						return "Pas de clé USB disponible";
					case Key.Loeschen:
						return "Effacer";
					case Key.Location:
						return "Emplacement";
					case Key.LoginFailed:
						return "Echec de la connexion";
					case Key.LogoutTime:
						return "Décon. aut. en min";
					case Key.MainSetupNode:
						return "Merck Serono Setup";
					case Key.ManulRefQuantity:
						return "Eigene Referenzmenge:";
					case Key.ManulRefWeight:
						return "Berechnetes Referenzgewicht:";
					case Key.MatNo:
						return "Material No";
					case Key.Name:
						return "Nom";
					case Key.Nein:
						return "Non";
					case Key.Netto:
						return "Net";
					case Key.NettoLive:
						return "poids net en cours:";
					case Key.Ok:
						return "Ok";
					case Key.Password:
						return "mot de passe";
					case Key.NewPassword:
						return "nouveau mot de passe";
					case Key.NewPasswordRepeat:
						return "Répéter mot de passe";
					case Key.PleaseLoginFirst:
						return "Veuillez d'abord vous inscrire.";
					case Key.PrinterPort:
						return "Port de imprimante:";
					case Key.ProductStart:
						return "Produit Démarrer :";
					case Key.ProductEnd:
						return "Produit Fin :";
					case Key.RecipeDecimalPlaces:
						return "Recette décimales:";
					case Key.RoomNumber:
						return "Numéro de chambre:";
					case Key.SapScaleNumber:
						return "Numéro de balance SAP :";
					case Key.SapSettings:
						return "SAP Settings";
					case Key.ScaleName:
						return "Désignation des balance";
					case Key.ScoreBoardPort:
						return "Scoreboard Port";
					case Key.ScoreBoardDigits:
						return "Scoreboard Digits";
					case Key.Setup:
						return "Setup";
					case Key.StateTextBatchNo:
						return "Veuillez saisir le numéro de lot et confirmer ensuite";
					case Key.StateTextEmptyScale:
						return "Veuillez confirmer que la balance est vide";
					case Key.StateTextFormula:
						return "Veuillez saisir le produit et confirmer ensuite";
					case Key.StateTextPrintTotal:
						return "La pesée a été réalisée avec succès.";
					case Key.StateTextTareScale1:
						return "Veuillez placer le récipient sur la balance, tarer puis";
					case Key.StateTextTareScale2:
						return "confirmer.";
					case Key.StateTextComponent1:
						return "Veuillez scanner / saisir le composant, remplir";
					case Key.StateTextComponent2:
						return "le récipient, puis confirmer le poids net";
					case Key.AddItemTextFormula:
						return "Veuillez entrer la produit";
					case Key.AddItemTextBatchNo:
						return "Veuillez entrer le numéro de lot";
					case Key.AddItemTextComponent:
						return "Veuillez entrer le composant";
					case Key.AddItemTextTare:
						return "Veuillez entrer la tare manuelle";
					case Key.Start:
						return "Démarrer";
					case Key.StartDate:
						return "Start Date";
					case Key.TareFormula:
						return "Tare";
					case Key.TareManuel:
						return "Tare manuelle";
					case Key.TerminalSubNode:
						return "Terminal config";
					case Key.Test:
						return "Test";
					case Key.TestResult:
						return "Test Result";
					case Key.TotalNetWeight:
						return "Poids net total";
					case Key.UpdateStatus:
						return "Update Status";
					case Key.Update:
						return "System Update";
					case Key.Username:
						return "nom de utilisateur";
					case Key.Waage:
						return "Bal";
					default:
						return string.Empty;
				}
			}

			public string Language
			{
				get { return "fr-FR"; }
			}
		}
	}
}