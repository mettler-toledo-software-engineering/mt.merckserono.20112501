﻿using System.IO;
using MT.Singularity.Platform;
using MT.Singularity.Presentation;
using MT.Singularity.Presentation.Drawing;

namespace IND930YMLUI.Infrastructure
{
    public static class Globals
    {

        public static SingularityEnvironment Environment = new SingularityEnvironment("mt.merckserono");

        public static string IND930USBDrive = "D:\\";
        public static string DegradedModeUserName = "DegradedModeUser";
        public static string UsbBackupBaseDirectory = Path.Combine(IND930USBDrive, "Backup");
        public static string SystemUpdateSourceDirectory = Path.Combine(IND930USBDrive, "systemupdate");
        public static string SystemUpdateTargetDirectory = SingularityEnvironment.ServiceDirectory;

        public static string BarcodeScannerName = "Barcode Scanner";
        public static readonly SolidColorBrush LightGray = Colors.LightGrayBrush;
        public static readonly Color Red = Colors.Red;
        public static readonly Color Green = Colors.MediumGreen;

        public static readonly int TextBlockHeight = 25;
        public static readonly int TextBlockWidth = 260;
        public static readonly int HeaderFontSize = 20;
        public static readonly int BodyFontSize = 18;

    }
}
