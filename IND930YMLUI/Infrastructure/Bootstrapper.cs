﻿using System;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using IND930.Library.ErrorHandling;
using IND930YMLUI.Config;
using IND930YMLUI.Views;
using Libraries.Peripherals.BarcodeScanner;
using Libraries.Peripherals.Helper;
using Libraries.Peripherals.Scoreboard;
// ReSharper disable once RedundantUsingDirective
using MT.Singularity;
using MT.Singularity.Composition;
using MT.Singularity.Logging;
using MT.Singularity.Platform.CommonUX.Controls.MeasurementDisplay;
using MT.Singularity.Platform.CommonUX.Infrastructure;
using MT.Singularity.Platform.Configuration;
using MT.Singularity.Platform.Configuration.Scale;
using MT.Singularity.Platform.Devices.Scale;
using MT.Singularity.Platform.Infrastructure;
using MT.Singularity.Presentation.UserInteraction;
using MT.Singularity.Presentation.Utils;
using MT.Singularity.Presentation.Model;
using MT.Singularity.Presentation;

namespace IND930YMLUI.Infrastructure
{
    /// <summary>
    /// Initializes and runs the application.
    /// </summary>
    internal class Bootstrapper : ClientBootstrapperBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Bootstrapper"/> class.
        /// </summary>
        public Bootstrapper()
            : base(Assembly.GetExecutingAssembly())
        {
        }

        /// <summary>
        /// Gets a value indicating whether it need to create customer database.
        /// </summary>
        protected override bool NeedToCreateCustomerDatabase
        {
            get { return true; }
        }

        /// <summary>
        /// Initializes the application in this method. This method is called after all
        /// services have been started and before the <see cref="T:MT.Singularity.Platform.Infrastructure.IShell" /> is shown.
        /// </summary>
        protected override async void InitializeApplication()
        {
            base.InitializeApplication();

            SetCustomCulture();

            await InitializeCustomerService();
        }

        /// <summary>
        /// Initializes the customer service.
        /// Put all customer services or components initialization code in this method.
        /// </summary>
        private async Task InitializeCustomerService()
        {
            try
            {
                

                CompositionContainer.EnableRecursiveResolution();

                var engine = CompositionContainer.Resolve<IPlatformEngine>();
                var securityService = await engine.GetSecurityServiceAsync();
                var configurationStore = CompositionContainer.Resolve<IConfigurationStore>();

                
                var customerComponent = new Components(configurationStore, securityService, CompositionContainer);
                
               
                CompositionContainer.AddInstance<IComponents>(customerComponent);
                CompositionContainer.AddAssembly(Assembly.Load("Libraries.Peripherals"));
                CompositionContainer.AddAssembly(Assembly.Load("Libraries.Authentication"));
                CompositionContainer.AddAssembly(Assembly.Load("IND930.Library"));

                CompositionContainer.AddInstance(typeof(IErrorMessageEventProvider));
                CompositionContainer.AddInstance(typeof(IUsbBarcodeScannerManager)); 
                CompositionContainer.AddInstance(typeof(ISerialBarcodeScannerManager));
                CompositionContainer.AddInstance(typeof(ISerialPrinterManager));
                CompositionContainer.AddInstance(typeof(ISerialScoreBoardManager));
                CompositionContainer.AddInstance(typeof(ActiveDirectoryManager));

                //CompositionContainer.AddInstance(typeof(Localization));
                CompositionContainer.AddInstance(typeof(WeightWindowControl));


                GetType().Assembly.GetTypes()
                    .Where(type => type.IsClass)
                    .Where(type => type.Name.EndsWith("View"))
                    .ToList()
                    .ForEach(viewModelType => CompositionContainer.AddInstance(viewModelType));

                GetType().Assembly.GetTypes()
                    .Where(type => type.IsClass)
                    .Where(type => type.Name.EndsWith("ViewModel"))
                    .ToList()
                    .ForEach(viewModelType => CompositionContainer.AddInstance(viewModelType));

            }
            catch (Exception ex)
            {
                Log4NetManager.ApplicationLogger.Error("Error Bootstrapper", ex);
                //throw;
            }

        }

        private void SetCustomCulture()
        {
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)Thread.CurrentThread.CurrentUICulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";

            Thread.CurrentThread.CurrentUICulture = customCulture;
        }
    }
}
