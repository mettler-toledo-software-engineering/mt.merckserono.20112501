﻿
using System;
using System.Collections.Generic;
using IND930.Library.ErrorHandling;
using IND930.Library.Models;
using IND930.Library.Printing;
using IND930YMLUI.Config;
using IND930YMLUI.Infrastructure;
using IND930YMLUI.Views;
using Libraries.Authentication.LocalTerminal;
using Libraries.Authentication.LoginAndLogout;
using Libraries.Authentication.User;
using Libraries.Peripherals.BarcodeScanner;
using Libraries.Peripherals.Scoreboard;
using MT.Singularity.Platform.Devices.Scale;
using MT.Singularity.Platform.UserManagement;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Input;
using MT.Singularity.Presentation.Model;

namespace IND930YMLUI.ViewModels
{
    /// <summary>
    /// Home Screen view model implementation
    /// </summary>
    public class HomeScreenViewModel : ViewModelBase
    {
        private FillProcessView _fillProcessView;
        private LoginView _loginView;
        private IFillProcessModel _fillProcess;
        private ActiveDirectoryManager _activeDirectoryManager;
        private ISerialScoreBoardManager _scoreBoardManager;
        private IAutomatedSerialScoreBoard _scoreBoard;
        private ISecurityService _securityService;
        private ILocalAuthentification _localAuthentification;
        private ILoginService _loginService;

        /// <summary>
        /// Constructor Initializes a new instance of the <see cref="HomeScreenViewModel"/> class.
        /// </summary>

        public HomeScreenViewModel(
            IScaleService scaleService,
            IErrorMessageEventProvider errorMessageEventProvider,
            ISerialPrinterManager serialPrinterManager,
            FillProcessView fillProcessView,
            LoginView loginView,
            IFillProcessModel fillProcess,
            ActiveDirectoryManager activeDirectoryManager,
            ISerialScoreBoardManager scoreBoardManager,
            IAutoLogoutService logoutService,
            ISecurityService securityService,
            ILocalAuthentification localAuthentification, 
            ILoginService loginService) : base(scaleService, errorMessageEventProvider, serialPrinterManager, logoutService)
        {
            _fillProcessView = fillProcessView;
            _loginView = loginView;
            _fillProcess = fillProcess;
            _activeDirectoryManager = activeDirectoryManager;
            _printerGa46 = _serialPrinterManager.GetConfiguredPrinterGa46();
            _scoreBoardManager = scoreBoardManager;
            _scoreBoard = _scoreBoardManager.GetAutomatedScoreBoard();
            _securityService = securityService;
            _localAuthentification = localAuthentification;
            _loginService = loginService;
            SetLoginButtonProperties();

        }


        //public ICommand TestBoard => new DelegateCommand(async () =>
        //{
        //    _scoreBoard.SetNewScoreBoardWeight(await _scaleService.SelectedScale.GetStableWeightAsync(UnitType.Display));
        //});

        //public ICommand TestBoard => new DelegateCommand(async () =>
        //{
        //    //_scoreBoard.SetNewScoreBoardWeight(await _scaleService.SelectedScale.GetStableWeightAsync(UnitType.Display));
        //});
        protected override void OnLogOffEvent(object sender, EventArgs e)
        {
            if (_fillProcess.LoggedInUser != null)
            {
                _fillProcess.LoggedInUser = null;
                SetLoginButtonProperties();
            }
        }

        public void OnPointerDown(object sender, PointerEventArgs pointerEventArgs)
        {
            AutoLogoutService.ResetLogOffTimer();
        }

        private string _loginButtonImage;

        public string LoginButtonImage
        {
            get { return _loginButtonImage; }
            set
            {
                if (_loginButtonImage != value)
                {
                    _loginButtonImage = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private string _loginButtonText;

        public string LoginButtonText
        {
            get { return _loginButtonText; }
            set
            {
                if (_loginButtonText != value)
                {
                    _loginButtonText = value;
                    NotifyPropertyChanged();
                }
            }
        }

        protected override void BarcodeScannerOnDataReceivedEvent(object sender, BarcodeDataReceivedEventArgs e)
        {
            

        }

        protected override void OnErrorMessageEvent(object sender, ErrorMessageEventArgs e)
        {
            MessageBox.Show((Visual)ParentPage, e.ErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private DelegateCommand _manageUserCommand;
        public ICommand LoginCommand => _manageUserCommand = new DelegateCommand(ManageUserCommandExecute);

        private void ManageUserCommandExecute()
        {
            if (_fillProcess.LoggedInUser == null && _securityService.CurrentUser.Name != Globals.DegradedModeUserName)
            {
                _loginView.ShowAsync(ParentVisual).ContinueWith(task =>
                {
                    SetLoginButtonProperties();
                });
            }
            else if (_fillProcess.LoggedInUser == null && _securityService.CurrentUser.Name == Globals.DegradedModeUserName)
            {
                var user = new Libraries.Authentication.User.User {UserName = Globals.DegradedModeUserName};
                var result = _localAuthentification.AuthentificateLocalUser(user);
                _fillProcess.LoggedInUser = result;
                _fillProcess.LoggedInUser.DisplayName = Globals.DegradedModeUserName;
                _loginService.RaiseUserLoginEvent();
                SetLoginButtonProperties();
            }
            else
            {
                _fillProcess.LoggedInUser = null;
                SetLoginButtonProperties();
            }
            
        }


        public ICommand StartProcessCommand =>  new DelegateCommand(StartProcessCommandExecute);



        private void StartProcessCommandExecute()
        {

            if (_fillProcess.LoggedInUser != null)
            {
                ParentPage.NavigationService.NavigateTo(_fillProcessView);
            }
            else
            {
                MessageBox.Show(ParentVisual, Localization.Get(Localization.Key.PleaseLoginFirst), string.Empty, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void SetLoginButtonProperties()
        {

            if (_fillProcess.LoggedInUser != null)
            {
                LoginButtonImage = "embedded://IND930YMLUI/IND930YMLUI.images.UserLogOff.al8";
                LoginButtonText = $"{_fillProcess.LoggedInUser.UserName}";
            }
            else
            {
                LoginButtonImage = "embedded://IND930YMLUI/IND930YMLUI.images.UserLogOn.al8";
                LoginButtonText = "Login";
            }
        }

        public void UpdateProperties()
        {
            SetLoginButtonProperties();
        }
    }
}
