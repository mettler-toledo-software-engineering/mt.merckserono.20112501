﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.IO.Ports;
using IND930.Library.ErrorHandling;
using IND930YMLUI.Config;
using IND930YMLUI.Infrastructure;
using Libraries.Authentication.LoginAndLogout;
using Libraries.Peripherals.BarcodeScanner;
using Libraries.Peripherals.Helper;
using Libraries.Peripherals.Printer;
using Libraries.Peripherals.USB;
using MT.Singularity.Composition;
using MT.Singularity.Data;
using MT.Singularity.Platform.Devices.Scale;
using MT.Singularity.Platform.Infrastructure;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Controls.Navigation;

namespace IND930YMLUI.ViewModels
{
    public abstract class ViewModelBase : ObservableObject
    {
        

        protected IUsbBarcodeScanner _usbBarcodeScanner;
        protected IUsbBarcodeScannerManager _usbBarcodeScannerManager;
        protected IErrorMessageEventProvider _errorMessageEventProvider;

        protected ISerialBarcodeScanner _serialbarcodeScanner;
        protected ISerialBarcodeScannerManager _serialBarcodeScannerManager;
        protected IScaleService _scaleService;
        protected IUsbEventProvider _usbEventProvider;
        protected ISerialPrinterManager _serialPrinterManager;
        protected IPrinterGa46 _printerGa46;
        protected IAutoLogoutService AutoLogoutService;

        public INavigationPage ParentPage { get; set; }

        public Visual ParentVisual => (Visual) ParentPage;
        public ChildWindow ParentChildWindow { get; set; }

        protected ViewModelBase(
            IScaleService scaleService,
            IErrorMessageEventProvider errorMessageEventProvider,
            IUsbEventProvider usbEventProvider,
            IUsbBarcodeScannerManager usbBarcodeScannerManager,
            IAutoLogoutService autoLogoutService
            )
        {
            _scaleService = scaleService;
            _errorMessageEventProvider = errorMessageEventProvider;
            _usbEventProvider = usbEventProvider;
            _usbBarcodeScannerManager = usbBarcodeScannerManager;
            _usbBarcodeScanner = usbBarcodeScannerManager.GetConfiguredBarcodeScanner();
            AutoLogoutService = autoLogoutService;

        }

        protected ViewModelBase(
            IScaleService scaleService,
            IErrorMessageEventProvider errorMessageEventProvider,
            ISerialBarcodeScannerManager serialBarcodeScannerManager,
            IAutoLogoutService autoLogoutService
        )
        {

            _scaleService = scaleService;
            _errorMessageEventProvider = errorMessageEventProvider;
            _serialBarcodeScannerManager = serialBarcodeScannerManager;
            _serialbarcodeScanner = serialBarcodeScannerManager.GetConfiguredBarcodeScanner();
            AutoLogoutService = autoLogoutService;
        }

        protected ViewModelBase(
            IScaleService scaleService,
            IErrorMessageEventProvider errorMessageEventProvider,
            ISerialPrinterManager serialPrinterManager,
            IAutoLogoutService autoLogoutService
        )
        {

            _scaleService = scaleService;
            _errorMessageEventProvider = errorMessageEventProvider;
            _serialPrinterManager = serialPrinterManager;
            AutoLogoutService = autoLogoutService;

        }

        protected ViewModelBase(IScaleService scaleService)
        {
            _scaleService = scaleService;
        }

        public virtual void RegisterEventsForViewModel()
        {
            if (_errorMessageEventProvider != null)
            {
                _errorMessageEventProvider.ErrorMessageEvent += OnErrorMessageEvent;
            }
            //if (_usbEventProvider != null)
            //{
            //    _usbEventProvider.DeviceInsertedEvent += UsbHandlerOnDeviceInsertedEvent;
            //}

            if (_usbBarcodeScanner != null)
            {
                _usbBarcodeScanner.DataReceivedEvent += BarcodeScannerOnDataReceivedEvent;
            }

            if (_serialbarcodeScanner != null)
            {
                _serialbarcodeScanner.DataReceivedEvent += BarcodeScannerOnDataReceivedEvent;
            }

            if (_scaleService != null)
            {
                _scaleService.SelectedScale.NewChangedWeightInDisplayUnit += SelectedScaleOnNewChangedWeightInDisplayUnit;
            }

            if (AutoLogoutService != null)
            {
                AutoLogoutService.LogOutEvent += OnLogOffEvent;
            }
            
        }

        protected virtual void OnLogOffEvent(object sender, EventArgs e)
        {

        }
        protected virtual void OnErrorMessageEvent(object sender, ErrorMessageEventArgs e)
        {
            
        }

        public virtual void UnregisterEventsForViewModel()
        {
            //if (_usbEventProvider != null)
            //{
            //    _usbEventProvider.DeviceInsertedEvent -= UsbHandlerOnDeviceInsertedEvent;
            //}
            if (_usbBarcodeScanner != null)
            {
                _usbBarcodeScanner.DataReceivedEvent -= BarcodeScannerOnDataReceivedEvent;
            }
            if (_serialbarcodeScanner != null)
            {
                _serialbarcodeScanner.DataReceivedEvent -= BarcodeScannerOnDataReceivedEvent;
            }

            if (_scaleService != null)
            {
                _scaleService.SelectedScale.NewChangedWeightInDisplayUnit -= SelectedScaleOnNewChangedWeightInDisplayUnit;
            }
            if (AutoLogoutService != null)
            {
                AutoLogoutService.LogOutEvent -= OnLogOffEvent;
            }

        }

        protected virtual void SelectedScaleOnNewChangedWeightInDisplayUnit(WeightInformation weight)
        {

        }

        protected virtual void UsbHandlerOnDeviceInsertedEvent(object sender, EventArgs e)
        {
            _usbBarcodeScanner = _usbBarcodeScannerManager.GetConfiguredBarcodeScanner();
        }
        protected virtual void BarcodeScannerOnDataReceivedEvent(object sender, BarcodeDataReceivedEventArgs e)
        {
            Console.WriteLine(e.Data);
        }
    }
}
