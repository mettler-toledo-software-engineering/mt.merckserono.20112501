﻿using System;

using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using IND930.Library.FillProcess.States;
using IND930.Library.Models;
using MT.Singularity;
using MT.Singularity.Platform.Devices.Scale;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Model;

namespace IND930YMLUI.ViewModels
{
    public class AddItemViewModel : ViewModelBase
    {
        private IAddItemViewConfiguration _addItemViewConfiguration;
        private IFillProcessModel _fillProcess;
        public AddItemViewModel(IScaleService scaleService, IAddItemViewConfiguration addItemViewConfiguration, IFillProcessModel fillProcess) : base(scaleService)
        {
            _addItemViewConfiguration = addItemViewConfiguration;
            _fillProcess = fillProcess;
        }

        private string _title;

        public string Title
        {
            get { return _title; }
            set
            {
                if (_title != value)
                {
                    _title = value;
                    NotifyPropertyChanged();
                }
            }
        }

        public void SetTitle()
        {
            switch (_addItemViewConfiguration.CurrentProcessState)
            {
                case ProcessStateEnum.InputFormula:
                    Title = Localization.Get(Localization.Key.AddItemTextFormula);
                    EnteredItem = _fillProcess.Formula;
                    break;
                case ProcessStateEnum.InputBatchNumber: Title = Localization.Get(Localization.Key.AddItemTextBatchNo);
                    EnteredItem = _fillProcess.BatchNumber;
                    break;
                case ProcessStateEnum.AddComponent: Title = Localization.Get(Localization.Key.AddItemTextComponent);
                    break;
                case ProcessStateEnum.TareFormula: Title = Localization.Get(Localization.Key.AddItemTextTare);
                    break;
                default: Title = "";
                    break;
            }
        }

        private string _enteredItem;

        public string EnteredItem
        {
            get { return _enteredItem; }
            set
            {
                if (_enteredItem != value)
                {
                    _enteredItem = value;
                    NotifyPropertyChanged();
                    _confirmCommand?.NotifyCanExecuteChanged();
                }
            }
        }

        private DelegateCommand _confirmCommand;
        public ICommand ConfirmCommand => _confirmCommand = new DelegateCommand(ConfirmCommandExecute, CanConfirmCommandExecute);

        private bool CanConfirmCommandExecute()
        {
            bool canExecute = string.IsNullOrWhiteSpace(EnteredItem) == false;
            return canExecute;
        }

        private void ConfirmCommandExecute()
        {
            switch (_addItemViewConfiguration.CurrentProcessState)
            {
                case ProcessStateEnum.InputFormula:
                    _fillProcess.Formula = EnteredItem;
                    break;
                case ProcessStateEnum.InputBatchNumber:
                    _fillProcess.BatchNumber = EnteredItem;
                    break;
                case ProcessStateEnum.AddComponent:
                    _fillProcess.CurrentComponent = EnteredItem;
                    break;
                case ProcessStateEnum.TareFormula:
                    PresetTare();
                    break;
                default:
                    Title = "";
                    break;
            }

            ParentChildWindow.DialogResult = DialogResult.OK;
            CloseChildWindow();
        }



        public ICommand CancelCommand => new DelegateCommand(CancelCommandExecute);

        private void CancelCommandExecute()
        {
            ParentChildWindow.DialogResult = DialogResult.Cancel;
            CloseChildWindow();
        }
        private void CloseChildWindow()
        {

            ClearProperties();
            ParentChildWindow.Close();
        }
        private void ClearProperties()
        {
            // letzten komponenten namen übernehmen // aber bei erster komponente nicht Tare oder batch anzeigen
            if (_addItemViewConfiguration.CurrentProcessState == ProcessStateEnum.AddComponent)
            {
                return;
            }
            EnteredItem = string.Empty;
        }

        private void PresetTare()
        {
            string presetweight = EnteredItem;
            WellknownWeightUnit unit = _scaleService.SelectedScale.CurrentDisplayUnit;
            _scaleService.SelectedScale.PresetTareAsync(presetweight, unit).ContinueWith(CheckWeightState);

            
        }

        private void CheckWeightState(Task<WeightState> tareTask)
        {
            if (tareTask.Exception == null)
            {
                if (tareTask.Result == WeightState.OK)
                {
                    _scaleService.SelectedScale.GetStableWeightAsync(UnitType.Display).ContinueWith(task =>
                    {
                        if (task.Exception == null)
                        {
                            _fillProcess.TaredContainer = task.Result;
                            _fillProcess.TareTime = DateTime.Now;
                            _fillProcess.HasBeenManuallyTared = true;
                        }
                    });
                }
            }
        }
    }
}
