﻿using System;

using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using IND930.Library.Models;
using Libraries.Authentication.ActiveDirectory;
using Libraries.Authentication.LoginAndLogout;
using Libraries.Authentication.User;
using MT.Singularity.Platform.Devices.Scale;
using MT.Singularity.Presentation;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Model;

namespace IND930YMLUI.ViewModels
{
    public class LoginViewModel : ViewModelBase
    {
        private IFillProcessModel _fillProcess;
        private IUser _user;
        private IActiveDirectoryAuthentification _authentification;
        private ILoginService _loginService;
        private bool _newPasswordNeeded = false;
        private bool _aProcessIsRunning = false;
        public LoginViewModel(IScaleService scaleService, IFillProcessModel fillProcess, IUser user, IActiveDirectoryAuthentification authentification, ILoginService loginService) : base(scaleService)
        {
            _fillProcess = fillProcess;
            _user = user;
            _authentification = authentification;
            _loginService = loginService;
            NewPasswordVisibility = _newPasswordNeeded ? Visibility.Visible : Visibility.Collapsed;
            WindowHeight = _newPasswordNeeded ? 580 : 400;
        }

        public char PasswordChar => '*';

        private int _windowHeight;

        public int WindowHeight
        {
            get { return _windowHeight; }
            set
            {
                if (_windowHeight != value)
                {
                    _windowHeight = value;
                    NotifyPropertyChanged();
                }
            }
        }


        private string _username;

        public string Username
        {
            get { return _username; }
            set
            {
                if (_username != value)
                {
                    _username = value;
                    NotifyPropertyChanged();
                    _confirmCommand?.NotifyCanExecuteChanged();
                }
            }
        }


        private string _password;

        public string Password
        {
            get { return _password; }
            set
            {
                if (_password != value)
                {
                    _password = value;
                    NotifyPropertyChanged();
                    _confirmCommand?.NotifyCanExecuteChanged();
                }
            }
        }

        private string _newPassword;

        public string NewPassword
        {
            get { return _newPassword; }
            set
            {
                if (_newPassword != value)
                {
                    _newPassword = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private string _newPasswordRepeat;

        public string NewPasswordRepeat
        {
            get { return _newPasswordRepeat; }
            set
            {
                if (_newPasswordRepeat != value)
                {
                    _newPasswordRepeat = value;
                    NotifyPropertyChanged();
                }
            }
        }
        private Visibility _newPasswordVisibility;

        public Visibility NewPasswordVisibility
        {
            get { return _newPasswordVisibility; }
            set
            {
                if (_newPasswordVisibility != value)
                {
                    _newPasswordVisibility = value;
                    NotifyPropertyChanged();
                }
            }
        }


        private string _message;

        public string Message
        {
            get { return _message; }
            set
            {
                if (_message != value)
                {
                    _message = value;
                    NotifyPropertyChanged();
                }
            }
        }


        private DelegateCommand _confirmCommand;
        public ICommand ConfirmCommand => _confirmCommand = new DelegateCommand(ConfirmCommandExecute, CanConfirmCommandExecute);

        private bool CanConfirmCommandExecute()
        {
            bool canExecute = string.IsNullOrWhiteSpace(Username) == false && _newPasswordNeeded == false && _aProcessIsRunning == false;
            // altes password != neues password, neues password != empty , neues password wiederholen und identisch
            return canExecute;
        }

        private async void ConfirmCommandExecute()
        {
            _user.UserName = Username;
            _user.Password = Password;

            Task<IActiveDirectoryAuthentificationResult> task = Task.Run(() => _authentification.AuthentificateUserWithDomainGroup(_user));

            _aProcessIsRunning = true;
            Message = Localization.Get(Localization.Key.CheckingCredentials);
            _confirmCommand?.NotifyCanExecuteChanged();
            

            var authentificationResult = await task;
            _fillProcess.LoggedInUser = authentificationResult.AuthentificatedUser;

            _aProcessIsRunning = false;
            Message = string.Empty;
            _confirmCommand?.NotifyCanExecuteChanged();

            CheckForLoginError();
        }

        private void CheckForLoginError()
        {
            if (_fillProcess.LoggedInUser == null)
            {
                Message = Localization.Get(Localization.Key.LoginFailed);
            }
            else
            {
                _loginService.RaiseUserLoginEvent();
                CloseChildWindow();
            }
        }

        public ICommand CancelCommand => new DelegateCommand(CancelCommandExecute);

        private void CancelCommandExecute()
        {
            CloseChildWindow();
        }
        private void CloseChildWindow()
        {
            ClearProperties();
            ParentChildWindow.Close();
        }
        private void ClearProperties()
        {
            Username = string.Empty;
            Password = string.Empty;
            Message = string.Empty;
        }
    }
}
