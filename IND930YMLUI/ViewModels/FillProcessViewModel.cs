﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using IND930.Library.ErrorHandling;
using IND930.Library.FillProcess;
using IND930.Library.FillProcess.States;
using IND930.Library.Helpers;
using IND930.Library.Models;
using IND930.Library.Printing;
using IND930YMLUI.Config;
using IND930YMLUI.TranslationHelper;
using IND930YMLUI.Views;
using Libraries.Authentication.LoginAndLogout;
using Libraries.Peripherals.BarcodeScanner;
using Libraries.Peripherals.Printer;
using Libraries.Peripherals.Scoreboard;
using Libraries.Peripherals.USB;
using MT.Singularity;
using MT.Singularity.Platform.Devices.Scale;
using MT.Singularity.Platform.Infrastructure;
using MT.Singularity.Platform.LogService;
using MT.Singularity.Platform.Memories;
using MT.Singularity.Platform.Translation;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Input;
using MT.Singularity.Presentation.Model;

namespace IND930YMLUI.ViewModels
{
    public class FillProcessViewModel : ViewModelBase
    {
        private IFillProcessModel _fillProcess;
        private IContext _processContext;
        private IAddItemViewConfiguration _addItemViewConfiguration;
        private AddItemView _addItemView;
        private ISerialPrinterManager _printerManager;
        private IPrinterGa46 _printer;
        
        private IPrinterTemplateConfiguration _printerTemplateConfiguration;
        private IAlibiLogComponent _alibiLogger;
        private Configuration _configuration;
        

        public FillProcessViewModel(IScaleService scaleService, IErrorMessageEventProvider errorMessageEventProvider, IUsbEventProvider usbEventProvider, IUsbBarcodeScannerManager usbBarcodeScannerManager,
            IFillProcessModel fillProcess, IContext processContext, IAddItemViewConfiguration addItemViewConfiguration, AddItemView addItemView, IAutoLogoutService logoutService, ISerialPrinterManager printerManager, IPrinterTemplateConfiguration printerTemplateConfiguration, IPlatformEngine platformEngine, IComponents components) : base(scaleService, errorMessageEventProvider, usbEventProvider, usbBarcodeScannerManager, logoutService)
        {
            _fillProcess = fillProcess;
            _processContext = processContext;
            _addItemViewConfiguration = addItemViewConfiguration;
            _addItemView = addItemView;
            _printerManager = printerManager;
            _printerTemplateConfiguration = printerTemplateConfiguration;
            _processContext.CanConfirmProcessEvent += ProcessContextOnCanConfirmProcessEvent;

            CanConfirm = false;
            CanTareScale = false;
            UpdateProperties();
            GetConfiguration(components).ContinueWith(RegisterWithConfigurationEvents);
            GetAlibiLogger(platformEngine).ContinueWith(CatchException);
        }

        protected override void SelectedScaleOnNewChangedWeightInDisplayUnit(WeightInformation weight)
        {
            double livenet = weight.NetWeight + _fillProcess.TotalFormulaNetWeight;
            TotalNetLive = $"{livenet.ToString(RecipeFormatter.RecipeDecimalPlaceFormatting)} {weight.Unit.ToTranslatedAbbreviation()}";
        }

        private void CatchException(Task getAlibiTask)
        {
            if (getAlibiTask.Exception != null)
            {
                Console.WriteLine(getAlibiTask.Exception);
            }
        }

        private async Task GetAlibiLogger(IPlatformEngine platformEngine)
        {
            var logservice = await platformEngine.GetLogServiceAsync();
            _alibiLogger = await logservice.GetAlibiLogComponentAsync();
        }

        private async Task<Configuration> GetConfiguration(IComponents components)
        {
            var config = await components.GetConfigurationAsync();
            return config;
        }

        private void RegisterWithConfigurationEvents(Task<Configuration> configurationTask)
        {
            if (configurationTask.IsCompleted)
            {
                _configuration = configurationTask.Result;
                ScaleName = _configuration.SapScaleNumber;
                Location = _configuration.RoomNumber;
                _fillProcess.ScaleName = ScaleName;
                _fillProcess.Location = Location;
                RecipeFormatter.RecipeDecimalPlaceFormatting = $"F{_configuration.RecipeDecimalPlaces}";
                _configuration.PropertyChanged += ConfigurationPropertyChanged;
            }
        }

        private void ConfigurationPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "SapScaleNumber")
            {
                ScaleName = _configuration.SapScaleNumber;
                _fillProcess.ScaleName = ScaleName;
            }

            if (e.PropertyName == "RoomNumber")
            {
                Location = _configuration.RoomNumber;
                _fillProcess.Location = Location;
            }

            if (e.PropertyName == "RecipeDecimalPlaces")
            {
                RecipeFormatter.RecipeDecimalPlaceFormatting = $"F{_configuration.RecipeDecimalPlaces}";
                UpdateProperties();
            }
        }


        protected override void OnLogOffEvent(object sender, EventArgs e)
        {
            LogoutCommandExecute();
        }

        public void OnPointerDown(object sender, PointerEventArgs pointerEventArgs)
        {
            AutoLogoutService.ResetLogOffTimer();
        }

        private void ProcessContextOnCanConfirmProcessEvent(object sender, CanConfirmProcessEventArg e)
        {
            CanConfirm = e.CanConfirmProcess;
        }


        protected override void BarcodeScannerOnDataReceivedEvent(object sender, BarcodeDataReceivedEventArgs e)
        {
            UpdateProperties();
        }

        public void UpdateProperties()
        {
            var unit = _scaleService.SelectedScale.CurrentDisplayUnit.ToTranslatedAbbreviation();
            AddButtonText = _processContext.ProcessState.ToAddButtonTextTranslation();
            AddButtonImage = _processContext.ProcessState.ToAddButtonImage();
            FinishButtonText = _processContext.ProcessState.ToFinishButtonTextTranslation();
            FinishButtonImage = _processContext.ProcessState.ToFinishButtonImage();

            InstructionLine = _processContext.ProcessState.ToInstructionLineTranslation();
            Formula = _fillProcess.Formula;
            BatchNumber = _fillProcess.BatchNumber;
            LoggedInUsername = _fillProcess.LoggedInUser == null ? string.Empty : _fillProcess.LoggedInUser.UserName;
            ComponentList = new ObservableCollection<ComponentModel>(_fillProcess.UsedComponents);
            FormulaTareValue = _fillProcess.TaredContainer != null ? $"{_fillProcess.TaredContainer.TareWeight.ToString(RecipeFormatter.RecipeDecimalPlaceFormatting)} {unit}" : string.Empty;
            CurrentComponent = _fillProcess.CurrentComponent;
            TotalFormulaNetWeight = $"{_fillProcess.TotalFormulaNetWeight.ToString(RecipeFormatter.RecipeDecimalPlaceFormatting)} {unit}";
            CanTareScale = _processContext.CanTareScale;
            CanAddItem = _processContext.CanAddItem;
            CanPrintTotal = _processContext.CanPrintTotal;
            
            _tareCommand?.NotifyCanExecuteChanged();
            _printTotalCommand?.NotifyCanExecuteChanged();
        }


        private string _totalNetLive;

        public string TotalNetLive
        {
            get { return _totalNetLive; }
            set
            {
                if (_totalNetLive != value)
                {
                    _totalNetLive = value;
                    NotifyPropertyChanged();
                }
            }
        }


        private string _scaleName;

        public string ScaleName
        {
            get { return _scaleName; }
            set
            {
                if (_scaleName != value)
                {
                    _scaleName = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private string _location;

        public string Location
        {
            get { return _location; }
            set
            {
                if (_location != value)
                {
                    _location = value;
                    NotifyPropertyChanged();
                }
            }
        }


        private string _finishButtonText;

        public string FinishButtonText
        {
            get { return _finishButtonText; }
            set
            {
                if (_finishButtonText != value)
                {
                    _finishButtonText = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private string _finishButtonImage;

        public string FinishButtonImage
        {
            get { return _finishButtonImage; }
            set
            {
                if (_finishButtonImage != value)
                {
                    _finishButtonImage = value;
                    NotifyPropertyChanged();
                }
            }
        }
        private string _totalFormulaNetWeight;

        public string TotalFormulaNetWeight
        {
            get { return _totalFormulaNetWeight; }
            set
            {
                if (_totalFormulaNetWeight != value)
                {
                    _totalFormulaNetWeight = value;
                    NotifyPropertyChanged();
                }
            }
        }


        private string _currentComponent;

        public string CurrentComponent
        {
            get { return _currentComponent; }
            set
            {
                if (_currentComponent != value)
                {
                    _currentComponent = value;
                    NotifyPropertyChanged();
                }
            }
        }


        private string _formulaTareValue;

        public string FormulaTareValue
        {
            get { return _formulaTareValue; }
            set
            {
                if (_formulaTareValue != value)
                {
                    _formulaTareValue = value;
                    NotifyPropertyChanged();
                }
            }
        }


        private ObservableCollection<ComponentModel> _componentList;

        public ObservableCollection<ComponentModel> ComponentList
        {
            get { return _componentList; }
            set
            {
                if (_componentList != value)
                {
                    _componentList = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private bool _canConfirm;

        public bool CanConfirm
        {
            get { return _canConfirm; }
            set
            {
                if (_canConfirm != value)
                {
                    _canConfirm = value;
                    NotifyPropertyChanged();
                    _confirmCommand?.NotifyCanExecuteChanged();
                }
            }
        }


        private bool _canPrintTotal;

        public bool CanPrintTotal
        {
            get { return _canPrintTotal; }
            set
            {
                if (_canPrintTotal != value)
                {
                    _canPrintTotal = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private bool _canAddItem;

        public bool CanAddItem
        {
            get { return _canAddItem; }
            set
            {
                if (_canAddItem != value)
                {
                    _canAddItem = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private bool _canTareScale;

        public bool CanTareScale
        {
            get { return _canTareScale; }
            set
            {
                if (_canTareScale != value)
                {
                    _canTareScale = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private string _instructionLine;

        public string InstructionLine
        {
            get { return _instructionLine; }
            set
            {
                if (_instructionLine != value)
                {
                    _instructionLine = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private string _batchNumber;

        public string BatchNumber
        {
            get { return _batchNumber; }
            set
            {
                if (_batchNumber != value)
                {
                    _batchNumber = value;
                    NotifyPropertyChanged();
                }
            }
        }


        private string _formula;

        public string Formula
        {
            get { return _formula; }
            set
            {
                if (_formula != value)
                {
                    _formula = value;
                    NotifyPropertyChanged();
                }
            }
        }


        private string _loggedInUsername;

        public string LoggedInUsername
        {
            get { return _loggedInUsername; }
            set
            {
                if (_loggedInUsername != value)
                {
                    _loggedInUsername = value;
                    NotifyPropertyChanged();
                }
            }
        }


        private string _addButtonImage;

        public string AddButtonImage
        {
            get { return _addButtonImage; }
            set
            {
                if (_addButtonImage != value)
                {
                    _addButtonImage = value;
                    NotifyPropertyChanged();
                }

            }
        }
        private string _addButtonText;

        public string AddButtonText
        {
            get { return _addButtonText; }
            set
            {
                if (_addButtonText != value)
                {
                    _addButtonText = value;
                    NotifyPropertyChanged();
                }
            }
        }


        public ICommand LogoutCommand => new DelegateCommand(LogoutCommandExecute);

        private void LogoutCommandExecute()
        {
            _fillProcess.LoggedInUser = null;
            ParentPage.NavigationService.Back();
        }

        public ICommand AddItemToProcessCommand => new DelegateCommand(AddItemToProcessCommandExecute);

        private async void AddItemToProcessCommandExecute()
        {
            _addItemViewConfiguration.CurrentProcessState = _processContext.ProcessState;
            var result = await _addItemView.ShowAsync(ParentVisual);
            if (result == DialogResult.OK)
            {
                UpdateProperties();
                CanConfirm = _processContext.ProcessState == ProcessStateEnum.InputFormula || _processContext.ProcessState == ProcessStateEnum.InputBatchNumber || _processContext.ProcessState == ProcessStateEnum.TareFormula || (_fillProcess.HasBeenManuallyTared && string.IsNullOrWhiteSpace(_fillProcess.CurrentComponent) == false);
            }

        }

        private DelegateCommand _confirmCommand;
        public ICommand ConfirmationCommand => _confirmCommand = new DelegateCommand(ConfirmationCommandExecute, CanConfirmationCommandExecute);

        private bool CanConfirmationCommandExecute()
        {
            return CanConfirm;
        }

        private async void ConfirmationCommandExecute()
        {
            try
            {
                CanConfirm = false;
                await _processContext.ConfirmationCommand();
                await _alibiLogger.SaveAndGetAlibiRecordAsync(0, UnitType.Display, _fillProcess.LoggedInUser.UserName);
                UpdateProperties();
                await Task.Delay(TimeSpan.FromSeconds(1));
            }
            catch (Exception e)
            {
                CanConfirm = true;
            }
            finally
            {
                CanConfirm = true;
            }

        }

        private DelegateCommand _tareCommand;
        public ICommand TareCommand => _tareCommand = new DelegateCommand(TareCommandExecute, CanTareCommandExecute);

        private bool CanTareCommandExecute()
        {
            return CanTareScale;
        }

        private void TareCommandExecute()
        {
            _processContext.TareCommand().ContinueWith(task =>
            {
                UpdateProperties();
            } );
            
            CanConfirm = true;
        }

        private DelegateCommand _printTotalCommand;
        public ICommand PrintTotalCommand => _printTotalCommand = new DelegateCommand(PrintTotalCommandExecute, CanPrintTotalCommandExecute);

        private bool CanPrintTotalCommandExecute()
        {
            return CanPrintTotal;
        }

        private async void PrintTotalCommandExecute()
        {
            _printer = _printerManager.GetConfiguredPrinterGa46();
            await _processContext.PrintTotalCommand();
            await _alibiLogger.SaveAndGetAlibiRecordAsync(0, UnitType.Display, _fillProcess.LoggedInUser.UserName);
            UpdateProperties();
        }

        public ICommand ExitCommand => new DelegateCommand(ExitCommandExecute);

        private void ExitCommandExecute()
        {

            ConfirmProcessIsFinished();
        }

        private void ConfirmProcessIsFinished()
        {
            if (_fillProcess?.UsedComponents?.Count > 0 && _fillProcess.HasBeenPrinted == false)
            {
                MessageBox.Show(ParentVisual, $"{Localization.Get(Localization.Key.CancelProcess1)}\n{Localization.Get(Localization.Key.CancelProcess2)}", Localization.Get(Localization.Key.Attention), MessageBoxButtons.OKCancel, MessageBoxIcon.Warning, result =>
                {
                    if (result == DialogResult.OK)
                    {
                        ResetProcess();
                    }
                });
            }
            else
            {
                ResetProcess();
            }
        }

        private void ResetProcess()
        {
            _fillProcess.ResetFillProcess();
            _printer = _printerManager.GetConfiguredPrinterGa46();
            _processContext.ProcessState = ProcessStateEnum.InputFormula;
            _processContext.TransitionTo(new InputFormulaState(_printer, _printerTemplateConfiguration));
            ParentPage.NavigationService.Back();
        }

    }
}
