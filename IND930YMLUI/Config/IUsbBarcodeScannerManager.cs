﻿using Libraries.Peripherals.BarcodeScanner;

namespace IND930YMLUI.Config
{
    /// <summary>
    /// this interfaces provides access to a usb barcode scanner in the devicemanager and reacts to insertion / removal of a usb devices / or change in system setup events and configures a
    /// functioning scanner after the events if possible
    /// </summary>
    /// <returns></returns>
    public interface IUsbBarcodeScannerManager
    {
        /// <summary>
        /// this method returns a initialized scanner with a configured serial port
        /// </summary>
        /// <returns></returns>
        IUsbBarcodeScanner GetConfiguredBarcodeScanner();
    }
}