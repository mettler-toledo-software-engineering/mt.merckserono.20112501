﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Threading.Tasks;
using IND930.Library.ErrorHandling;
using Libraries.Peripherals.Printer;
using Libraries.Peripherals.Scoreboard;
using Libraries.Peripherals.SerialInterface;
using MT.Singularity.Composition;
using MT.Singularity.IO;
using MT.Singularity.Platform.Devices;
using MT.Singularity.Platform.Infrastructure;
using MT.Singularity.Serialization;

namespace IND930YMLUI.Config
{

    [Export(typeof(ISerialScoreBoardManager))]
    [InjectionBehavior(IsSingleton = true)]
    public class SerialScoreBoardManager : ISerialScoreBoardManager
    {
        private IPlatformEngine _engine;
        private ISerialScoreboardConfiguration _scoreBoardConfiguration;
        private IAutomatedSerialScoreBoard _automatedScoreBoard;
        private IManuelSerialScoreboard _manuelSerialScoreboard;
        private IErrorMessageEventProvider _errorMessageEventProvider;
        private ISerialInterfaceProvider _serialInterfaceProvider;
        private ISerialInterface _serialInterface;
        private Configuration _configuration;


        public SerialScoreBoardManager(IErrorMessageEventProvider errorMessageEventProvider, IPlatformEngine engine, ISerialInterfaceProvider serialInterfaceProvider, IComponents customerComponent, ISerialScoreboardConfiguration scoreBoardConfiguration, IAutomatedSerialScoreBoard automatedScoreBoard, IManuelSerialScoreboard manuelSerialScoreboard)
        {
            _errorMessageEventProvider = errorMessageEventProvider;
            _engine = engine;
            _serialInterfaceProvider = serialInterfaceProvider;
            _scoreBoardConfiguration = scoreBoardConfiguration;
            _automatedScoreBoard = automatedScoreBoard;
            _manuelSerialScoreboard = manuelSerialScoreboard;

            customerComponent.GetConfigurationAsync().ContinueWith(RegisterWithConfigurationEvents);
        }

        private async Task RegisterWithConfigurationEvents(Task<Configuration> configurationTask)
        {
            if (configurationTask.IsCompleted)
            {
                _configuration = configurationTask.Result;
                _scoreBoardConfiguration.NumberOfDigits = _configuration.ScoreBoardDigits;
                _serialInterface = await _serialInterfaceProvider.FindSerialInterface(_engine, _configuration.ScoreBoardPort);
                await ConfigureSerialScoreBoard(_serialInterface);
                _configuration.PropertyChanged += ConfigurationPropertyChanged;
            }
        }

        private async void ConfigurationPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "ScoreBoardPort")
            {
                _serialInterface = await _serialInterfaceProvider.FindSerialInterface(_engine, _configuration.ScoreBoardPort);
                await ConfigureSerialScoreBoard(_serialInterface);
            }


        }

        public IAutomatedSerialScoreBoard GetAutomatedScoreBoard()
        {
            return _automatedScoreBoard;
        }

        private async Task ConfigureSerialScoreBoard(ISerialInterface serialInterface)
        {

            if (serialInterface == null)
            {
                _errorMessageEventProvider.TriggerErrorMessageEvent(this, $"no serial device found");
                return;
            }
            IConnectionChannel<DataSegment> serialConnectionChannel = await serialInterface.CreateConnectionChannelAsync();
            var delimiterSerializer = new DelimiterSerializer(serialConnectionChannel, CommonDataSegments.Newline);
            _scoreBoardConfiguration.StringSerializer = new StringSerializer(delimiterSerializer);
            _automatedScoreBoard.InitScoreBoard();
        }



        public IManuelSerialScoreboard GetManuelSerialScoreboard()
        {
            //_manuelSerialScoreboard.InitScoreBoard();
            return _manuelSerialScoreboard;
        }
    }
}
