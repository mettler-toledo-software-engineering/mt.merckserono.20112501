﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.IO.Ports;
using System.Text;
using System.Threading.Tasks;
using IND930YMLUI.Infrastructure;
using Libraries.Peripherals.BarcodeScanner;
using Libraries.Peripherals.Helper;
using Libraries.Peripherals.USB;
using MT.Singularity.Composition;

namespace IND930YMLUI.Config
{
    [Export(typeof(IUsbBarcodeScannerManager))]
    [InjectionBehavior(IsSingleton = true)]
    public class UsbBarcodeScannerManager : IUsbBarcodeScannerManager
    {

        private IUsbBarcodeScannerConfiguration _scannerConfiguration;
        private IUsbDevicePortNameFinder _portNameFinder;
        private IUsbBarcodeScanner _usbBarcodeScanner;
        private Configuration _configuration;
        private EndOfDataCharacter _endOfDataCharacter;

        /// <summary>
        /// this class looks for usb barcode scanner in the devicemanager and reacts to insertion / removal of a usb devices / or change in system setup events and provides
        /// a configured and functioning scanner after the events if possible
        /// </summary>
        /// <param name="usbEventProvider"></param>
        /// <param name="portNameFinder"></param>
        /// <param name="customerComponent"></param>
        /// <param name="scannerConfiguration"></param>
        /// <param name="usbBarcodeScanner"></param>
        public UsbBarcodeScannerManager(IUsbEventProvider usbEventProvider, IUsbDevicePortNameFinder portNameFinder, IComponents customerComponent, IUsbBarcodeScannerConfiguration scannerConfiguration, IUsbBarcodeScanner usbBarcodeScanner)
        {
            _scannerConfiguration = scannerConfiguration;
            _portNameFinder = portNameFinder;
            _usbBarcodeScanner = usbBarcodeScanner;
            _endOfDataCharacter = EndOfDataCharacter.Cr;
            //usbEventProvider.DeviceInsertedEvent += UsbEventProviderOnDeviceInsertedEvent;
            //usbEventProvider.DeviceRemovedEvent += UsbEventProviderOnDeviceRemovedEvent;
            customerComponent.GetConfigurationAsync().ContinueWith(RegisterWithConfigurationEvents);
            
        }


        private void RegisterWithConfigurationEvents(Task<Configuration> configurationTask)
        {
            if (configurationTask.IsCompleted)
            {
                
                _configuration = configurationTask.Result;
                _configuration.PropertyChanged += ConfigurationPropertyChanged;

                ConfigureBarcodeScanner(_configuration.BarcodeScannerPort);
                //ConfigureBarcodeScanner(Globals.BarcodeScannerName);
                
            }
        }

        private void ConfigurationPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            //todo change des eod zeichens berücksichtigen
            if (e.PropertyName == "BarcodeScannerPort")
            {
                ConfigureBarcodeScanner(_configuration.BarcodeScannerPort);
            }
        }

        private void UsbEventProviderOnDeviceInsertedEvent(object sender, EventArgs e)
        {
            ConfigureBarcodeScanner(Globals.BarcodeScannerName);
        }

        private void UsbEventProviderOnDeviceRemovedEvent(object sender, EventArgs e)
        {
            
            ConfigureBarcodeScanner(Globals.BarcodeScannerName);
        }
        /// <summary>
        /// this method returns a initialized scanner with a configured serial port
        /// </summary>
        /// <returns></returns>
        public IUsbBarcodeScanner GetConfiguredBarcodeScanner()
        {
            return _usbBarcodeScanner;
        }

        private void ConfigureBarcodeScanner(string port)
        {
            var portname = _portNameFinder.FindPortName(port);

            if (string.IsNullOrEmpty(portname) == false && _usbBarcodeScanner.Initialized == false)
            {
                _scannerConfiguration.UsedSerialPort = new SerialPort(portname);
                _scannerConfiguration.EndOfDataCharacter = _endOfDataCharacter;
                _usbBarcodeScanner.InitBarcodeScanner();
            }
            else if(string.IsNullOrEmpty(portname) == true)
            {
                _usbBarcodeScanner.Dispose();
                _scannerConfiguration.UsedSerialPort = null;
                
            }
        }
    }
}
