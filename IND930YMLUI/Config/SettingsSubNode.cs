﻿using System;
using System.IO;
using System.Threading.Tasks;
using MT.Singularity.Collections;
using MT.Singularity.Logging;
using MT.Singularity.Platform.CommonUX.Setup.Impl;
using MT.Singularity.Platform.CommonUX.Setup.ViewModels;
using MT.Singularity.Platform.Infrastructure;
using MT.Singularity.Presentation;
using IND930.Library.Update;
using IND930YMLUI.Infrastructure;
using MT.Singularity.Composition;

namespace IND930YMLUI.Config
{
    class SettingsSubNode : GroupSetupMenuItem
    {
        private readonly Configuration _configuration;

        public SettingsSubNode(SetupMenuContext context, IComponents customerComponent, Configuration configuration)
            : base(context, new TitleAndSubtitle(Localization.GetTranslationModule(), (int)Localization.Key.GeneralSettings), configuration, customerComponent)
        {
            _configuration = configuration;

        }

        public override Task ShowChildrenAsync()
        {
            try
            {
                // Titles for the my Setup parameters


                var scannerPortTitle = new TitleAndSubtitle(Localization.GetTranslationModule(), (int)Localization.Key.BarcodeScannerPort);
                var scannerPortTarget = new TextSetupMenuItem(_context, scannerPortTitle, _configuration, "BarcodeScannerPort");

                var printerPortTitle = new TitleAndSubtitle(Localization.GetTranslationModule(), (int)Localization.Key.PrinterPort);
                var printerPortTarget = new TextSetupMenuItem(_context, printerPortTitle, _configuration, "PrinterPort");

                var roomNumberTitle = new TitleAndSubtitle(Localization.GetTranslationModule(), (int)Localization.Key.RoomNumber);
                var roomNumberTarget = new TextSetupMenuItem(_context, roomNumberTitle, _configuration, "RoomNumber");

                var scoreBoardPortTitle = new TitleAndSubtitle(Localization.GetTranslationModule(), (int)Localization.Key.ScoreBoardPort);
                var scoreBoardPortTarget = new TextSetupMenuItem(_context, scoreBoardPortTitle, _configuration, "ScoreBoardPort");

                var scoreBoardDigitsTitle = new TitleAndSubtitle(Localization.GetTranslationModule(), (int)Localization.Key.ScoreBoardDigits);
                var scoreBoardDigitsTarget = new TextSetupMenuItem(_context, scoreBoardDigitsTitle, _configuration, "ScoreBoardDigits");

                var sapScaleNumberTitle = new TitleAndSubtitle(Localization.GetTranslationModule(), (int)Localization.Key.SapScaleNumber);
                var sapScaleNumberTarget = new TextSetupMenuItem(_context, sapScaleNumberTitle, _configuration, "SapScaleNumber");

                var domainNameTitle = new TitleAndSubtitle(Localization.GetTranslationModule(), (int)Localization.Key.DomainName);
                var domainNameTarget = new TextSetupMenuItem(_context, domainNameTitle, _configuration, "DomainName");

                var domainGroup1Title = new TitleAndSubtitle(Localization.GetTranslationModule(), (int)Localization.Key.DomainGroup);
                var domainGroup1Target = new TextSetupMenuItem(_context, domainGroup1Title, _configuration, "DomainGroup1");

                var domainGroup2Title = new TitleAndSubtitle(Localization.GetTranslationModule(), (int)Localization.Key.DomainGroup);
                var domainGroup2Target = new TextSetupMenuItem(_context, domainGroup2Title, _configuration, "DomainGroup2");

                var domainGroup3Title = new TitleAndSubtitle(Localization.GetTranslationModule(), (int)Localization.Key.DomainGroup);
                var domainGroup3Target = new TextSetupMenuItem(_context, domainGroup3Title, _configuration, "DomainGroup3");

                var logoutTimeTitle = new TitleAndSubtitle(Localization.GetTranslationModule(), (int)Localization.Key.LogoutTime);
                var logoutTimeTarget = new TextSetupMenuItem(_context, logoutTimeTitle, _configuration, "LogoutTime");

                var recipeDecimalPlacesTitle = new TitleAndSubtitle(Localization.GetTranslationModule(), (int)Localization.Key.RecipeDecimalPlaces);
                var recipeDecimalPlacesTarget = new TextSetupMenuItem(_context, recipeDecimalPlacesTitle, _configuration, "RecipeDecimalPlaces");

                // sqlUserPasswordTarget.PasswordChar = char.Parse("*");

                //buttons können nicht in gruppen gelegt werden sondern müssen direkt an das child
                var button = new ImageButtonSetupMenuItem(_context, "embedded://IND930YMLUI/IND930YMLUI.images.FlashDisk.al8", new Color(0xFF, 0x00, 0x00, 0x00), Localization.GetTranslationModule(), (int)Localization.Key.Update, 20, SystemUpdate);


                var rangeGroup1 = new GroupedSetupMenuItems(_context, scannerPortTarget, printerPortTarget, scoreBoardPortTarget, scoreBoardDigitsTarget);
                var rangeGroup2 = new GroupedSetupMenuItems(_context, logoutTimeTarget, roomNumberTarget, sapScaleNumberTarget, recipeDecimalPlacesTarget);
                var rangeGroup3 = new GroupedSetupMenuItems(_context, domainNameTarget, domainGroup1Target, domainGroup2Target, domainGroup3Target);

                Children = Indexable.ImmutableValues<SetupMenuItem>(rangeGroup1,rangeGroup2, rangeGroup3, button);

                
            }
            catch (Exception ex)
            {
                Log4NetManager.ApplicationLogger.Error("Error Printer Setup Node.ShowChildrenAsync", ex);
            }

            return TaskEx.CompletedTask;
        }

        private void SystemUpdate()
        {
            bool success;
            var updateconfig = ApplicationBootstrapperBase.CompositionContainer.Resolve<ISystemUpdateConfiguration>();
            updateconfig.SourceDirectory = Globals.SystemUpdateSourceDirectory;
            updateconfig.TargetDirectory = Globals.SystemUpdateTargetDirectory;

            var update = ApplicationBootstrapperBase.CompositionContainer.Resolve<ISystemUpdate>();
            
            success = update.ExecuteUpdate();

            if (success == true)
            {
                _configuration.UsbUpdateResult = "Update copied.";
            }
            else
            {
                _configuration.UsbUpdateResult = "Update failed!";
            }
        }
    }
}
