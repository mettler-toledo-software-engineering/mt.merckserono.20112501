﻿using System.ComponentModel;
using MT.Singularity.Components;
using MT.Singularity.Platform.Configuration;

namespace IND930YMLUI.Config
{
    [Component]
    public class Configuration : ComponentConfiguration
    {

        [Setting(WritePermissions = Permissions.UntilSupervisorId)]
        [Component]
        [DefaultValue(3)]
        public virtual int PrinterPort
        {
            get { return (int)GetLocal(); }
            set { SetLocal(value); }
        }

        [Setting(WritePermissions = Permissions.UntilSupervisorId)]
        [Component]
        [DefaultValue(5)]
        public virtual int ScoreBoardPort
        {
            get { return (int)GetLocal(); }
            set { SetLocal(value); }
        }

        [Setting(WritePermissions = Permissions.UntilSupervisorId)]
        [Component]
        [DefaultValue(5)]
        public virtual int ScoreBoardDigits
        {
            get { return (int)GetLocal(); }
            set { SetLocal(value); }
        }

        [Setting(WritePermissions = Permissions.UntilSupervisorId)]
        [Component]
        [DefaultValue("COM6")]
        public virtual string BarcodeScannerPort
        {
            get { return (string)GetLocal(); }
            set { SetLocal(value); }
        }

        [Setting(WritePermissions = Permissions.UntilSupervisorId)]
        [Component]
        [DefaultValue("B0000")]
        public virtual string RoomNumber
        {
            get { return (string)GetLocal(); }
            set { SetLocal(value); }
        }

        [Setting(WritePermissions = Permissions.UntilSupervisorId)]
        [Component]
        [DefaultValue("19BL099")]
        public virtual string SapScaleNumber
        {
            get { return (string)GetLocal(); }
            set { SetLocal(value); }
        }

        [Setting(WritePermissions = Permissions.UntilSupervisorId)]
        [Component]
        [DefaultValue("automation.chau1")]
        public virtual string DomainName
        {
            get { return (string)GetLocal(); }
            set { SetLocal(value); }
        }

        [Setting(WritePermissions = Permissions.UntilSupervisorId)]
        [Component]
        [DefaultValue("operator")]
        public virtual string DomainGroup1
        {
            get { return (string)GetLocal(); }
            set { SetLocal(value); }
        }
        [Setting(WritePermissions = Permissions.UntilSupervisorId)]
        [Component]
        [DefaultValue("maintenance")]
        public virtual string DomainGroup2
        {
            get { return (string)GetLocal(); }
            set { SetLocal(value); }
        }
        [Setting(WritePermissions = Permissions.UntilSupervisorId)]
        [Component]
        [DefaultValue("administrator")]
        public virtual string DomainGroup3
        {
            get { return (string)GetLocal(); }
            set { SetLocal(value); }
        }

        [Setting(WritePermissions = Permissions.UntilSupervisorId)]
        [Component]
        [DefaultValue(15)]
        public virtual int LogoutTime
        {
            get { return (int)GetLocal(); }
            set { SetLocal(value); }
        }

        [Setting(WritePermissions = Permissions.UntilSupervisorId)]
        [Component]
        [DefaultValue(2)]
        public virtual int RecipeDecimalPlaces
        {
            get { return (int)GetLocal(); }
            set { SetLocal(value); }
        }

        [Setting(WritePermissions = Permissions.UntilSupervisorId)]
        [Component]
        [DefaultValue("")]
        public virtual string UsbUpdateResult
        {
            get { return (string)GetLocal(); }
            set { SetLocal(value); }
        }



    }
}
