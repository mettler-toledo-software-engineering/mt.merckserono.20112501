﻿using System.Threading.Tasks;
using Libraries.Peripherals.Printer;

namespace IND930YMLUI.Config
{
    public interface ISerialPrinterManager
    {
        IPrinterGa46 GetConfiguredPrinterGa46();
    }
}