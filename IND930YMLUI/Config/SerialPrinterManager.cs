﻿using System;
using System.ComponentModel;
using System.Threading.Tasks;
using IND930.Library.ErrorHandling;
using IND930.Library.Printing;
using Libraries.Peripherals.Printer;
using Libraries.Peripherals.SerialInterface;
using MT.Singularity.Composition;
using MT.Singularity.IO;
using MT.Singularity.Platform.Devices;
using MT.Singularity.Platform.Infrastructure;
using MT.Singularity.Serialization;

namespace IND930YMLUI.Config
{

    [Export(typeof(ISerialPrinterManager))]
    [InjectionBehavior(IsSingleton = true)]
    public class SerialPrinterManager : ISerialPrinterManager
    {
        private IPlatformEngine _engine;
        private ISerialPrinterConfiguration _printerConfiguration;
        private IPrinterGa46 _printerGa46;
        private IErrorMessageEventProvider _errorMessageEventProvider;
        private ISerialInterfaceProvider _serialInterfaceProvider;
        private ISerialInterface _serialInterface;
        private IPrinterTemplateConfiguration _printerTemplateConfiguration;
        private Configuration _configuration;


        public SerialPrinterManager(IErrorMessageEventProvider errorMessageEventProvider, IPlatformEngine engine, ISerialInterfaceProvider serialInterfaceProvider, IComponents customerComponent, ISerialPrinterConfiguration printerConfiguration, IPrinterGa46 serialPrinterGa46, IPrinterTemplateConfiguration printerTemplateConfiguration)
        {
            _errorMessageEventProvider = errorMessageEventProvider;
            _engine = engine;
            _serialInterfaceProvider = serialInterfaceProvider;
            _printerConfiguration = printerConfiguration;
            _printerGa46 = serialPrinterGa46;
            _printerTemplateConfiguration = printerTemplateConfiguration;

            customerComponent.GetConfigurationAsync().ContinueWith(RegisterWithConfigurationEvents);
        }

        private async Task RegisterWithConfigurationEvents(Task<Configuration> configurationTask)
        {
            if (configurationTask.IsCompleted)
            {
                _configuration = configurationTask.Result;
                _serialInterface = await _serialInterfaceProvider.FindSerialInterface(_engine, _configuration.PrinterPort);
                await ConfigureSerialPrinter(_serialInterface);
                _configuration.PropertyChanged += ConfigurationPropertyChanged;
                _printerTemplateConfiguration.RoomNumber = _configuration.RoomNumber;
            }
        }

        private async void ConfigurationPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            //todo change des eod zeichens berücksichtigen
            if (e.PropertyName == "PrinterPort")
            {
                _serialInterface = await _serialInterfaceProvider.FindSerialInterface(_engine, _configuration.PrinterPort);
                await ConfigureSerialPrinter(_serialInterface);
            }

            if (e.PropertyName == "RoomNumber")
            {
                _printerTemplateConfiguration.RoomNumber = _configuration.RoomNumber;
            }
        }

        private void OnFindSerialInterfaceCompleted(Task findSerialInterfaceTask)
        {
            if (findSerialInterfaceTask.Exception != null)
            {
                throw findSerialInterfaceTask.Exception;
            }
        }


        public IPrinterGa46 GetConfiguredPrinterGa46()
        {
            
            return _printerGa46;
        }

        private async Task ConfigureSerialPrinter(ISerialInterface serialInterface)
        {

            if (serialInterface == null)
            {
                _errorMessageEventProvider.TriggerErrorMessageEvent(this, $"no serial device found");
                return;
            }
            _printerConfiguration.SerialInterface = serialInterface;
            IConnectionChannel<DataSegment> serialConnectionChannel = await _printerConfiguration.SerialInterface.CreateConnectionChannelAsync();
            var delimiterSerializer = new DelimiterSerializer(serialConnectionChannel, CommonDataSegments.Newline);
            _printerConfiguration.StringSerializer = new StringSerializer(delimiterSerializer);
            _printerConfiguration.StringSerializer.CodePage = CodePage.Default;
            await _printerGa46.SetupPrinter();

        }


        private void TriggerErrorEvent(Exception exception)
        {
            _errorMessageEventProvider.TriggerErrorMessageEvent(this, "serial printer could not be initialized", exception);
        }
    }
}

