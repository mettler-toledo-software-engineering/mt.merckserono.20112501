﻿using Libraries.Peripherals.Scoreboard;

namespace IND930YMLUI.Config
{
    public interface ISerialScoreBoardManager
    {
        IAutomatedSerialScoreBoard GetAutomatedScoreBoard();
        IManuelSerialScoreboard GetManuelSerialScoreboard();
    }
}