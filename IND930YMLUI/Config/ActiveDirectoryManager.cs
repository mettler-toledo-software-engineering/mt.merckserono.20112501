﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Threading.Tasks;
using IND930YMLUI.Infrastructure;
using Libraries.Authentication.ActiveDirectory;
using Libraries.Authentication.LoginAndLogout;
using MT.Singularity.Composition;

namespace IND930YMLUI.Config
{

    [Export(typeof(ActiveDirectoryManager))]
    [InjectionBehavior(IsSingleton = true)]
    public class ActiveDirectoryManager
    {
        private Configuration _configuration;
        private IActiveDirectoryConfiguration _activeDirectoryConfiguration;
        private IAutoLogoutConfiguration _logoutConfiguration;
        private IComponents _component;

        public ActiveDirectoryManager(IComponents customerComponent, IActiveDirectoryConfiguration activeDirectoryConfiguration, IActiveDirectoryAuthentification activeDirectoryAuthentification, IAutoLogoutConfiguration autoLogoutConfiguration)
        {
            _component = customerComponent;
            _activeDirectoryConfiguration = activeDirectoryConfiguration;
            _logoutConfiguration = autoLogoutConfiguration;
            _component.GetConfigurationAsync().ContinueWith(RegisterWithConfigurationEvents);
        }
        private void RegisterWithConfigurationEvents(Task<Configuration> configurationTask)
        {
            if (configurationTask.IsCompleted)
            {
                _configuration = configurationTask.Result;
                _activeDirectoryConfiguration.DomainName = _configuration.DomainName;
                _logoutConfiguration.LogoutTime = _configuration.LogoutTime;
                _activeDirectoryConfiguration.DomainGroups.Clear();
                _activeDirectoryConfiguration.DomainGroups.Add(1, _configuration.DomainGroup1);
                _activeDirectoryConfiguration.DomainGroups.Add(2, _configuration.DomainGroup2);
                _activeDirectoryConfiguration.DomainGroups.Add(3, _configuration.DomainGroup3);
            }
            _configuration.PropertyChanged += ConfigurationPropertyChanged;
        }

        private void ConfigurationPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "DomainName")
            {
                _activeDirectoryConfiguration.DomainName = _configuration.DomainName;
            }
            if (e.PropertyName  == "DomainGroup1")
            {
                _activeDirectoryConfiguration.DomainGroups[1] = _configuration.DomainGroup1;
            }
            if (e.PropertyName == "DomainGroup2")
            {
                _activeDirectoryConfiguration.DomainGroups[2] = _configuration.DomainGroup2;
            }
            if (e.PropertyName == "DomainGroup3")
            {
                _activeDirectoryConfiguration.DomainGroups[3] = _configuration.DomainGroup3;
            }

            if (e.PropertyName == "LogoutTime")
            {
                _logoutConfiguration.LogoutTime = _configuration.LogoutTime;
            }
        }

    }
}
