﻿using MT.Singularity.Platform.Configuration;

namespace IND930YMLUI.Config
{
    public interface IComponents : IConfigurable<Configuration>
    {
    }
}
