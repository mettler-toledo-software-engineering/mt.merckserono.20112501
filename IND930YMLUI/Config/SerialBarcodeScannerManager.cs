﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using IND930.Library.ErrorHandling;
using Libraries.Peripherals.BarcodeScanner;
using Libraries.Peripherals.Helper;
using Libraries.Peripherals.SerialInterface;
using MT.Singularity.Composition;
using MT.Singularity.Logging;
using MT.Singularity.Platform.Devices;
using MT.Singularity.Platform.Infrastructure;

namespace IND930YMLUI.Config
{
    [Export(typeof(ISerialBarcodeScannerManager))]
    [InjectionBehavior(IsSingleton = true)]
    public class SerialBarcodeScannerManager : ISerialBarcodeScannerManager
    {
        private IPlatformEngine _engine;
        private ISerialBarcodeScannerConfiguration _scannerConfiguration;
        private ISerialBarcodeScanner _barcodeScanner;
        private IErrorMessageEventProvider _errorMessageEventProvider;
        private ISerialInterfaceProvider _serialInterfaceProvider;
        private ISerialInterface _serialInterface;
        private Configuration _configuration;
        

        public SerialBarcodeScannerManager(IErrorMessageEventProvider errorMessageEventProvider, IPlatformEngine engine, ISerialInterfaceProvider serialInterfaceProvider, IComponents customerComponent, ISerialBarcodeScannerConfiguration scannerConfiguration, ISerialBarcodeScanner serialBarcodeScanner)
        {
            _errorMessageEventProvider = errorMessageEventProvider;
            _engine = engine;
            _serialInterfaceProvider = serialInterfaceProvider;
            _scannerConfiguration = scannerConfiguration;
            _barcodeScanner = serialBarcodeScanner;
        
            customerComponent.GetConfigurationAsync().ContinueWith(RegisterWithConfigurationEvents);
        }

        private async Task RegisterWithConfigurationEvents(Task<Configuration> configurationTask)
        {
            if (configurationTask.IsCompleted)
            {
                _configuration = configurationTask.Result;
                _serialInterface = await _serialInterfaceProvider.FindSerialInterface(_engine, _configuration.BarcodeScannerPort);
                await ConfigureSerialBarcodeScanner(_serialInterface);
                _configuration.PropertyChanged += ConfigurationPropertyChanged;
            }
        }

        private async void ConfigurationPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            //todo change des eod zeichens berücksichtigen
            if (e.PropertyName == "BarcodeScannerPort")
            {
                _serialInterface = await _serialInterfaceProvider.FindSerialInterface(_engine, _configuration.BarcodeScannerPort);
                await ConfigureSerialBarcodeScanner(_serialInterface);
            }
        }

        private void OnFindSerialInterfaceCompleted(Task findSerialInterfaceTask)
        {
            if (findSerialInterfaceTask.Exception != null)
            {
                throw findSerialInterfaceTask.Exception;
            }
        }

        //private async Task FindSerialInterface(IPlatformEngine engine, string portname)
        //{
        //    var interfaceservice = await engine.GetInterfaceServiceAsync();
        //    var allinterfaces = await interfaceservice.GetAllInterfacesAsync();
        //    var allserialinterfaces = allinterfaces.OfType<ISerialInterface>().ToList();
        //    var serialInterface = allserialinterfaces.FirstOrDefault(serial => serial.PortName == portname);

        //    string availableports = "available Ports: ";
        //    allserialinterfaces.ForEach(entry => availableports += $"{entry.PortName} ");
        //    Log4NetManager.ApplicationLogger.Info(availableports);
        //    Console.WriteLine(availableports);
        //    if (serialInterface != null)
        //    {
        //        await ConfigureSerialBarcodeReader(serialInterface).ContinueWith(OnInitError); ;
        //    }
        //    else
        //    {
        //        _errorMessageEventProvider.TriggerErrorMessageEvent(this, $"no serial device found on port {portname}; {availableports}");
        //    }
        //}

        public ISerialBarcodeScanner GetConfiguredBarcodeScanner()
        {
            return _barcodeScanner;
        }

        private async Task ConfigureSerialBarcodeScanner(ISerialInterface serialInterface)
        {

            if (serialInterface == null)
            {
                _errorMessageEventProvider.TriggerErrorMessageEvent(this, $"no serial device found");
                return;
            }

            var serialconnection = await serialInterface.CreateConnectionChannelAsync();
            if (serialconnection != null && _barcodeScanner.Initialized)
            {
                _scannerConfiguration.SerialConnectionChannel = null;
                _barcodeScanner.Dispose();
            }
            if (serialconnection != null && _barcodeScanner.Initialized == false)
            {
                _scannerConfiguration.SerialConnectionChannel = serialconnection;
                await _barcodeScanner.InitBarcodeScanner().ContinueWith(OnInitError);
            }
            

        }

        private void OnInitError(Task<bool> initTask)
        {
            if (initTask.Exception != null)
            {
                TriggerErrorEvent(initTask.Exception);
            }
        }
        private void OnInitError(Task initTask)
        {
            if (initTask.Exception != null)
            {
                TriggerErrorEvent(initTask.Exception);
            }
        }

        private void TriggerErrorEvent(Exception exception)
        {
            _errorMessageEventProvider.TriggerErrorMessageEvent(this, "serial scanner could not be initialized", exception);
        }
    }
}
