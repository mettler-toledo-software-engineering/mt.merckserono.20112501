﻿using Libraries.Peripherals.BarcodeScanner;

namespace IND930YMLUI.Config
{
    public interface ISerialBarcodeScannerManager
    {
        ISerialBarcodeScanner GetConfiguredBarcodeScanner();
    }
}