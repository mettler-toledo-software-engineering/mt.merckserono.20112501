﻿using System;

using System.Collections.Generic;
using System.Text;
using IND930.Library.FillProcess.States;

namespace IND930YMLUI.TranslationHelper
{
    public static class ProcessStepTextProvider
    {
        private const string BeakerImage = "embedded://IND930YMLUI/IND930YMLUI.images.Beaker New.al8";
        private const string AddImage = "embedded://IND930YMLUI/IND930YMLUI.images.Add.al8";
        private const string PtImage = "embedded://IND930YMLUI/IND930YMLUI.images.Weight-Scale.al8";

        private const string ExitImage = "embedded://IND930YMLUI/IND930YMLUI.images.Exit.al8";
        private const string CancelImage = "embedded://IND930YMLUI/IND930YMLUI.images.Cancel.al8";
        public static string ToAddButtonTextTranslation(this ProcessStateEnum processState)
        {
            switch (processState)
            {
                case ProcessStateEnum.InputFormula: return Localization.Get(Localization.Key.Formel);
                case ProcessStateEnum.InputBatchNumber: return Localization.Get(Localization.Key.BatchNo);
                case ProcessStateEnum.AddComponent: return Localization.Get(Localization.Key.Component);
                case ProcessStateEnum.TareFormula: return Localization.Get(Localization.Key.TareManuel);
                default: return string.Empty;
            }
        }

        public static string ToInstructionLineTranslation(this ProcessStateEnum processState)
        {
            switch (processState)
            {
                case ProcessStateEnum.InputFormula: return Localization.Get(Localization.Key.StateTextFormula);
                case ProcessStateEnum.InputBatchNumber: return Localization.Get(Localization.Key.StateTextBatchNo);
                case ProcessStateEnum.EmptyScale: return Localization.Get(Localization.Key.StateTextEmptyScale);
                case ProcessStateEnum.TareFormula: return $"{Localization.Get(Localization.Key.StateTextTareScale1)}\n{Localization.Get(Localization.Key.StateTextTareScale2)}"; 
                case ProcessStateEnum.AddComponent: return $"{Localization.Get(Localization.Key.StateTextComponent1)}\n{Localization.Get(Localization.Key.StateTextComponent2)}";  ;
                case ProcessStateEnum.PrintTotalNet: return $"{Localization.Get(Localization.Key.StateTextPrintTotal)}";

                default: return string.Empty;
            }
        }

        public static string ToAddButtonImage(this ProcessStateEnum processState)
        {
            switch (processState)
            {
                case ProcessStateEnum.InputFormula: return AddImage;
                case ProcessStateEnum.InputBatchNumber: return AddImage;
                case ProcessStateEnum.EmptyScale: return AddImage;
                case ProcessStateEnum.AddComponent: return BeakerImage;
                case ProcessStateEnum.TareFormula: return PtImage;
                default: return AddImage;
            }
        }

        public static string ToFinishButtonImage(this ProcessStateEnum processState)
        {
            switch (processState)
            {
                case ProcessStateEnum.InputFormula: return CancelImage;
                case ProcessStateEnum.InputBatchNumber: return CancelImage;
                case ProcessStateEnum.EmptyScale: return CancelImage;
                case ProcessStateEnum.TareFormula: return CancelImage;
                case ProcessStateEnum.AddComponent: return CancelImage;
                case ProcessStateEnum.PrintTotalNet: return ExitImage;
                default: return CancelImage;
            }
        }

        public static string ToFinishButtonTextTranslation(this ProcessStateEnum processState)
        {
            switch (processState)
            {
                case ProcessStateEnum.InputFormula: return Localization.Get(Localization.Key.Cancel);
                case ProcessStateEnum.InputBatchNumber: return Localization.Get(Localization.Key.Cancel);
                case ProcessStateEnum.EmptyScale: return Localization.Get(Localization.Key.Cancel);
                case ProcessStateEnum.TareFormula: return Localization.Get(Localization.Key.Cancel);
                case ProcessStateEnum.AddComponent: return Localization.Get(Localization.Key.Cancel);
                case ProcessStateEnum.PrintTotalNet: return Localization.Get(Localization.Key.Exit);
                default: return string.Empty;
            }
        }
    }
}
