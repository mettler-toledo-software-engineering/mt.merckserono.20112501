﻿using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Controls.Grid;
using MT.Singularity.Presentation.Controls.DataGrid;
using MT.Singularity.Presentation.Controls.Navigation;
using MT.Singularity.Platform.CommonUX.Controls;
using MT.Singularity.Platform.CommonUX.Controls.MeasurementDisplay.View;
namespace IND930YMLUI.Views
{
    [System.CodeDom.Compiler.GeneratedCodeAttribute("YmlCodeGenerator", "2.1.0.0")]
    public partial class HomeScreen : MT.Singularity.Presentation.Controls.Navigation.NavigationPage
    {
        private MT.Singularity.Presentation.Controls.DynamicDockPanel _weightWindowControl;
        private void InitializeComponents()
        {
            MT.Singularity.Presentation.Controls.DockPanel internal1;
            MT.Singularity.Presentation.Controls.DockPanel internal2;
            MT.Singularity.Presentation.Controls.DockPanel internal3;
            MT.Singularity.Platform.CommonUX.Controls.SoftKeyRibbon internal4;
            MT.Singularity.Platform.CommonUX.Controls.SoftKeyItem internal5;
            MT.Singularity.Platform.CommonUX.Controls.ZeroSoftkey internal6;
            MT.Singularity.Platform.CommonUX.Controls.SoftKeyItem internal7;
            MT.Singularity.Platform.CommonUX.Controls.TareSoftkey internal8;
            MT.Singularity.Platform.CommonUX.Controls.SoftKeyItem internal9;
            MT.Singularity.Platform.CommonUX.Controls.ClearSoftkey internal10;
            MT.Singularity.Platform.CommonUX.Controls.SoftKeyItem internal11;
            MT.Singularity.Platform.CommonUX.Controls.ImageButton internal12;
            MT.Singularity.Platform.CommonUX.Controls.SoftKeyItem internal13;
            MT.Singularity.Platform.CommonUX.Controls.ImageButton internal14;
            _weightWindowControl = new MT.Singularity.Presentation.Controls.DynamicDockPanel();
            _weightWindowControl.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            _weightWindowControl.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Right;
            internal2 = new MT.Singularity.Presentation.Controls.DockPanel();
            internal2.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            internal2.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Right;
            internal5 = new MT.Singularity.Platform.CommonUX.Controls.SoftKeyItem();
            internal5.Index = 0;
            internal6 = new MT.Singularity.Platform.CommonUX.Controls.ZeroSoftkey();
            internal5.Content = internal6;
            internal7 = new MT.Singularity.Platform.CommonUX.Controls.SoftKeyItem();
            internal7.Index = 1;
            internal8 = new MT.Singularity.Platform.CommonUX.Controls.TareSoftkey();
            internal7.Content = internal8;
            internal9 = new MT.Singularity.Platform.CommonUX.Controls.SoftKeyItem();
            internal9.Index = 2;
            internal10 = new MT.Singularity.Platform.CommonUX.Controls.ClearSoftkey();
            internal9.Content = internal10;
            internal11 = new MT.Singularity.Platform.CommonUX.Controls.SoftKeyItem();
            internal11.Index = 3;
            internal12 = new MT.Singularity.Platform.CommonUX.Controls.ImageButton();
            internal12.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Bottom;
            internal12.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal12.Margin = new MT.Singularity.Presentation.Thickness(2, 0, 0, 2);
            this.bindings[0] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal12.Text,() =>  _homeScreenViewModel.LoginButtonText,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal12.CustomClass = "ImageButtonDefaultHomeScreenButton";
            this.bindings[1] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal12.ImageSource,() =>  _homeScreenViewModel.LoginButtonImage,MT.Singularity.Expressions.BindingMode.OneWay,false);
            this.bindings[2] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal12.Command,() =>  _homeScreenViewModel.LoginCommand,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal11.Content = internal12;
            internal13 = new MT.Singularity.Platform.CommonUX.Controls.SoftKeyItem();
            internal13.Index = 7;
            internal14 = new MT.Singularity.Platform.CommonUX.Controls.ImageButton();
            internal14.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Bottom;
            internal14.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal14.Margin = new MT.Singularity.Presentation.Thickness(2, 0, 0, 2);
            internal14.Text = IND930YMLUI.Localization.Get(IND930YMLUI.Localization.Key.Start);
            internal14.AddTranslationAction(() => {
                internal14.Text = IND930YMLUI.Localization.Get(IND930YMLUI.Localization.Key.Start);
            });
            internal14.CustomClass = "ImageButtonDefaultHomeScreenButton";
            internal14.ImageSource = "embedded://IND930YMLUI/IND930YMLUI.images.Start.al8";
            this.bindings[3] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal14.Command,() =>  _homeScreenViewModel.StartProcessCommand,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal13.Content = internal14;
            internal4 = new MT.Singularity.Platform.CommonUX.Controls.SoftKeyRibbon(internal5, internal7, internal9, internal11, internal13);
            internal4.SoftKeysPerPage = 8;
            internal4.Height = 90;
            internal3 = new MT.Singularity.Presentation.Controls.DockPanel(internal4);
            internal3.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Bottom;
            internal1 = new MT.Singularity.Presentation.Controls.DockPanel(_weightWindowControl, internal2, internal3);
            internal1.PointerDown += _homeScreenViewModel.OnPointerDown;
            this.Content = internal1;
        }
        private readonly MT.Singularity.Expressions.IDataBinding[] bindings = new MT.Singularity.Expressions.IDataBinding[4];
    }
}
