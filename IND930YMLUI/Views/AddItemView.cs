﻿using System;
using IND930.Library.FillProcess.States;
using IND930YMLUI.ViewModels;
using MT.Singularity.Composition;

namespace IND930YMLUI.Views
{
    /// <summary>
    /// Interaction logic for AddItemView
    /// </summary>
    [Export(typeof(AddItemView))]
    public partial class AddItemView
    {
        private AddItemViewModel _addItemViewModel;
        public AddItemView(AddItemViewModel addItemViewModel) 
        {
            _addItemViewModel = addItemViewModel;
            _addItemViewModel.ParentChildWindow = this;

            InitializeComponents();
        }

        protected override void OnShown(EventArgs e)
        {
            _addItemViewModel.SetTitle();
        }
    }
}
