﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Runtime.InteropServices;
using System.Text;
using IND930.Library.Models;
using IND930YMLUI.ViewModels;
using MT.Singularity;
using MT.Singularity.Composition;
using MT.Singularity.Presentation;
using MT.Singularity.Presentation.Controls;

namespace IND930YMLUI.Views
{
    /// <summary>
    /// Interaction logic for LoginView
    /// </summary>
    [Export(typeof(LoginView))]
    public partial class LoginView
    {

        private LoginViewModel _loginViewModel;
        public LoginView(LoginViewModel loginViewModel)
        {
            _loginViewModel = loginViewModel;
            _loginViewModel.ParentChildWindow = this;

            InitializeComponents();
        }
    }
}
