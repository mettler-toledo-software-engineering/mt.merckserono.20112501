﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Text;
using IND930YMLUI.ViewModels;
using MT.Singularity;
using MT.Singularity.Composition;
using MT.Singularity.Presentation;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Controls.Navigation;

namespace IND930YMLUI.Views
{
    /// <summary>
    /// Interaction logic for FillProcessView
    /// </summary>
    [Export(typeof(FillProcessView))]
    public partial class FillProcessView
    {
        private readonly FillProcessViewModel _fillProcessViewModel;
        private readonly WeightWindowControl _weightWindow;
        public FillProcessView(FillProcessViewModel fillProcessViewModel, WeightWindowControl weightWindow)
        {
            _fillProcessViewModel = fillProcessViewModel;
            _weightWindow = weightWindow;
            InitializeComponents();
        }

        /// <summary>
        /// This method is called before the home screen is shown.
        /// </summary>
        public void BeforeStart(IRootVisualProvider rootVisualProvider)
        {
        }

        


        /// <summary>
        /// Called when a page is being navigated to for the first time.
        /// </summary>
        protected override void OnFirstNavigation()
        {
            _weightWindowControl.Add(_weightWindow);
            _fillProcessViewModel.RegisterEventsForViewModel();
            _fillProcessViewModel.UpdateProperties();
            _fillProcessViewModel.ParentPage = this;
            base.OnFirstNavigation();
        }

        /// <summary>
        /// Called when the user is navigating away from the current page to <paramref name="nextPage" />.
        /// </summary>
        /// <param name="nextPage">The next page the user is navigating to.</param>
        /// <returns>
        /// A value how the navigation framework should proceed with the navigation request.
        /// </returns>
        protected override NavigationResult OnNavigatingAway(INavigationPage nextPage)
        {
            var result = base.OnNavigatingAway(nextPage);
            _fillProcessViewModel.UnregisterEventsForViewModel();
            if (result == NavigationResult.Proceed)
            {
                _weightWindowControl.Remove(_weightWindow);
                _fillProcessViewModel.UpdateProperties();

            }
            return result;
        }

        /// <summary>
        /// Called when a page is reactivated when returning from another page.
        /// </summary>
        /// <param name="previousPage">The page that the user is returning from.</param>
        protected override void OnNavigationReturning(INavigationPage previousPage)
        {
            base.OnNavigationReturning(previousPage);
            _fillProcessViewModel.RegisterEventsForViewModel();
            _fillProcessViewModel.ParentPage = this;
            _fillProcessViewModel.UpdateProperties();
            _weightWindowControl.Add(_weightWindow);
        }

        protected override NavigationResult OnNavigatingBack(INavigationPage nextPage)
        {
            _fillProcessViewModel.UnregisterEventsForViewModel();
            _fillProcessViewModel.UpdateProperties();
            _weightWindowControl.Remove(_weightWindow);
            return base.OnNavigatingBack(nextPage);
        }
    }
}
