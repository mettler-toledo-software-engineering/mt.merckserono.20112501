﻿using MT.Singularity.Presentation.Controls;
using MT.Singularity.Platform.CommonUX.Controls;
using IND930YMLUI.Infrastructure;
namespace IND930YMLUI.Views
{
    [System.CodeDom.Compiler.GeneratedCodeAttribute("YmlCodeGenerator", "2.1.0.0")]
    public partial class LoginView : MT.Singularity.Presentation.Controls.ChildWindow
    {
        private void InitializeComponents()
        {
            MT.Singularity.Presentation.Controls.StackPanel internal1;
            MT.Singularity.Presentation.Controls.StackPanel internal2;
            MT.Singularity.Presentation.Controls.TextBlock internal3;
            MT.Singularity.Presentation.Controls.TextBox internal4;
            MT.Singularity.Presentation.Controls.StackPanel internal5;
            MT.Singularity.Presentation.Controls.TextBlock internal6;
            MT.Singularity.Presentation.Controls.TextBox internal7;
            MT.Singularity.Presentation.Controls.StackPanel internal8;
            MT.Singularity.Presentation.Controls.TextBlock internal9;
            MT.Singularity.Presentation.Controls.TextBox internal10;
            MT.Singularity.Presentation.Controls.StackPanel internal11;
            MT.Singularity.Presentation.Controls.TextBlock internal12;
            MT.Singularity.Presentation.Controls.TextBox internal13;
            MT.Singularity.Presentation.Controls.TextBlock internal14;
            MT.Singularity.Presentation.Controls.StackPanel internal15;
            MT.Singularity.Platform.CommonUX.Controls.ImageButton internal16;
            MT.Singularity.Platform.CommonUX.Controls.ImageButton internal17;
            internal3 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal3.Text = IND930YMLUI.Localization.Get(IND930YMLUI.Localization.Key.Username);
            internal3.AddTranslationAction(() => {
                internal3.Text = IND930YMLUI.Localization.Get(IND930YMLUI.Localization.Key.Username);
            });
            internal3.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Left;
            internal3.Padding = new MT.Singularity.Presentation.Thickness(0, 5, 0, 0);
            internal3.MinHeight = 50;
            internal3.MinWidth = 260;
            internal3.FontSize = ((System.Nullable<System.Int32>)30);
            internal4 = new MT.Singularity.Presentation.Controls.TextBox();
            this.bindings[0] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal4.Text,() =>  _loginViewModel.Username,MT.Singularity.Expressions.BindingMode.TwoWay,false);
            internal4.MinHeight = 50;
            internal4.MinWidth = 300;
            internal4.FontSize = ((System.Nullable<System.Int32>)30);
            internal2 = new MT.Singularity.Presentation.Controls.StackPanel(internal3, internal4);
            internal2.Orientation = MT.Singularity.Presentation.Orientation.Horizontal;
            internal2.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal2.Margin = new MT.Singularity.Presentation.Thickness(0, 20, 0, 20);
            internal6 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal6.Text = IND930YMLUI.Localization.Get(IND930YMLUI.Localization.Key.Password);
            internal6.AddTranslationAction(() => {
                internal6.Text = IND930YMLUI.Localization.Get(IND930YMLUI.Localization.Key.Password);
            });
            internal6.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Left;
            internal6.Padding = new MT.Singularity.Presentation.Thickness(0, 5, 0, 0);
            internal6.MinHeight = 50;
            internal6.MinWidth = 260;
            internal6.FontSize = ((System.Nullable<System.Int32>)30);
            internal7 = new MT.Singularity.Presentation.Controls.TextBox();
            this.bindings[1] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal7.Text,() =>  _loginViewModel.Password,MT.Singularity.Expressions.BindingMode.TwoWay,false);
            internal7.PasswordChar = _loginViewModel.PasswordChar;
            internal7.MinHeight = 50;
            internal7.MinWidth = 300;
            internal7.FontSize = ((System.Nullable<System.Int32>)30);
            internal5 = new MT.Singularity.Presentation.Controls.StackPanel(internal6, internal7);
            internal5.Orientation = MT.Singularity.Presentation.Orientation.Horizontal;
            internal5.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal5.Margin = new MT.Singularity.Presentation.Thickness(0, 20, 0, 20);
            internal9 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal9.Text = IND930YMLUI.Localization.Get(IND930YMLUI.Localization.Key.NewPassword);
            internal9.AddTranslationAction(() => {
                internal9.Text = IND930YMLUI.Localization.Get(IND930YMLUI.Localization.Key.NewPassword);
            });
            internal9.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Left;
            internal9.Padding = new MT.Singularity.Presentation.Thickness(0, 10, 0, 0);
            internal9.MinHeight = 50;
            internal9.MinWidth = 260;
            internal9.FontSize = ((System.Nullable<System.Int32>)24);
            internal10 = new MT.Singularity.Presentation.Controls.TextBox();
            this.bindings[2] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal10.Text,() =>  _loginViewModel.NewPassword,MT.Singularity.Expressions.BindingMode.TwoWay,false);
            internal10.PasswordChar = _loginViewModel.PasswordChar;
            internal10.MinHeight = 50;
            internal10.MinWidth = 300;
            internal10.FontSize = ((System.Nullable<System.Int32>)30);
            internal8 = new MT.Singularity.Presentation.Controls.StackPanel(internal9, internal10);
            internal8.Orientation = MT.Singularity.Presentation.Orientation.Horizontal;
            internal8.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            this.bindings[3] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal8.Visibility,() =>  _loginViewModel.NewPasswordVisibility,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal8.Margin = new MT.Singularity.Presentation.Thickness(0, 20, 0, 20);
            internal12 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal12.Text = IND930YMLUI.Localization.Get(IND930YMLUI.Localization.Key.NewPasswordRepeat);
            internal12.AddTranslationAction(() => {
                internal12.Text = IND930YMLUI.Localization.Get(IND930YMLUI.Localization.Key.NewPasswordRepeat);
            });
            internal12.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Left;
            internal12.Padding = new MT.Singularity.Presentation.Thickness(0, 10, 0, 0);
            internal12.MinHeight = 50;
            internal12.MinWidth = 260;
            internal12.FontSize = ((System.Nullable<System.Int32>)24);
            internal13 = new MT.Singularity.Presentation.Controls.TextBox();
            this.bindings[4] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal13.Text,() =>  _loginViewModel.NewPasswordRepeat,MT.Singularity.Expressions.BindingMode.TwoWay,false);
            internal13.PasswordChar = _loginViewModel.PasswordChar;
            internal13.MinHeight = 50;
            internal13.MinWidth = 300;
            internal13.FontSize = ((System.Nullable<System.Int32>)30);
            internal11 = new MT.Singularity.Presentation.Controls.StackPanel(internal12, internal13);
            internal11.Orientation = MT.Singularity.Presentation.Orientation.Horizontal;
            internal11.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            this.bindings[5] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal11.Visibility,() =>  _loginViewModel.NewPasswordVisibility,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal11.Margin = new MT.Singularity.Presentation.Thickness(0, 20, 0, 20);
            internal14 = new MT.Singularity.Presentation.Controls.TextBlock();
            this.bindings[6] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal14.Text,() =>  _loginViewModel.Message,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal14.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Left;
            internal14.Margin = new MT.Singularity.Presentation.Thickness(20, 0, 0, 0);
            internal14.Height = 40;
            internal14.FontSize = ((System.Nullable<System.Int32>)30);
            this.bindings[7] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal14.Foreground,() =>  Globals.Red,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal16 = new MT.Singularity.Platform.CommonUX.Controls.ImageButton();
            internal16.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            internal16.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Left;
            internal16.Height = 90;
            internal16.Width = 150;
            internal16.Text = IND930YMLUI.Localization.Get(IND930YMLUI.Localization.Key.Ok);
            internal16.AddTranslationAction(() => {
                internal16.Text = IND930YMLUI.Localization.Get(IND930YMLUI.Localization.Key.Ok);
            });
            internal16.FontSize = ((System.Nullable<System.Int32>)24);
            internal16.CustomClass = "ImageButtonDefaultHomeScreenButton";
            internal16.ImageSource = "embedded://IND930YMLUI/IND930YMLUI.images.ok.al8";
            this.bindings[8] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal16.Command,() =>  _loginViewModel.ConfirmCommand,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal16.Margin = new MT.Singularity.Presentation.Thickness(0, 20, 60, 20);
            internal17 = new MT.Singularity.Platform.CommonUX.Controls.ImageButton();
            internal17.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            internal17.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Left;
            internal17.Height = 90;
            internal17.Width = 150;
            internal17.FontSize = ((System.Nullable<System.Int32>)24);
            internal17.Text = IND930YMLUI.Localization.Get(IND930YMLUI.Localization.Key.Cancel);
            internal17.AddTranslationAction(() => {
                internal17.Text = IND930YMLUI.Localization.Get(IND930YMLUI.Localization.Key.Cancel);
            });
            internal17.CustomClass = "ImageButtonDefaultHomeScreenButton";
            internal17.ImageSource = "embedded://IND930YMLUI/IND930YMLUI.images.Cancel.al8";
            this.bindings[9] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal17.Command,() =>  _loginViewModel.CancelCommand,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal17.Margin = new MT.Singularity.Presentation.Thickness(0, 20, 0, 20);
            internal15 = new MT.Singularity.Presentation.Controls.StackPanel(internal16, internal17);
            internal15.Orientation = MT.Singularity.Presentation.Orientation.Horizontal;
            internal15.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal1 = new MT.Singularity.Presentation.Controls.StackPanel(internal2, internal5, internal8, internal11, internal14, internal15);
            internal1.Orientation = MT.Singularity.Presentation.Orientation.Vertical;
            this.Content = internal1;
            this.bindings[10] = MT.Singularity.Expressions.ExpressionBinding.Create(() => this.Height,() =>  _loginViewModel.WindowHeight,MT.Singularity.Expressions.BindingMode.OneWay,false);
            this.Width = 620;
        }
        private readonly MT.Singularity.Expressions.IDataBinding[] bindings = new MT.Singularity.Expressions.IDataBinding[11];
    }
}
