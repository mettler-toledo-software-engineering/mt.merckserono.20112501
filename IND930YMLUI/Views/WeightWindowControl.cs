﻿using MT.Singularity.Composition;

namespace IND930YMLUI.Views
{
    /// <summary>
    /// Interaction logic for WeightWindowControl
    /// </summary>
    [Export(typeof(WeightWindowControl))]
    [InjectionBehavior(IsSingleton = true)]
    public partial class WeightWindowControl
    {
        public WeightWindowControl()
        {
            InitializeComponents();
            myWeightWindow.Activate();
        }
    }
}
