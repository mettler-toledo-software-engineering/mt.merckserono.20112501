﻿using MT.Singularity.Presentation.Controls;
using MT.Singularity.Platform.CommonUX.Controls;
using IND930YMLUI.Infrastructure;
namespace IND930YMLUI.Views
{
    [System.CodeDom.Compiler.GeneratedCodeAttribute("YmlCodeGenerator", "2.1.0.0")]
    public partial class AddItemView : MT.Singularity.Presentation.Controls.ChildWindow
    {
        private void InitializeComponents()
        {
            MT.Singularity.Presentation.Controls.StackPanel internal1;
            MT.Singularity.Presentation.Controls.StackPanel internal2;
            MT.Singularity.Presentation.Controls.TextBlock internal3;
            MT.Singularity.Presentation.Controls.TextBox internal4;
            MT.Singularity.Presentation.Controls.StackPanel internal5;
            MT.Singularity.Platform.CommonUX.Controls.ImageButton internal6;
            MT.Singularity.Platform.CommonUX.Controls.ImageButton internal7;
            internal3 = new MT.Singularity.Presentation.Controls.TextBlock();
            this.bindings[0] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal3.Text,() =>  _addItemViewModel.Title,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal3.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Left;
            internal3.MinHeight = 50;
            internal3.MinWidth = 240;
            internal3.FontSize = ((System.Nullable<System.Int32>)30);
            internal2 = new MT.Singularity.Presentation.Controls.StackPanel(internal3);
            internal2.Orientation = MT.Singularity.Presentation.Orientation.Horizontal;
            internal2.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Left;
            internal2.Margin = new MT.Singularity.Presentation.Thickness(65, 20, 20, 20);
            internal4 = new MT.Singularity.Presentation.Controls.TextBox();
            internal4.Margin = new MT.Singularity.Presentation.Thickness(65, 20, 0, 20);
            this.bindings[1] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal4.Text,() =>  _addItemViewModel.EnteredItem,MT.Singularity.Expressions.BindingMode.TwoWay,false);
            internal4.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Left;
            internal4.MinHeight = 50;
            internal4.MinWidth = 240;
            internal4.FontSize = ((System.Nullable<System.Int32>)30);
            internal6 = new MT.Singularity.Platform.CommonUX.Controls.ImageButton();
            internal6.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            internal6.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Left;
            internal6.Height = 90;
            internal6.Width = 150;
            internal6.Text = IND930YMLUI.Localization.Get(IND930YMLUI.Localization.Key.Ok);
            internal6.AddTranslationAction(() => {
                internal6.Text = IND930YMLUI.Localization.Get(IND930YMLUI.Localization.Key.Ok);
            });
            internal6.FontSize = ((System.Nullable<System.Int32>)24);
            internal6.CustomClass = "ImageButtonDefaultHomeScreenButton";
            internal6.ImageSource = "embedded://IND930YMLUI/IND930YMLUI.images.ok.al8";
            this.bindings[2] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal6.Command,() =>  _addItemViewModel.ConfirmCommand,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal6.Margin = new MT.Singularity.Presentation.Thickness(65, 20, 60, 20);
            internal7 = new MT.Singularity.Platform.CommonUX.Controls.ImageButton();
            internal7.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            internal7.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Left;
            internal7.Height = 90;
            internal7.Width = 150;
            internal7.FontSize = ((System.Nullable<System.Int32>)24);
            internal7.Text = IND930YMLUI.Localization.Get(IND930YMLUI.Localization.Key.Cancel);
            internal7.AddTranslationAction(() => {
                internal7.Text = IND930YMLUI.Localization.Get(IND930YMLUI.Localization.Key.Cancel);
            });
            internal7.CustomClass = "ImageButtonDefaultHomeScreenButton";
            internal7.ImageSource = "embedded://IND930YMLUI/IND930YMLUI.images.Cancel.al8";
            this.bindings[3] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal7.Command,() =>  _addItemViewModel.CancelCommand,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal7.Margin = new MT.Singularity.Presentation.Thickness(0, 20, 0, 20);
            internal5 = new MT.Singularity.Presentation.Controls.StackPanel(internal6, internal7);
            internal5.Orientation = MT.Singularity.Presentation.Orientation.Horizontal;
            internal5.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Left;
            internal1 = new MT.Singularity.Presentation.Controls.StackPanel(internal2, internal4, internal5);
            internal1.Orientation = MT.Singularity.Presentation.Orientation.Vertical;
            this.Content = internal1;
            this.Height = 380;
            this.Width = 500;
        }
        private readonly MT.Singularity.Expressions.IDataBinding[] bindings = new MT.Singularity.Expressions.IDataBinding[4];
    }
}
