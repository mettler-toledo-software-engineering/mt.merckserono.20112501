﻿using MT.Singularity.Presentation.Controls.Navigation;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Platform.CommonUX.Controls;
using MT.Singularity.Platform.CommonUX.Controls.MeasurementDisplay.View;
namespace IND930YMLUI.Views
{
    [System.CodeDom.Compiler.GeneratedCodeAttribute("YmlCodeGenerator", "2.1.0.0")]
    public partial class WeightWindowControl : MT.Singularity.Presentation.Controls.ContentControl
    {
        private MT.Singularity.Platform.CommonUX.Controls.MeasurementDisplay.View.WeightDisplayWindow myWeightWindow;
        private void InitializeComponents()
        {
            MT.Singularity.Presentation.Controls.DockPanel internal1;
            myWeightWindow = new MT.Singularity.Platform.CommonUX.Controls.MeasurementDisplay.View.WeightDisplayWindow();
            internal1 = new MT.Singularity.Presentation.Controls.DockPanel(myWeightWindow);
            internal1.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            internal1.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Right;
            this.Content = internal1;
            this.Width = 700;
            this.Height = 250;
        }
    }
}
